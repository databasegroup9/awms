-- CREATE ALL TABLES
CREATE TABLE AnimalWelfareOrganisation (
org_name		VARCHAR(50)    not null,	
org_email		VARCHAR(50),	
org_phone		NUMBER,	
	PRIMARY KEY (org_name)
);

CREATE TABLE AnimalWelfareOrgLocations (
org_name		VARCHAR(50)    not null,	
org_location 		VARCHAR(50)    not null,	
PRIMARY KEY (org_name, org_location),
FOREIGN KEY (org_name) REFERENCES AnimalWelfareOrganisation (org_name)
);

CREATE TABLE AnimalShelter (
shelter_num		NUMBER    not null,	
mgr_id			NUMBER    not null,	
org_name		VARCHAR(50), 
shelter_location 	VARCHAR(50),	
shelter_name		VARCHAR(50),
PRIMARY KEY (shelter_num)
);

CREATE TABLE Department (
dept_num		NUMBER    not null,		
dept_name		VARCHAR(50), 
phone_ext 		NUMBER,
shelter_num		NUMBER,    
PRIMARY KEY (dept_num)
);

CREATE TABLE Employee (
emp_id			    NUMBER          not null,
email			VARCHAR(50)     not null,	
full_name 			VARCHAR(50),
salary			NUMBER,
phone			NUMBER,
dept_num		NUMBER,
isdeptmanager 	NUMBER,
emp_password	    VARCHAR(50), 
PRIMARY KEY (emp_id)
);

CREATE TABLE AnimalCareDept (
dept_num		NUMBER    not null,		
animal_type		VARCHAR(50), 
vet_contact		NUMBER, 
PRIMARY KEY (dept_num)
);

CREATE TABLE CustomerServiceDept (
dept_num		NUMBER    not null,		
public_email		VARCHAR(50), 
public_phone		NUMBER, 
PRIMARY KEY (dept_num)
);

CREATE TABLE LoginAttempt(
emp_id		NUMBER    not null,		
login_id		NUMBER    not null,
login_datetime		DATE,
login_result		NUMBER,
PRIMARY KEY (emp_id, login_id)
);

CREATE TABLE Customer(
cust_id			NUMBER    not null,
cust_email		VARCHAR(50),	
cust_name 		VARCHAR(50),
cust_age		NUMBER,
cust_address		VARCHAR(50),	
PRIMARY KEY (cust_id)
);

CREATE TABLE Serves (
cust_id			NUMBER    not null,	
dept_num		NUMBER    not null,	
PRIMARY KEY (cust_id, dept_num)
);

CREATE TABLE Animal(
animal_num		NUMBER    not null,
pet_name		VARCHAR(50),	
adopt_fee 		NUMBER,
description			VARCHAR(500),
common_name	VARCHAR(50),
breed			VARCHAR(50),
adopted_by		NUMBER,
kept_in			NUMBER,
PRIMARY KEY (animal_num),
FOREIGN KEY (kept_in) REFERENCES AnimalCareDept (dept_num),
FOREIGN KEY (adopted_by) REFERENCES Customer (cust_id)
);

CREATE TABLE CareFor(
employee_id		    NUMBER    not null,	
animal_num		NUMBER    not null,
PRIMARY KEY (employee_id, animal_num),
FOREIGN KEY (employee_id) REFERENCES Employee(emp_id),
FOREIGN KEY (animal_num) REFERENCES Animal(animal_num)
);

--FOREIGN KEYS
ALTER TABLE AnimalShelter ADD FOREIGN KEY (org_name) REFERENCES AnimalWelfareOrganisation (org_name);
ALTER TABLE AnimalShelter ADD FOREIGN KEY (mgr_id) REFERENCES Employee (emp_id);

ALTER TABLE Department ADD FOREIGN KEY (shelter_num) REFERENCES AnimalShelter (shelter_num);

ALTER TABLE Employee ADD FOREIGN KEY (dept_num) REFERENCES Department (dept_num);

ALTER TABLE AnimalCareDept ADD FOREIGN KEY (dept_num) REFERENCES Department (dept_num);

ALTER TABLE CustomerServiceDept ADD FOREIGN KEY (dept_num) REFERENCES Department(dept_num);

ALTER TABLE LoginAttempt ADD FOREIGN KEY (emp_id) REFERENCES Employee (emp_id);

ALTER TABLE Serves ADD FOREIGN KEY (cust_id) REFERENCES Customer(cust_id);
ALTER TABLE Serves ADD FOREIGN KEY (dept_num) REFERENCES CustomerServiceDept (dept_num);

-- CREATE VIEWS
/
CREATE OR REPLACE VIEW V_AVAILABLE_ANIMALS AS SELECT a. animal_num, a.pet_name, a.description, a.common_name, a.breed, a.adopt_fee, s.shelter_location
FROM Animal a, Department d, AnimalShelter s
WHERE adopted_by IS NULL
AND a.kept_in = d.dept_num 
AND d.shelter_num = s.shelter_num;
/
--CREATE TRIGGERS
/
CREATE OR REPLACE TRIGGER TR_ANIMALADOPT 
AFTER UPDATE OF ADOPTED_BY ON ANIMAL 
FOR EACH ROW
DECLARE
cust NUMBER; 
BEGIN
SELECT :new.adopted_by INTO cust FROM dual;
DELETE FROM carefor WHERE animal_num = :new.animal_num AND cust is not null;
END;
/
CREATE OR REPLACE TRIGGER TR_ANIMALADOPT_AGE_CHK 
BEFORE UPDATE OF ADOPTED_BY ON ANIMAL
referencing new as new old as old
for each row
DECLARE
age NUMBER;
BEGIN
IF :new.adopted_by is not null THEN
    SELECT cust_age INTO age FROM customer WHERE :new.adopted_by = cust_id;
     if (age < 18) THEN
         raise_application_error (-20999,'Age of customer is not 18 or over.');
    end if;
END IF;    
end;
--CREATE SEQUENCES
/
CREATE SEQUENCE customers_seq START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE dept_seq START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE animal_seq START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE employee_seq START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE log_seq START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE shelter_seq START WITH 1 INCREMENT BY 1;
/
--CREATE INDEXES
CREATE INDEX IDX_ANIMAL_TYPE ON ANIMAL (COMMON_NAME) GLOBAL PARTITION BY HASH (COMMON_NAME) partitions 4;
CREATE INDEX IDX_LOGINTIME ON LOGINATTEMPT (LOGIN_DATETIME);
CREATE INDEX IDX_EMP_SALARY ON EMPLOYEE (DEPT_NUM, SALARY);
CREATE INDEX IDX_ANIMAL_ADOPTED ON ANIMAL (ADOPTED_BY) GLOBAL PARTITION BY HASH (ADOPTED_BY) partitions 4;
-- CREATE PROCEDURES
/
CREATE OR REPLACE PROCEDURE sm_DelDepartment 
(
    dept_id in NUMBER, 
    manager_id in NUMBER
)
IS
BEGIN
-- Remove department from department manager employee.
UPDATE Employee SET dept_num = null, isdeptmanager = 0
WHERE dept_num = dept_id;
--Delete department
DELETE FROM Animalcaredept WHERE dept_num = dept_id;
DELETE FROM Customerservicedept WHERE dept_num = dept_id;
DELETE FROM Department WHERE dept_num = dept_id;
END;
/
CREATE OR REPLACE PROCEDURE sm_DelEmployee
( 
    id in NUMBER
)
IS
BEGIN
-- Delete Employee
DELETE FROM Employee WHERE emp_id = id;
END;
/
CREATE OR REPLACE PROCEDURE sm_EditDepartment 
(
    dept_id in NUMBER, 
    old_manager_id in NUMBER,
    new_manager_id in NUMBER,
    d_name in varchar2,
    ext in NUMBER,
    dept_type in NUMBER,
    pub_phone in NUMBER,
    pub_email in VARCHAR2,
    vet in NUMBER,
    animal in VARCHAR2,
    old_type in NUMBER
)
IS
BEGIN
--Update the department
UPDATE department SET 
dept_name = d_name,  
phone_ext = ext
WHERE dept_num = dept_id;

IF old_manager_id != new_manager_id THEN
    --Remove old Dept Manager
    UPDATE Employee SET isdeptmanager = 0 WHERE emp_id = old_manager_id;
    --Update new Dept Manager
    UPDATE Employee SET isdeptmanager = 1, dept_num = dept_id WHERE emp_id = new_manager_id;
END IF;

IF dept_type = 0 THEN
--Update AnimalCareDept
  UPDATE animalcaredept SET animal_type = animal, vet_contact = vet WHERE dept_num = dept_id;
END IF;
IF dept_type = 1 THEN
--Update CustomerServiceDept
  UPDATE customerservicedept SET public_email = pub_email, public_phone = pub_phone WHERE dept_num = dept_id;
END IF;

IF old_type != dept_type THEN
  IF dept_type = 0 THEN
--Update AnimalCareDept
  DELETE FROM customerservicedept WHERE dept_num = dept_id;
  INSERT INTO animalcaredept VALUES (dept_id, animal, vet);
END IF;
IF dept_type = 1 THEN
--Update CustomerServiceDept
  DELETE FROM animalcaredept WHERE dept_num = dept_id;
  INSERT INTO customerservicedept VALUES (dept_id, pub_email, pub_phone);
END IF;
END IF;
END;
/
CREATE OR REPLACE PROCEDURE sm_EditEmployee
(
    id in NUMBER, 
    emp_email in VARCHAR,
    emp_name in VARCHAR,
    sal in NUMBER,
    ph in NUMBER,
    dept in NUMBER
)
IS
BEGIN
--Update the Employee
UPDATE employee SET 
full_name = emp_name,  
email = emp_email,
salary = sal,
phone = ph,
dept_num = dept
WHERE emp_id = id;
END;
/
CREATE OR REPLACE PROCEDURE sm_NewDepartment 
(
    dept_name in department.dept_name%TYPE,
    ext in department.phone_ext%TYPE,
    shelter_id in NUMBER,
    dept_type in NUMBER,
    dept_id out NUMBER,
    mgr_id in NUMBER,
    pub_phone in NUMBER,
    pub_email in VARCHAR2,
    vet in NUMBER,
    animal in VARCHAR2
)
IS
BEGIN
dept_id := dept_seq.NEXTVAL;
--Insert Dept
INSERT INTO department VALUES (dept_id,dept_name,ext,shelter_id);
--Update Manager
UPDATE employee SET dept_num = dept_id, isdeptmanager = 1 WHERE emp_id = mgr_id;
--Add Animal Care
IF dept_type = 0 THEN
INSERT INTO animalcaredept VALUES (dept_id,animal,vet);
END IF;
--Add Customer Service
IF dept_type = 1 THEN
INSERT INTO customerservicedept VALUES (dept_id,pub_email,pub_phone);
END IF;
END;
/
CREATE OR REPLACE PROCEDURE sm_NewEmployee
(
    id out NUMBER, 
    emp_email in VARCHAR,
    emp_name in VARCHAR,
    sal in NUMBER,
    ph in NUMBER,
    dept in NUMBER,
    pass in VARCHAR
)
IS
BEGIN
id := employee_seq.NEXTVAL;
--Insert the Employee
INSERT INTO employee VALUES 
( id, emp_email, emp_name, sal, ph, dept, 0, pass );
END;
/
CREATE OR REPLACE PROCEDURE sp_AddAnimal
( 
    animal_id OUT NUMBER, 
    pet_name IN VARCHAR, 
    adopt_fee IN NUMBER, 
    descr IN VARCHAR,
    common_name IN VARCHAR, 
    breed IN VARCHAR,
    kept_in IN NUMBER 
)
IS
BEGIN
animal_id := animal_seq.NEXTVAL;
INSERT INTO Animal
VALUES 
(
animal_id, pet_name, adopt_fee, 
descr, common_name, breed, NULL, kept_in
);
END;
/
CREATE OR REPLACE PROCEDURE sp_AddCustomer
( 
    cust_id OUT NUMBER, 
    cust_email IN VARCHAR, 
    cust_name IN VARCHAR, 
    cust_age IN NUMBER,
    cust_address IN VARCHAR
)
IS
BEGIN
cust_id := customers_seq.NEXTVAL;
INSERT INTO Customer
VALUES 
(
cust_id, cust_email, cust_name, cust_age, cust_address
);
END;
/
CREATE OR REPLACE PROCEDURE sp_DeleteAnimal ( a_num IN NUMBER )
IS
BEGIN
DELETE FROM Carefor WHERE animal_num = a_num;
DELETE FROM Animal WHERE animal_num = a_num;
END;
/
CREATE OR REPLACE PROCEDURE sp_DeleteCustomer ( c_num IN NUMBER )
IS
BEGIN
DELETE FROM Serves WHERE cust_id = c_num;
DELETE FROM Customer WHERE cust_id = c_num;
END;
/
CREATE OR REPLACE PROCEDURE sp_EditAnimal
(
	animal_id IN NUMBER, 
    p_name IN VARCHAR, 
    fee IN NUMBER, 
    descr IN VARCHAR,
    c_name IN VARCHAR, 
    b IN VARCHAR,
    kept IN NUMBER, 
    adopted IN NUMBER 
)
IS
BEGIN
UPDATE Animal SET 
pet_name = p_name, 
adopt_fee = fee, 
description = descr,
common_name = c_name, 
breed = b, 
adopted_by = adopted, 
kept_in = kept
WHERE animal_num = animal_id;
END;
/
CREATE OR REPLACE PROCEDURE sp_EditCustomer
(
	c_id IN NUMBER, 
    c_email IN VARCHAR, 
    c_name IN VARCHAR, 
    c_age IN NUMBER,
    c_address IN VARCHAR
)
IS
BEGIN
UPDATE Customer SET 
cust_email = c_email, 
cust_name = c_name,
cust_age = c_age, 
cust_address = c_address
WHERE cust_id = c_id;
END;
/
CREATE OR REPLACE PROCEDURE sm_NewShelter
(
    id out NUMBER, 
    mgr in NUMBER,
    org in VARCHAR,
    location in VARCHAR,
    name in VARCHAR
)
IS
BEGIN
id := shelter_seq.NEXTVAL;
--Insert the AnimalShelter
INSERT INTO AnimalShelter VALUES 
( id, mgr, org, location, name);
END;
/
CREATE OR REPLACE PROCEDURE sm_DelShelter
( 
    id in NUMBER
)
IS
BEGIN
-- Delete AnimalShelter
DELETE FROM AnimalShelter WHERE shelter_num = id;
END;
/
CREATE OR REPLACE PROCEDURE sm_EditShelter
(
    id in NUMBER, 
    mgr in NUMBER,
    location in VARCHAR,
    name in VARCHAR
)
IS
BEGIN
--Update the AnimalShelter
UPDATE AnimalShelter SET 
mgr_id = mgr,  
shelter_location = location,
shelter_name = name
WHERE shelter_num = id;
END;
/


