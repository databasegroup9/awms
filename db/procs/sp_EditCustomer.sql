CREATE OR REPLACE PROCEDURE sp_EditCustomer
(
	c_id IN NUMBER, 
    c_email IN VARCHAR, 
    c_name IN VARCHAR, 
    c_age IN NUMBER,
    c_address IN VARCHAR
)
IS
BEGIN
UPDATE Customer SET 
cust_email = c_email, 
cust_name = c_name,
cust_age = c_age, 
cust_address = c_address
WHERE cust_id = c_id;
END;
