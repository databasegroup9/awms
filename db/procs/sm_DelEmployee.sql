CREATE OR REPLACE PROCEDURE sm_DelEmployee
( 
    id in NUMBER
)
IS
BEGIN
-- Delete Employee
DELETE FROM Employee WHERE emp_id = id;
END;