CREATE OR REPLACE PROCEDURE sm_EditDepartment 
(
    dept_id in NUMBER, 
    old_manager_id in NUMBER,
    new_manager_id in NUMBER,
    d_name in varchar2,
    ext in NUMBER,
    dept_type in NUMBER,
    pub_phone in NUMBER,
    pub_email in VARCHAR2,
    vet in NUMBER,
    animal in VARCHAR2,
    old_type in NUMBER
)
IS
BEGIN
--Update the department
UPDATE department SET 
dept_name = d_name,  
phone_ext = ext
WHERE dept_num = dept_id;

IF old_manager_id != new_manager_id THEN
    --Remove old Dept Manager
    UPDATE Employee SET isdeptmanager = 0 WHERE emp_id = old_manager_id;
    --Update new Dept Manager
    UPDATE Employee SET isdeptmanager = 1, dept_num = dept_id WHERE emp_id = new_manager_id;
END IF;

IF dept_type = 0 THEN
--Update AnimalCareDept
  UPDATE animalcaredept SET animal_type = animal, vet_contact = vet WHERE dept_num = dept_id;
END IF;
IF dept_type = 1 THEN
--Update CustomerServiceDept
  UPDATE customerservicedept SET public_email = pub_email, public_phone = pub_phone WHERE dept_num = dept_id;
END IF;

IF old_type != dept_type THEN
  IF dept_type = 0 THEN
--Update AnimalCareDept
  DELETE FROM customerservicedept WHERE dept_num = dept_id;
  INSERT INTO animalcaredept VALUES (dept_id, animal, vet);
END IF;
IF dept_type = 1 THEN
--Update CustomerServiceDept
  DELETE FROM animalcaredept WHERE dept_num = dept_id;
  INSERT INTO customerservicedept VALUES (dept_id, pub_email, pub_phone);
END IF;
END IF;
END;