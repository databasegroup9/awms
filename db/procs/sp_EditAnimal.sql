CREATE OR REPLACE PROCEDURE sp_EditAnimal
(
	animal_id IN NUMBER, 
    p_name IN VARCHAR, 
    fee IN NUMBER, 
    descr IN VARCHAR,
    c_name IN VARCHAR, 
    b IN VARCHAR,
    kept IN NUMBER, 
    adopted IN NUMBER 
)
IS
BEGIN
UPDATE Animal SET 
pet_name = p_name, 
adopt_fee = fee, 
description = descr,
common_name = c_name, 
breed = b, 
adopted_by = adopted, 
kept_in = kept
WHERE animal_num = animal_id;
END;
