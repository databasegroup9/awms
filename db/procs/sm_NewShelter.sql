CREATE OR REPLACE PROCEDURE sm_NewShelter
(
    id out NUMBER, 
    mgr in NUMBER,
    org in VARCHAR,
    location in VARCHAR,
    name in VARCHAR
)
IS
BEGIN
id := shelter_seq.NEXTVAL;
--Insert the AnimalShelter
INSERT INTO AnimalShelter VALUES 
( id, mgr, org, location, name);
END;