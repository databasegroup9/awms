CREATE OR REPLACE PROCEDURE sm_NewEmployee
(
    id out NUMBER, 
    emp_email in VARCHAR,
    emp_name in VARCHAR,
    sal in NUMBER,
    ph in NUMBER,
    dept in NUMBER,
    pass in VARCHAR
)
IS
BEGIN
id := employee_seq.NEXTVAL;
--Insert the Employee
INSERT INTO employee VALUES 
( id, emp_email, emp_name, sal, ph, dept, 0, pass );
END;