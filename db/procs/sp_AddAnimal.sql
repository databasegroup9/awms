CREATE OR REPLACE PROCEDURE sp_AddAnimal
( 
    animal_id OUT NUMBER, 
    pet_name IN VARCHAR, 
    adopt_fee IN NUMBER, 
    descr IN VARCHAR,
    common_name IN VARCHAR, 
    breed IN VARCHAR,
    kept_in IN NUMBER 
)
IS
BEGIN
animal_id := animal_seq.NEXTVAL;
INSERT INTO Animal
VALUES 
(
animal_id, pet_name, adopt_fee, 
descr, common_name, breed, NULL, kept_in
);
END
