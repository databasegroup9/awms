CREATE OR REPLACE PROCEDURE sm_EditEmployee
(
    id in NUMBER, 
    emp_email in VARCHAR,
    emp_name in VARCHAR,
    sal in NUMBER,
    ph in NUMBER,
    dept in NUMBER
)
IS
BEGIN
--Update the Employee
UPDATE employee SET 
full_name = emp_name,  
email = emp_email,
salary = sal,
phone = ph,
dept_num = dept
WHERE emp_id = id;
END;