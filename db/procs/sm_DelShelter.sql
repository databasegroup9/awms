CREATE OR REPLACE PROCEDURE sm_DelShelter
( 
    id in NUMBER
)
IS
BEGIN
-- Delete AnimalShelter
DELETE FROM AnimalShelter WHERE shelter_num = id;
END;