CREATE OR REPLACE PROCEDURE sp_DeleteCustomer ( c_num IN NUMBER )
IS
BEGIN
DELETE FROM Serves WHERE cust_id = c_num;
DELETE FROM Customer WHERE cust_id = c_num;
END;