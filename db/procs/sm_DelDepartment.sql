CREATE OR REPLACE PROCEDURE sm_DelDepartment 
(
    dept_id in NUMBER, 
    manager_id in NUMBER
)
IS
BEGIN
-- Remove department from department manager employee.
UPDATE Employee SET dept_num = null, isdeptmanager = 0
WHERE dept_num = dept_id;
--Delete department
DELETE FROM Animalcaredept WHERE dept_num = dept_id;
DELETE FROM Customerservicedept WHERE dept_num = dept_id;
DELETE FROM Department WHERE dept_num = dept_id;
END;