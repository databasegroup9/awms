CREATE OR REPLACE PROCEDURE sp_AddCustomer
( 
    cust_id OUT NUMBER, 
    cust_email IN VARCHAR, 
    cust_name IN NUMBER, 
    cust_age IN NUMBER,
    cust_address IN VARCHAR
)
IS
BEGIN
cust_id := customers_seq.NEXTVAL;
INSERT INTO Customer
VALUES 
(
cust_id, cust_email, cust_name, cust_age, cust_address
);
END;
