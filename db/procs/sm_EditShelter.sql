CREATE OR REPLACE PROCEDURE sm_EditShelter
(
    id in NUMBER, 
    mgr in NUMBER,
    location in VARCHAR,
    name in VARCHAR
)
IS
BEGIN
--Update the AnimalShelter
UPDATE AnimalShelter SET 
mgr_id = mgr,  
shelter_location = location,
shelter_name = name
WHERE shelter_num = id;
END;