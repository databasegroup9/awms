CREATE OR REPLACE PROCEDURE sm_NewDepartment 
(
    dept_name in department.dept_name%TYPE,
    ext in department.phone_ext%TYPE,
    shelter_id in NUMBER,
    dept_type in NUMBER,
    dept_id out NUMBER,
    mgr_id in NUMBER,
    pub_phone in NUMBER,
    pub_email in VARCHAR2,
    vet in NUMBER,
    animal in VARCHAR2
)
IS
BEGIN
dept_id := dept_seq.NEXTVAL;
--Insert Dept
INSERT INTO department VALUES (dept_id,dept_name,ext,shelter_id);
--Update Manager
UPDATE employee SET dept_num = dept_id, isdeptmanager = 1 WHERE emp_id = mgr_id;
--Add Animal Care
IF dept_type = 0 THEN
INSERT INTO animalcaredept VALUES (dept_id,animal,vet);
END IF;
--Add Customer Service
IF dept_type = 1 THEN
INSERT INTO customerservicedept VALUES (dept_id,pub_email,pub_phone);
END IF;
END;