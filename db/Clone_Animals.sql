DECLARE
max_animal Animal.animal_num%TYPE;
BEGIN
    INSERT INTO Animal
    VALUES (1,'Bob',100,'Cute Doggie','Dog','Doberman',NULL,1);
    SELECT MAX(animal_num) INTO max_animal FROM Animal;
    FOR i in max_animal+1..max_animal+10000
    LOOP
    INSERT INTO Animal
    VALUES (i,'Bob',100,'Cute Doggie','Dog','Doberman',NULL,1);
    END LOOP;
END;