﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Threading;
using System.Timers;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace AWMS_Customer
{
    
    public partial class AnimalsForm : Form
    {
        private OracleConnection Conn;
        private bool working;
        private DataTable table;

        private const String animals = 
            "SELECT ANIMAL_NUM AS ID, PET_NAME AS Name, DESCRIPTION, COMMON_NAME AS Type, BREED AS Breed, ADOPT_FEE AS Fee, SHELTER_LOCATION AS Location " + 
            "FROM V_AVAILABLE_ANIMALS";

        private class BackGroundArgs
        {
            public OracleConnection Conn;
            public DataTable table;
        }

        public AnimalsForm()
        {
            try
            {
                InitializeComponent();
                var connStr = File.ReadAllText("DBS.conn");
                Conn = new OracleConnection(connStr);
                Conn.Open();
                loadingLbl.Visible = working = true;
                backgroundWorker.RunWorkerAsync(new BackGroundArgs {Conn = Conn, table = table});
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);
                Close();
            }
            
        }

        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            Thread.Sleep(5000);
            var lines = new List<String>();
            lines.Add("var animals = [");
            var args = e.Argument as BackGroundArgs;
            var a = new OracleDataAdapter(animals, args.Conn);
            args.table = new DataTable();
            a.Fill(args.table);
            e.Result = args;
            int id = 0;
            foreach (DataRow row in args.table.Rows)
            {
                id++;
                var ID = row["ID"].ToString();
                var NAME = row["NAME"].ToString();
                var DESCRIPTION = row["DESCRIPTION"].ToString();
                var TYPE = row["TYPE"].ToString();
                var BREED = row["BREED"].ToString();
                var FEE = row["FEE"].ToString();
                var LOCATION = row["LOCATION"].ToString();

                String line = string.Format(@"{{""id"":{0},""name"":""{1}"",""description"":""{2}"",""type"":""{3}"",""breed"":""{4}"",""fee"":""{5}"",""location"":""{6}""}},",
                    ID, NAME, DESCRIPTION, TYPE, BREED, FEE, LOCATION);
                lines.Add(line);
            }

            lines[lines.Count-1] = lines[lines.Count - 1].TrimEnd(',');

            lines.Add("];");

            File.WriteAllLines(@".\animals.js", lines.ToArray());
        }

        private void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            var result = e.Result as BackGroundArgs;
            dataGridView1.DataSource = table = result.table;
            working = loadingLbl.Visible = false;
            Conn.Close();
            Conn.Open();
            backgroundWorker.RunWorkerAsync(new BackGroundArgs { Conn = Conn, table = table });
        }
    }
}
