﻿using System;

namespace AWMS_Install
{
    public class AWOrg
    {
        public String OrgName;
        public String OrgEmail;
        public int OrgPhone;

        public override string ToString()
        {
            return OrgName;
        }
    }

    public class AnimalShelter
    {
        public String ShelterName;
        public String ShelterLocation;

        public override string ToString()
        {
            return ShelterName;
        }
    }

    public  class Employee
    {
        public String Name;
        public String Email;
        public String Password;

        public override string ToString()
        {
            return Name + " - " + Email;
        }
    }
    
}
