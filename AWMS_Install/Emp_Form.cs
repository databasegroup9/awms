﻿using System;
using System.Windows.Forms;

namespace AWMS_Install
{
    public partial class Emp_Form : Form
    {
        public Employee Employee;

        public Emp_Form()
        {
            InitializeComponent();
        }

        private void CancelBtn_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Dispose();
        }

        private void OKBtn_Click(object sender, EventArgs e)
        {
            try
            {
                Employee = new Employee
                {
                    Name = nameTextBox.Text.Trim(),
                    Email = locationTextBox.Text.Trim(),
                    Password = passwordTextBox.Text.Trim()
                };

                DialogResult = DialogResult.OK;
                Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
        }
    }
}
