﻿namespace AWMS_Install
{
    partial class AWMS_DB_Install
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.connFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.connFileLocationTextBox = new System.Windows.Forms.TextBox();
            this.selectConnFileBtn = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.orgLbl = new System.Windows.Forms.Label();
            this.AWODetailsBtn = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.shelterLbl = new System.Windows.Forms.Label();
            this.shelterDetailsBtn = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.empLbl = new System.Windows.Forms.Label();
            this.empDetailsBtn = new System.Windows.Forms.Button();
            this.installBtn = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // connFileDialog
            // 
            this.connFileDialog.FileName = "connFileDialog";
            // 
            // connFileLocationTextBox
            // 
            this.connFileLocationTextBox.Enabled = false;
            this.connFileLocationTextBox.Location = new System.Drawing.Point(12, 30);
            this.connFileLocationTextBox.Name = "connFileLocationTextBox";
            this.connFileLocationTextBox.Size = new System.Drawing.Size(376, 20);
            this.connFileLocationTextBox.TabIndex = 0;
            // 
            // selectConnFileBtn
            // 
            this.selectConnFileBtn.Location = new System.Drawing.Point(148, 56);
            this.selectConnFileBtn.Name = "selectConnFileBtn";
            this.selectConnFileBtn.Size = new System.Drawing.Size(98, 23);
            this.selectConnFileBtn.TabIndex = 1;
            this.selectConnFileBtn.Text = "Select Conn File";
            this.selectConnFileBtn.UseVisualStyleBackColor = true;
            this.selectConnFileBtn.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.orgLbl);
            this.groupBox1.Controls.Add(this.AWODetailsBtn);
            this.groupBox1.Location = new System.Drawing.Point(13, 86);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(375, 99);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Animal Welfare Organisation";
            // 
            // orgLbl
            // 
            this.orgLbl.AutoSize = true;
            this.orgLbl.Location = new System.Drawing.Point(3, 33);
            this.orgLbl.Name = "orgLbl";
            this.orgLbl.Size = new System.Drawing.Size(33, 13);
            this.orgLbl.TabIndex = 7;
            this.orgLbl.Text = "None";
            // 
            // AWODetailsBtn
            // 
            this.AWODetailsBtn.Location = new System.Drawing.Point(6, 70);
            this.AWODetailsBtn.Name = "AWODetailsBtn";
            this.AWODetailsBtn.Size = new System.Drawing.Size(98, 23);
            this.AWODetailsBtn.TabIndex = 6;
            this.AWODetailsBtn.Text = "Details";
            this.AWODetailsBtn.UseVisualStyleBackColor = true;
            this.AWODetailsBtn.Click += new System.EventHandler(this.AWODetailsBtn_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.shelterLbl);
            this.groupBox2.Controls.Add(this.shelterDetailsBtn);
            this.groupBox2.Location = new System.Drawing.Point(13, 191);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(375, 95);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "First Animal Shelter";
            // 
            // shelterLbl
            // 
            this.shelterLbl.AutoSize = true;
            this.shelterLbl.Location = new System.Drawing.Point(7, 33);
            this.shelterLbl.Name = "shelterLbl";
            this.shelterLbl.Size = new System.Drawing.Size(33, 13);
            this.shelterLbl.TabIndex = 8;
            this.shelterLbl.Text = "None";
            // 
            // shelterDetailsBtn
            // 
            this.shelterDetailsBtn.Location = new System.Drawing.Point(6, 66);
            this.shelterDetailsBtn.Name = "shelterDetailsBtn";
            this.shelterDetailsBtn.Size = new System.Drawing.Size(98, 23);
            this.shelterDetailsBtn.TabIndex = 7;
            this.shelterDetailsBtn.Text = "Details";
            this.shelterDetailsBtn.UseVisualStyleBackColor = true;
            this.shelterDetailsBtn.Click += new System.EventHandler(this.shelterDetailsBtn_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.empLbl);
            this.groupBox3.Controls.Add(this.empDetailsBtn);
            this.groupBox3.Location = new System.Drawing.Point(13, 292);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(375, 95);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "First Animal Shelter Manager";
            // 
            // empLbl
            // 
            this.empLbl.AutoSize = true;
            this.empLbl.Location = new System.Drawing.Point(6, 30);
            this.empLbl.Name = "empLbl";
            this.empLbl.Size = new System.Drawing.Size(33, 13);
            this.empLbl.TabIndex = 9;
            this.empLbl.Text = "None";
            // 
            // empDetailsBtn
            // 
            this.empDetailsBtn.Location = new System.Drawing.Point(6, 66);
            this.empDetailsBtn.Name = "empDetailsBtn";
            this.empDetailsBtn.Size = new System.Drawing.Size(98, 23);
            this.empDetailsBtn.TabIndex = 8;
            this.empDetailsBtn.Text = "Details";
            this.empDetailsBtn.UseVisualStyleBackColor = true;
            this.empDetailsBtn.Click += new System.EventHandler(this.empDetailsBtn_Click);
            // 
            // installBtn
            // 
            this.installBtn.Enabled = false;
            this.installBtn.Location = new System.Drawing.Point(313, 402);
            this.installBtn.Name = "installBtn";
            this.installBtn.Size = new System.Drawing.Size(75, 23);
            this.installBtn.TabIndex = 5;
            this.installBtn.Text = "Install DB";
            this.installBtn.UseVisualStyleBackColor = true;
            this.installBtn.Click += new System.EventHandler(this.installBtn_Click);
            // 
            // AWMS_DB_Install
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(412, 437);
            this.Controls.Add(this.installBtn);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.selectConnFileBtn);
            this.Controls.Add(this.connFileLocationTextBox);
            this.Name = "AWMS_DB_Install";
            this.Text = "AWMS Server Install";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog connFileDialog;
        private System.Windows.Forms.TextBox connFileLocationTextBox;
        private System.Windows.Forms.Button selectConnFileBtn;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button AWODetailsBtn;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button shelterDetailsBtn;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button empDetailsBtn;
        private System.Windows.Forms.Button installBtn;
        private System.Windows.Forms.Label orgLbl;
        private System.Windows.Forms.Label shelterLbl;
        private System.Windows.Forms.Label empLbl;
    }
}

