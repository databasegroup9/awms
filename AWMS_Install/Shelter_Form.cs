﻿using System;
using System.Windows.Forms;

namespace AWMS_Install
{
    public partial class Shelter_Form : Form
    {
        public AnimalShelter AnShelter;

        public Shelter_Form()
        {
            InitializeComponent();
        }

        private void CancelBtn_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Dispose();
        }

        private void OKBtn_Click(object sender, EventArgs e)
        {
            try
            {
                AnShelter = new AnimalShelter
                {
                    ShelterName = nameTextBox.Text.Trim(),
                    ShelterLocation = locationTextBox.Text.Trim(),
                };

                DialogResult = DialogResult.OK;
                Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
        }
    }
}
