﻿using System;
using System.Data;
using System.IO;
using System.Windows.Forms;
using AWMS_Install.Properties;
using Oracle.DataAccess.Client;

namespace AWMS_Install
{
    public class DataBase
    {
        private String ConnStr;

        public OracleConnection Connection = null;

        public void Connect(String conn)
        {
            ConnStr = conn;
           
            try
            {
                Connection = new OracleConnection(ConnStr);
                Connection.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex);
                if (Connection != null)
                {
                    Connection.Close();
                }   
            }
        }

        public void Close()
        {
            Connection.Close();
        }

        public void SetupDatabase(Employee e, AWOrg o, AnimalShelter s)
        {
            try
            {
                var command = new OracleCommand(GetEmployeeScript(e), Connection);
                command.ExecuteNonQuery();

                command = new OracleCommand(GetOrgScript(o), Connection);
                command.ExecuteNonQuery();

                command = new OracleCommand(GetShelterScript(o, s), Connection);
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex);
                if (Connection != null)
                {
                    Connection.Close();
                }   
            }
        }

        private string GetShelterScript(AWOrg org, AnimalShelter shelter)
        {
            return string.Format("INSERT INTO AnimalShelter VALUES ( shelter_seq.NEXTVAL, employee_seq.CURRVAL, '{0}', '{1}', '{2}')"
                , org.OrgName, shelter.ShelterLocation, shelter.ShelterName);
        }

        private string GetOrgScript(AWOrg org)
        {
            return string.Format("INSERT INTO animalwelfareorganisation VALUES ( '{0}', '{1}', {2})"
                , org.OrgName, org.OrgEmail, org.OrgPhone);
        }

        private string GetEmployeeScript(Employee employee)
        {
            return string.Format("INSERT INTO employee VALUES ( employee_seq.NEXTVAL, '{0}', '{1}', 1, 1, NULL, 0, '{2}' )"
                , employee.Email,employee.Name,employee.Password);
        }
    }
}
