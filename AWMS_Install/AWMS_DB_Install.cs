﻿using System;
using System.IO;
using System.Windows.Forms;

namespace AWMS_Install
{
    public partial class AWMS_DB_Install : Form
    {
        private AnimalShelter shelter;
        private AWOrg org;
        private Employee emp;
        private String connStr;
        private String filePath;
        private DataBase db;

        public AWMS_DB_Install()
        {
            InitializeComponent();
            connFileDialog.Title = "Select Connection File";
            connFileDialog.InitialDirectory = ".";
            connFileDialog.Multiselect = false;
            connFileDialog.Filter = "Connection Files (*.conn)|*.conn";
            db = new DataBase();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var result = connFileDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                filePath = connFileDialog.FileName;
                connStr = File.ReadAllText(filePath);
            }
            connFileLocationTextBox.Text = filePath;
        }

        private void AWODetailsBtn_Click(object sender, EventArgs e)
        {
            using (var dialog = new AWO_Form())
            {
               dialog.ShowDialog();
               if(dialog.DialogResult == DialogResult.OK)
               {
                   org = dialog.AwOrg;
               }
            }
            RefreshLabels();
        }

        private void shelterDetailsBtn_Click(object sender, EventArgs e)
        {
            using (var dialog = new Shelter_Form())
            {
                dialog.ShowDialog();
                if (dialog.DialogResult == DialogResult.OK)
                {
                    shelter = dialog.AnShelter;
                }
            }
            RefreshLabels();
        }

        private void empDetailsBtn_Click(object sender, EventArgs e)
        {
            using (var dialog = new Emp_Form())
            {
                dialog.ShowDialog();
                if (dialog.DialogResult == DialogResult.OK)
                {
                    emp = dialog.Employee;
                }
            }
            RefreshLabels();
        }

        private void RefreshLabels()
        {
            int c = 0;

            if (org != null)
            {
                orgLbl.Text = org.ToString();
                c++;
            }

            if (emp != null)
            {
                empLbl.Text = emp.ToString();
                c++;
            }

            if (shelter != null)
            {
                shelterLbl.Text = shelter.ToString();
                c++;
            }

            if (c == 3 && connStr != null)
            {
                installBtn.Enabled = true;
            }
        }

        private void installBtn_Click(object sender, EventArgs e)
        {
            db.Connect(connStr);
            db.SetupDatabase(emp,org,shelter);
            db.Close();
            MessageBox.Show(connStr);
        }
    }
}
