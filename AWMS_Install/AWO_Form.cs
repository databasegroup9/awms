﻿using System;
using System.Windows.Forms;

namespace AWMS_Install
{
    public partial class AWO_Form : Form
    {
        public AWOrg AwOrg;

        public AWO_Form()
        {
            InitializeComponent();
        }

        private void CancelBtn_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Dispose();
        }

        private void OKBtn_Click(object sender, EventArgs e)
        {
            try
            {
                AwOrg = new AWOrg
                {
                    OrgName = nameTextBox.Text.Trim(),
                    OrgEmail = emailTextBox.Text.Trim(),
                    OrgPhone = Convert.ToInt32(numberTextBox.Text.Trim())
                };

                DialogResult = DialogResult.OK;
                Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
        }
    }
}
