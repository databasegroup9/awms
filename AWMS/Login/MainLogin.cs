﻿using System;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Windows.Forms;
using AMWS.DeptManagerApp;
using AMWS.EmployeeApp;
using AMWS.ShelterManagerApp;
using AWMS.Common;

namespace AWMS
{
    public struct LoginDetails
    {
        public String Email;
        public String Pass;
        public String Server;
    }

    public partial class MainLogin : Form
    {
        private string connPath;
        private DataBaseManager DataBase;

        public MainLogin(DataBaseManager db)
        {
            InitializeComponent();
            DataBase = db;

            connFileDialog.Title = "Select Connection File";
            connFileDialog.InitialDirectory = ".";
            connFileDialog.Multiselect = false;
            connFileDialog.Filter = "Connection Files (*.conn)|*.conn";
            connFileDialog.FileName = "DBS.conn";

            //Default
            serverTextBox.Text = "AWMS.conn";
            PassInputBox.Text = "password";
            UserNameInputBox.Text = "james@rspca.com";
        }

        private void LoginBtn_Click(object sender, EventArgs e)
        {
            loadingLbl.Text = "Login In...";
            LoginBtn.Enabled = false;

            backgroundWorker.RunWorkerAsync(LoginArgs());
        }

        private LoginDetails LoginArgs()
        {
            return new LoginDetails()
            {
                Email = UserNameInputBox.Text.Trim(),
                Pass = PassInputBox.Text.Trim(),
                Server = File.ReadAllText(serverTextBox.Text)
            };
        }

        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            var args = (LoginDetails)e.Argument;
            DataBase.Connect(args.Email,args.Pass,args.Server);
        }

        private void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (DataBase.Connection.State == ConnectionState.Open)
            {
                if (DataBase.ConnectedUser.UserId == -1)
                {
                    MessageBox.Show("Incorrect Login Details!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    ResetLoginForm();
                }
                if (DataBase.ConnectedUser.UserId == -2)
                {
                    MessageBox.Show("Employee is not assigned to a department!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    ResetLoginForm();
                }
                else
                {
                    loadingLbl.Text = "Loading Application...";
                    LaunchApplication();
                }
            }
            else
            {
                ResetLoginForm();
            }
        }

        public void ResetLoginForm()
        {
            loadingLbl.Text = "";
            LoginBtn.Enabled = true;
            DataBase.Connection.Close();
        }

        private void LaunchApplication()
        {
            //MessageBox.Show(DataBase.ConnectedUser.FullName + " " + DataBase.ConnectedUser.UserType.ToString(), "User");
            switch(DataBase.ConnectedUser.UserType)
            {
                case UserType.EMPLOYEE:
                    LaunchEmployeeApp();
                    break;
                case UserType.SHELTER_MANAGER:
                    LaunchShelterManagerApp();
                    break;
                case UserType.DEPT_MANAGER:
                    LaunchDeptManagerApp();
                    break;
            }
        }

        private void LaunchShelterManagerApp()
        {
            var app = new MainShelterManagerApp(this,DataBase);
            app.Show();
            Hide();
        }

        private void LaunchDeptManagerApp()
        {
            var app = new MainDeptManagerApp(this, DataBase);
            app.Show();
            Hide();
        }

        private void LaunchEmployeeApp()
        {
            var app = new EmployeeMainApp(this, DataBase);
            app.Show();
            Hide();
        }

        private void connFileSelectBtn_Click(object sender, EventArgs e)
        {
            var result = connFileDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                connPath = connFileDialog.FileName;
                serverTextBox.Text = connPath;
            }
            
        }
    }   
}
