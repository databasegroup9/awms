﻿namespace AWMS
{
    partial class MainLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.HeadingLbl = new System.Windows.Forms.Label();
            this.LoginBtn = new System.Windows.Forms.Button();
            this.UserNameInputBox = new System.Windows.Forms.TextBox();
            this.UserLoginLbl = new System.Windows.Forms.Label();
            this.EmailLbl = new System.Windows.Forms.Label();
            this.PassLbl = new System.Windows.Forms.Label();
            this.loginErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.loadingLbl = new System.Windows.Forms.Label();
            this.PassInputBox = new System.Windows.Forms.TextBox();
            this.serverTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.backgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.connFileSelectBtn = new System.Windows.Forms.Button();
            this.connFileDialog = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.loginErrorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // HeadingLbl
            // 
            this.HeadingLbl.AutoSize = true;
            this.HeadingLbl.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HeadingLbl.Location = new System.Drawing.Point(66, 9);
            this.HeadingLbl.Name = "HeadingLbl";
            this.HeadingLbl.Size = new System.Drawing.Size(357, 29);
            this.HeadingLbl.TabIndex = 0;
            this.HeadingLbl.Text = "Animal Welfare Management System\r\n";
            // 
            // LoginBtn
            // 
            this.LoginBtn.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LoginBtn.Location = new System.Drawing.Point(100, 198);
            this.LoginBtn.Name = "LoginBtn";
            this.LoginBtn.Size = new System.Drawing.Size(86, 29);
            this.LoginBtn.TabIndex = 1;
            this.LoginBtn.Text = "Login";
            this.LoginBtn.UseVisualStyleBackColor = true;
            this.LoginBtn.Click += new System.EventHandler(this.LoginBtn_Click);
            // 
            // UserNameInputBox
            // 
            this.UserNameInputBox.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UserNameInputBox.Location = new System.Drawing.Point(100, 103);
            this.UserNameInputBox.Name = "UserNameInputBox";
            this.UserNameInputBox.Size = new System.Drawing.Size(297, 26);
            this.UserNameInputBox.TabIndex = 2;
            // 
            // UserLoginLbl
            // 
            this.UserLoginLbl.AutoSize = true;
            this.UserLoginLbl.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UserLoginLbl.Location = new System.Drawing.Point(12, 56);
            this.UserLoginLbl.Name = "UserLoginLbl";
            this.UserLoginLbl.Size = new System.Drawing.Size(144, 25);
            this.UserLoginLbl.TabIndex = 4;
            this.UserLoginLbl.Text = "Employee Login:";
            // 
            // EmailLbl
            // 
            this.EmailLbl.AutoSize = true;
            this.EmailLbl.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EmailLbl.Location = new System.Drawing.Point(13, 106);
            this.EmailLbl.Name = "EmailLbl";
            this.EmailLbl.Size = new System.Drawing.Size(46, 20);
            this.EmailLbl.TabIndex = 5;
            this.EmailLbl.Text = "Email:";
            // 
            // PassLbl
            // 
            this.PassLbl.AutoSize = true;
            this.PassLbl.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PassLbl.Location = new System.Drawing.Point(13, 138);
            this.PassLbl.Name = "PassLbl";
            this.PassLbl.Size = new System.Drawing.Size(72, 20);
            this.PassLbl.TabIndex = 6;
            this.PassLbl.Text = "Password:";
            // 
            // loginErrorProvider
            // 
            this.loginErrorProvider.ContainerControl = this;
            // 
            // loadingLbl
            // 
            this.loadingLbl.AutoSize = true;
            this.loadingLbl.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadingLbl.Location = new System.Drawing.Point(192, 207);
            this.loadingLbl.Name = "loadingLbl";
            this.loadingLbl.Size = new System.Drawing.Size(0, 20);
            this.loadingLbl.TabIndex = 7;
            // 
            // PassInputBox
            // 
            this.PassInputBox.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PassInputBox.Location = new System.Drawing.Point(100, 135);
            this.PassInputBox.Name = "PassInputBox";
            this.PassInputBox.PasswordChar = '•';
            this.PassInputBox.Size = new System.Drawing.Size(297, 26);
            this.PassInputBox.TabIndex = 3;
            // 
            // serverTextBox
            // 
            this.serverTextBox.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serverTextBox.Location = new System.Drawing.Point(100, 166);
            this.serverTextBox.Name = "serverTextBox";
            this.serverTextBox.Size = new System.Drawing.Size(297, 26);
            this.serverTextBox.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 169);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 20);
            this.label1.TabIndex = 9;
            this.label1.Text = "Connection:";
            // 
            // backgroundWorker
            // 
            this.backgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker_DoWork);
            this.backgroundWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker_RunWorkerCompleted);
            // 
            // connFileSelectBtn
            // 
            this.connFileSelectBtn.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.connFileSelectBtn.Location = new System.Drawing.Point(403, 166);
            this.connFileSelectBtn.Name = "connFileSelectBtn";
            this.connFileSelectBtn.Size = new System.Drawing.Size(30, 26);
            this.connFileSelectBtn.TabIndex = 10;
            this.connFileSelectBtn.Text = "...";
            this.connFileSelectBtn.UseVisualStyleBackColor = true;
            this.connFileSelectBtn.Click += new System.EventHandler(this.connFileSelectBtn_Click);
            // 
            // connFileDialog
            // 
            this.connFileDialog.FileName = "openFileDialog1";
            // 
            // MainLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(469, 242);
            this.Controls.Add(this.connFileSelectBtn);
            this.Controls.Add(this.PassInputBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.serverTextBox);
            this.Controls.Add(this.loadingLbl);
            this.Controls.Add(this.PassLbl);
            this.Controls.Add(this.EmailLbl);
            this.Controls.Add(this.UserLoginLbl);
            this.Controls.Add(this.UserNameInputBox);
            this.Controls.Add(this.LoginBtn);
            this.Controls.Add(this.HeadingLbl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "MainLogin";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "AWMS";
            ((System.ComponentModel.ISupportInitialize)(this.loginErrorProvider)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label HeadingLbl;
        private System.Windows.Forms.Button LoginBtn;
        private System.Windows.Forms.TextBox UserNameInputBox;
        private System.Windows.Forms.Label UserLoginLbl;
        private System.Windows.Forms.Label EmailLbl;
        private System.Windows.Forms.Label PassLbl;
        private System.Windows.Forms.ErrorProvider loginErrorProvider;
        private System.Windows.Forms.Label loadingLbl;
        private System.Windows.Forms.TextBox PassInputBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox serverTextBox;
        private System.ComponentModel.BackgroundWorker backgroundWorker;
        private System.Windows.Forms.Button connFileSelectBtn;
        private System.Windows.Forms.OpenFileDialog connFileDialog;
    }
}

