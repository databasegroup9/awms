﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using AMWS.ShelterManagerApp;
using AWMS;
using AWMS.Procs;
using AWMS.Common;

namespace AMWS.DeptManagerApp
{
    public partial class MainDeptManagerApp : Form
    {
        private MainLogin logForm;
        public DataBaseManager Database;

        private bool logout;

        public sm_GetEmployees EmployeesProc;
        public sm_GetDepartments DeptsProc;

        public MainDeptManagerApp(MainLogin lg, DataBaseManager dbm)
        {
            InitializeComponent();
            logForm = lg;
            Database = dbm;
            Database.Close();
            Database.Connect();

            RefreshEmployeesList();
            SetUserAndShelter();  
        }

        private void SetUserAndShelter()
        {
            UserNameLbl.Text = Database.ConnectedUser.FullName;

            var proc = new sm_GetShelterDetails(Database.Connection, Database.ShelterId);
            proc.Execute();

            DeptsProc = new sm_GetDepartments(Database.Connection, Database.ShelterId);
            DeptsProc.Execute();

            ShelterLbl.Text = proc.Result.Name;
            deptLabel.Text = FindDepartment(Database.DeptId).ToString();
        }

        private Department FindDepartment(int? deptId)
        {
            foreach (Department department in DeptsProc.AllDepartments)
            {
                if (department.DepartmentId == deptId)
                {
                    return department;
                }
            }
            return null;
        }

        private void AddDptMngBtn_Click(object sender, EventArgs e)
        {
            try
            {
                var DepMgnPP = new EmployeePP(Database.Connection, this);
                DepMgnPP.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void EditDeptManagerBtn_Click(object sender, EventArgs e)
        {
            if (EmployeesListView.SelectedIndices.Count == 1)
            {
                var employee = FindEmployee(EmployeesListView.SelectedItems[0]);
                var DepMgnPP = new EmployeePP(Database.Connection, employee, this);
                DepMgnPP.Show();
            }
        }

        private User FindEmployee(ListViewItem selectedItem)
        {
            int id = Convert.ToInt32(selectedItem.SubItems[0].Text);
            foreach (User user in EmployeesProc.AllEmployeeList)
            {
                if (user.UserId == id)
                {
                    return user;
                }
            }
            return new User("ERROR","ERROR",UserType.INVALID,-1,0);
        }

        private void DelDeptManagerBtn_Click(object sender, EventArgs e)
        {
            if (EmployeesListView.SelectedIndices.Count == 1)
            {
                var employee = FindEmployee(EmployeesListView.SelectedItems[0]);
                var proc = new sm_DelEmployee(Database.Connection, employee.UserId);
                proc.Execute();
            }
            RefreshEmployeesList();
        }

        private void RefreshEmployeesList()
        {
            EmployeesProc = new sm_GetEmployees(Database.Connection, Database.ShelterId);
            EmployeesProc.Execute();
            
            EmployeesListView.Items.Clear();
            
            string[] itm = new string[3];
            var item = new ListViewItem();

            //Add Dept Manager
            List<DeptManager> list = EmployeesProc.DeptManagerList;
            foreach (DeptManager dm in list)
            {
                if (dm.DeptId == Database.DeptId)
                {
                    itm = new string[3];
                    itm[0] = dm.UserId.ToString();
                    itm[1] = dm.FullName;
                    itm[2] = dm.Dept.Name;

                    item = new ListViewItem(itm) { Group = EmployeesListView.Groups[1] };
                    EmployeesListView.Items.Add(item);
                }
            }

            //Add Employees
            foreach (Employee e in EmployeesProc.EmployeeList)
            {
                if (e.DeptId == Database.DeptId)
                {
                    int groupId;
                    itm = new string[3];
                    itm[0] = e.UserId.ToString();
                    itm[1] = e.FullName;
                    if (e.DeptId == null)
                    {
                        itm[2] = "None";
                        groupId = 3;
                    }
                    else
                    {
                        itm[2] = GetDeptName(e.DeptId);
                        groupId = 2;
                    }

                    item = new ListViewItem(itm) { Group = EmployeesListView.Groups[groupId] };
                    EmployeesListView.Items.Add(item);
                }    
            }
        }

        private string GetDeptName(int? deptId)
        {
            DeptsProc = new sm_GetDepartments(Database.Connection,Database.ShelterId);
            DeptsProc.Execute();
            foreach (Department d in DeptsProc.AllDepartments)
            {
                if (d.DepartmentId == deptId)
                {
                    return d.Name;
                }
            }

            return "None";
        }

        public void OnPropertyPageClosed()
        {
            RefreshEmployeesList();
        }

        private void LogOutBtn_Click(object sender, EventArgs e)
        {
            logForm.ResetLoginForm();
            logForm.Show();
            Database.ConnectedUser = null;
            logout = true;
            Dispose();       
        }      
    } 
}
