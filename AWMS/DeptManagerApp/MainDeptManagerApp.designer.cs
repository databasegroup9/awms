﻿namespace AMWS.DeptManagerApp
{
    partial class MainDeptManagerApp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);

            if (!logout)
            {
                logForm.Dispose();
            }          
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ListViewGroup listViewGroup1 = new System.Windows.Forms.ListViewGroup("Shelter Manager", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup2 = new System.Windows.Forms.ListViewGroup("Department Manager", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup3 = new System.Windows.Forms.ListViewGroup("Employees", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup4 = new System.Windows.Forms.ListViewGroup("Unassigned Employees", System.Windows.Forms.HorizontalAlignment.Left);
            this.label1 = new System.Windows.Forms.Label();
            this.LogOutBtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.UserNameLbl = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.ShelterLbl = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.EmployeesListView = new System.Windows.Forms.ListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader4 = new System.Windows.Forms.ColumnHeader();
            this.DelDeptManagerBtn = new System.Windows.Forms.Button();
            this.EditDeptManagerBtn = new System.Windows.Forms.Button();
            this.AddDptMngBtn = new System.Windows.Forms.Button();
            this.tabDptManagers = new System.Windows.Forms.TabControl();
            this.label4 = new System.Windows.Forms.Label();
            this.deptLabel = new System.Windows.Forms.Label();
            this.tabPage2.SuspendLayout();
            this.tabDptManagers.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(450, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "Loged In As:";
            // 
            // LogOutBtn
            // 
            this.LogOutBtn.Location = new System.Drawing.Point(595, 9);
            this.LogOutBtn.Name = "LogOutBtn";
            this.LogOutBtn.Size = new System.Drawing.Size(75, 23);
            this.LogOutBtn.TabIndex = 2;
            this.LogOutBtn.Text = "Log Out";
            this.LogOutBtn.UseVisualStyleBackColor = true;
            this.LogOutBtn.Click += new System.EventHandler(this.LogOutBtn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(13, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(213, 18);
            this.label2.TabIndex = 3;
            this.label2.Text = "AWMS: Department Manager";
            // 
            // UserNameLbl
            // 
            this.UserNameLbl.AutoSize = true;
            this.UserNameLbl.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UserNameLbl.Location = new System.Drawing.Point(449, 42);
            this.UserNameLbl.Name = "UserNameLbl";
            this.UserNameLbl.Size = new System.Drawing.Size(0, 20);
            this.UserNameLbl.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 20);
            this.label3.TabIndex = 6;
            this.label3.Text = "Shelter:";
            // 
            // ShelterLbl
            // 
            this.ShelterLbl.AutoSize = true;
            this.ShelterLbl.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ShelterLbl.Location = new System.Drawing.Point(99, 29);
            this.ShelterLbl.Name = "ShelterLbl";
            this.ShelterLbl.Size = new System.Drawing.Size(34, 20);
            this.ShelterLbl.TabIndex = 7;
            this.ShelterLbl.Text = "Test";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.Transparent;
            this.tabPage2.Controls.Add(this.EmployeesListView);
            this.tabPage2.Controls.Add(this.DelDeptManagerBtn);
            this.tabPage2.Controls.Add(this.EditDeptManagerBtn);
            this.tabPage2.Controls.Add(this.AddDptMngBtn);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(654, 436);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Employees";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // EmployeesListView
            // 
            this.EmployeesListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader4});
            this.EmployeesListView.FullRowSelect = true;
            listViewGroup1.Header = "Shelter Manager";
            listViewGroup1.Name = "ShelterManagerGroup";
            listViewGroup2.Header = "Department Manager";
            listViewGroup2.Name = "DeptManagersGroup";
            listViewGroup3.Header = "Employees";
            listViewGroup3.Name = "EmployeesGroup";
            listViewGroup4.Header = "Unassigned Employees";
            listViewGroup4.Name = "UnEmployeeGroup";
            this.EmployeesListView.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup1,
            listViewGroup2,
            listViewGroup3,
            listViewGroup4});
            this.EmployeesListView.Location = new System.Drawing.Point(5, 6);
            this.EmployeesListView.MultiSelect = false;
            this.EmployeesListView.Name = "EmployeesListView";
            this.EmployeesListView.Size = new System.Drawing.Size(496, 396);
            this.EmployeesListView.TabIndex = 8;
            this.EmployeesListView.UseCompatibleStateImageBehavior = false;
            this.EmployeesListView.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Employee ID";
            this.columnHeader1.Width = 81;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Name";
            this.columnHeader2.Width = 142;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Department";
            this.columnHeader4.Width = 120;
            // 
            // DelDeptManagerBtn
            // 
            this.DelDeptManagerBtn.Location = new System.Drawing.Point(506, 63);
            this.DelDeptManagerBtn.Name = "DelDeptManagerBtn";
            this.DelDeptManagerBtn.Size = new System.Drawing.Size(142, 23);
            this.DelDeptManagerBtn.TabIndex = 7;
            this.DelDeptManagerBtn.Text = "Delete";
            this.DelDeptManagerBtn.UseVisualStyleBackColor = true;
            this.DelDeptManagerBtn.Click += new System.EventHandler(this.DelDeptManagerBtn_Click);
            // 
            // EditDeptManagerBtn
            // 
            this.EditDeptManagerBtn.Location = new System.Drawing.Point(506, 35);
            this.EditDeptManagerBtn.Name = "EditDeptManagerBtn";
            this.EditDeptManagerBtn.Size = new System.Drawing.Size(142, 23);
            this.EditDeptManagerBtn.TabIndex = 6;
            this.EditDeptManagerBtn.Text = "Edit";
            this.EditDeptManagerBtn.UseVisualStyleBackColor = true;
            this.EditDeptManagerBtn.Click += new System.EventHandler(this.EditDeptManagerBtn_Click);
            // 
            // AddDptMngBtn
            // 
            this.AddDptMngBtn.Location = new System.Drawing.Point(506, 6);
            this.AddDptMngBtn.Name = "AddDptMngBtn";
            this.AddDptMngBtn.Size = new System.Drawing.Size(143, 23);
            this.AddDptMngBtn.TabIndex = 5;
            this.AddDptMngBtn.Text = "Add";
            this.AddDptMngBtn.UseVisualStyleBackColor = true;
            this.AddDptMngBtn.Click += new System.EventHandler(this.AddDptMngBtn_Click);
            // 
            // tabDptManagers
            // 
            this.tabDptManagers.Controls.Add(this.tabPage2);
            this.tabDptManagers.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabDptManagers.Location = new System.Drawing.Point(12, 79);
            this.tabDptManagers.Name = "tabDptManagers";
            this.tabDptManagers.SelectedIndex = 0;
            this.tabDptManagers.Size = new System.Drawing.Size(662, 465);
            this.tabDptManagers.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 49);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 20);
            this.label4.TabIndex = 8;
            this.label4.Text = "Department:";
            // 
            // deptLabel
            // 
            this.deptLabel.AutoSize = true;
            this.deptLabel.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deptLabel.Location = new System.Drawing.Point(99, 49);
            this.deptLabel.Name = "deptLabel";
            this.deptLabel.Size = new System.Drawing.Size(34, 20);
            this.deptLabel.TabIndex = 9;
            this.deptLabel.Text = "Test";
            // 
            // MainDeptManagerApp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(689, 556);
            this.Controls.Add(this.deptLabel);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.ShelterLbl);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.UserNameLbl);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.LogOutBtn);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tabDptManagers);
            this.Name = "MainDeptManagerApp";
            this.Text = "AWMS: Department Manager App";
            this.tabPage2.ResumeLayout(false);
            this.tabDptManagers.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button LogOutBtn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label UserNameLbl;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label ShelterLbl;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ListView EmployeesListView;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.Button DelDeptManagerBtn;
        private System.Windows.Forms.Button EditDeptManagerBtn;
        private System.Windows.Forms.Button AddDptMngBtn;
        private System.Windows.Forms.TabControl tabDptManagers;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label deptLabel;
    }
}