﻿using System;
using Oracle.DataAccess.Client;

namespace AWMS.Procs
{
    public class StoredProcBase
    {
        public OracleConnection Connection;

        public StoredProcBase(OracleConnection c)
        {
            Connection = c;
        }

        public StoredProcBase()
        {
        }

        public virtual void Execute()
        {
            throw new NotImplementedException();
        }
    }
}
