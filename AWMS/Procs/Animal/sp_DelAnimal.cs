﻿using System;
using System.Data;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace AWMS.Procs
{
    public class sp_DelAnimal : StoredProcBase
    {
        private readonly int animalNum;

        public sp_DelAnimal(OracleConnection c, int a)
        {
            Connection = c;
            animalNum = a;
        }

        public override void Execute()
        {
            try
            {
                var com = new OracleCommand("sp_DeleteAnimal", Connection) {CommandType = CommandType.StoredProcedure};
                com.Parameters.Add("a_num", OracleDbType.Int32).Value = animalNum;
                com.Prepare();
                com.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace, "Error!");
            }
        }
    }
}
