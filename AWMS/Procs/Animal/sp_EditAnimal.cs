﻿using System;
using System.Data;
using System.Windows.Forms;
using AWMS.Common;
using Oracle.DataAccess.Client;

namespace AWMS.Procs
{
    public class sp_EditAnimal : StoredProcBase
    {
        private readonly Animal animal;

        public sp_EditAnimal(OracleConnection c, Animal a)
        {
            Connection = c;
            animal = a;
        }

        public override void Execute()
        {
            try
            {
                var com = new OracleCommand("sp_EditAnimal", Connection) {CommandType = CommandType.StoredProcedure};
                com.Parameters.Add("animal_id", OracleDbType.Int32).Value = animal.AnimalNum;
                com.Parameters.Add("p_name", OracleDbType.Varchar2).Value = animal.PetName;
                com.Parameters.Add("fee", OracleDbType.Int32).Value = animal.AdoptionFee;
                com.Parameters.Add("descr", OracleDbType.Varchar2).Value = animal.Description;
                com.Parameters.Add("c_name", OracleDbType.Varchar2).Value = animal.CommonAnimalName;
                com.Parameters.Add("b", OracleDbType.Varchar2).Value = animal.Breed;
                com.Parameters.Add("kept", OracleDbType.Int32).Value = animal.KeptIn;
                com.Parameters.Add("adopted", OracleDbType.Int32).Value = animal.AdoptedBy;
                com.Prepare();
                com.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace, "Error!");
            }
        }
    }
}
