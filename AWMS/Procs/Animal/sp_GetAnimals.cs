﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using AWMS.Common;
using Oracle.DataAccess.Client;

namespace AWMS.Procs
{
    public class sp_GetAnimals : StoredProcBase
    {
        public struct AdoptedAnimal
        {
            public String Customer;
            public int AnimalNum;
            public String PetName;
            public String AnimalType;
        }

        public List<AdoptedAnimal> AdoptedAnimals = new List<AdoptedAnimal>(); 
        public List<Animal> AnimalsList = new List<Animal>();
        public List<int> MyAnimalNumbers = new List<int>();
        public List<int> AnimalsNotCared = new List<int>(); 
        private readonly int deptId;
        private readonly int empId;

        public sp_GetAnimals(OracleConnection c)
        {
            Connection = c;
        }

        public sp_GetAnimals(OracleConnection c, int d, int e)
        {
            Connection = c;
            deptId = d;
            empId = e;
        }

        public override void Execute()
        {
            try
            {
                GetAnimals();
                GetMyAnimalNumbers();
                GetAdoptedAnimals();
                GetAnimalsNotCared();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace, "Error!");
            }
        }

        private void GetAdoptedAnimals()
        {
            try
            {
                var command = new OracleCommand(GetAdoptedAnimalsQuery(), Connection);
                OracleDataReader read = command.ExecuteReader();

                while (read.Read())
                {
                    int id = read.GetInt32(0);
                    string pn = read.GetString(1);
                    string t = read.GetString(2);
                    int cid = read.GetInt32(3);
                    string cn = read.GetString(4);

                    var a = new AdoptedAnimal {Customer = cid + " " + cn, PetName = pn, AnimalType = t, AnimalNum = id};
                    AdoptedAnimals.Add(a);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace, "Error!");
            }
        }

        public void ExecuteAvailableAnimals()
        {
            try
            {
                var command = new OracleCommand(GetAvailableAnimalsQuery(), Connection);
                OracleDataReader read = command.ExecuteReader();

                while (read.Read())
                {
                    int id = read.GetInt32(0);
                    string pn = read.GetString(1);
                    string cn = read.GetString(2);
                    string b = read.GetString(3);
                    int fee = read.GetInt32(4);

                    var a = new Animal(pn, null, cn, b, fee, id, 0);
                    AnimalsList.Add(a);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace, "Error!");
            }
        }

        private void GetAnimals()
        {
            var command = new OracleCommand(GetAnimalsQuery(), Connection);
            OracleDataReader read = command.ExecuteReader();

            while (read.Read())
            {
                int id = read.GetInt32(0);
                string pn = read.GetString(1);
                int fee = read.GetInt32(2);
                string d = read.GetString(3);
                string cn = read.GetString(4);
                string b = read.GetString(5);
                var ad = read.GetValue(6).ToString();

                var a = new Animal(pn,d,cn,b,fee,id,deptId);
                if (!string.IsNullOrEmpty(ad)) a.AdoptedBy = Convert.ToInt32(ad);
                AnimalsList.Add(a);
            }

            read.Close();
        }

        private void GetMyAnimalNumbers()
        {
            var command = new OracleCommand(GetMyAnimalsQuery(), Connection);
            OracleDataReader read = command.ExecuteReader();

            while (read.Read())
            {
                MyAnimalNumbers.Add(read.GetInt32(0));
            }

            read.Close();
        }

        private void GetAnimalsNotCared()
        {
            var command = new OracleCommand(GetAnimalsNotCaredQuery(), Connection);
            OracleDataReader read = command.ExecuteReader();

            while (read.Read())
            {
                AnimalsNotCared.Add(read.GetInt32(0));
            }

            read.Close();
        }

        public string GetAnimalsQuery()
        {
            return "SELECT animal_num,pet_name,adopt_fee,description,common_name,breed,adopted_by " +
                "FROM Animal WHERE adopted_by is null AND kept_in = " + deptId;
        }

        public string GetMyAnimalsQuery()
        {
            return "SELECT animal_num FROM carefor WHERE employee_id = " + empId;
        }

        public string GetAvailableAnimalsQuery()
        {
            return "SELECT animal_num, pet_name, common_name, breed, adopt_fee FROM v_available_animals";
        }

        public string GetAdoptedAnimalsQuery()
        {
            return "SELECT animal_num, pet_name, common_name, cust_id, cust_name FROM animal, customer WHERE cust_id = adopted_by";
        }

        public string GetAnimalsNotCaredQuery()
        {
            return "SELECT animal_num FROM animal WHERE adopted_by is null " +
                   "AND animal_num NOT IN (SELECT animal_num FROM carefor)"+
                   "AND kept_in = " + deptId;
        }
    }
}
