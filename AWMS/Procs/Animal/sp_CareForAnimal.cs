﻿using System;
using System.Data;
using System.Windows.Forms;
using AWMS.Common;
using Oracle.DataAccess.Client;

namespace AWMS.Procs
{
    public class sp_CareForAnimal : StoredProcBase
    {
        private readonly int animalNum;
        private readonly int empId;
        private readonly bool assign;

        public sp_CareForAnimal(OracleConnection c, int a, int e, bool asg)
        {
            Connection = c;
            animalNum = a;
            assign = asg;
            empId = e;
        }

        public override void Execute()
        {
            try
            {
                var com = new OracleCommand(GetAssignQuery(), Connection);
                com.Prepare();
                com.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace, "Error!");
            }
        }
        private string GetAssignQuery()
        {
            if (assign)
            {
                return string.Format("INSERT INTO carefor VALUES ({0},{1})", empId, animalNum);
            }
            return string.Format("DELETE FROM carefor WHERE employee_id = {0} AND animal_num = {1}",empId,animalNum);
        }
    }
}
