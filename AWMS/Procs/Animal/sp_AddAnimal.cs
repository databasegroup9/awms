﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using AWMS.Common;
using Oracle.DataAccess.Client;

namespace AWMS.Procs
{
    public class sp_AddAnimal : StoredProcBase
    {
        private Animal animal;
        public int AnimalNum;

        public sp_AddAnimal(OracleConnection c, Animal a)
        {
            Connection = c;
            animal = a;
        }

        public override void Execute()
        {
            try
            {
                var com = new OracleCommand("sp_AddAnimal", Connection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.Add("animal_id", OracleDbType.Int32).Direction = ParameterDirection.Output;
                com.Parameters.Add("pet_name", OracleDbType.Varchar2).Value = animal.PetName;
                com.Parameters.Add("adopt_fee", OracleDbType.Int32).Value = animal.AdoptionFee;
                com.Parameters.Add("descr", OracleDbType.Varchar2).Value = animal.Description;
                com.Parameters.Add("common_name", OracleDbType.Varchar2).Value = animal.CommonAnimalName;
                com.Parameters.Add("breed", OracleDbType.Varchar2).Value = animal.Breed;
                com.Parameters.Add("kept_in", OracleDbType.Int32).Value = animal.KeptIn;
                com.Prepare();
                com.ExecuteNonQuery();
                AnimalNum = Convert.ToInt32(com.Parameters["animal_id"].Value.ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace, "Error!");
            }
        }
    }
}
