﻿using System;
using System.Windows.Forms;
using Oracle.DataAccess.Client;
using AWMS.Common;

namespace AWMS.Procs
{
    class sm_GetShelterDetails : StoredProcBase
    {
        private int? shelterId;
        public AnimalShelter Result;

        public sm_GetShelterDetails(OracleConnection c, int? s)
        {
            Connection = c;
            shelterId = s;
        }

        public override void Execute()
        {
            try
            {
                var command = new OracleCommand(GenerateQuery(), Connection);
                OracleDataReader read = command.ExecuteReader();
                Result = new AnimalShelter(shelterId == null ? 0 : (int)shelterId);

                read.Read();

                Result.ManagerId = read.GetInt32(0);
                Result.Name = read.GetString(1);
                Result.Location = read.GetString(2);

                read.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        public string GenerateQuery()
        {
            return "SELECT mgr_id,shelter_name,shelter_location FROM ANIMALSHELTER WHERE shelter_num = " + shelterId;
        }
    }
}
