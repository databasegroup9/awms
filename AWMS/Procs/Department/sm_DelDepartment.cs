﻿using System.Data;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace AWMS.Procs
{
    class sm_DelDepartment : StoredProcBase
    {
        private int deptId;
        private int managerId;

        public sm_DelDepartment(OracleConnection c, int deptId, int managerId)
        {
            Connection = c;
            this.deptId = deptId;
            this.managerId = managerId;
        }

        public override void Execute()
        {
            try
            {
                var com = new OracleCommand("sm_DelDepartment", Connection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.Add("dept_id", OracleDbType.Int32).Value = deptId;
                com.Parameters.Add("manager_id", OracleDbType.Int32).Value = managerId;
                com.Prepare();
                com.ExecuteNonQuery();
            }
            catch (OracleException ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
                    
        }
    }
}
