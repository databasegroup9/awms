﻿using System;
using System.Data;
using System.Windows.Forms;
using AWMS.Common;
using Oracle.DataAccess.Client;

namespace AWMS.Procs
{
    class sm_EditDepartment : StoredProcBase
    {
        private String Name;
        private int newManagerId; 
        private int oldManagerId;
        private int deptId;
        private int ext;
        private int deptType;
        private string publicEmail;
        private int publicPhone;
        private string animalType;
        private int vetContact;
        private int oldType;

        public sm_EditDepartment(OracleConnection c, String n, int m, int o, int d, int e, DepartmentType t, int v, string a, DepartmentType ot)
        {
            Setup(c,n,m,o,d,e,t,ot);
            animalType = a;
            vetContact = v;
        }

        public sm_EditDepartment(OracleConnection c, String n, int m, int o, int d, int e, DepartmentType t, string pe, int pp, DepartmentType ot)
        {
            Setup(c, n, m, o, d, e, t, ot);
            publicEmail = pe;
            publicPhone = pp;
        }

        private void Setup(OracleConnection c, String n, int m, int o, int d, int e, DepartmentType t, DepartmentType ot)
        {
            int dt = t == DepartmentType.ANIMAL_CARE ? 0 : 1;
            int odt = ot == DepartmentType.ANIMAL_CARE ? 0 : 1;
            Connection = c;
            Name = n;
            newManagerId = m;
            oldManagerId = o;
            deptId = d;
            ext = e;
            deptType = dt;
            oldType = odt;
        }

        public override void Execute()
        {
            try
            {
                var com = new OracleCommand("sm_EditDepartment", Connection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.Add("dept_id", OracleDbType.Int32).Value = deptId;
                com.Parameters.Add("old_manager_id", OracleDbType.Int32).Value = oldManagerId;
                com.Parameters.Add("new_manager_id", OracleDbType.Int32).Value = newManagerId;
                com.Parameters.Add("d_name", OracleDbType.Varchar2).Value = Name;
                com.Parameters.Add("ext", OracleDbType.Int32).Value = ext;
                com.Parameters.Add("dept_type", OracleDbType.Int32).Value = deptType;
                com.Parameters.Add("pub_phone", OracleDbType.Int32).Value = publicPhone;
                com.Parameters.Add("pub_email", OracleDbType.Varchar2).Value = publicEmail;
                com.Parameters.Add("vet", OracleDbType.Int32).Value = vetContact;
                com.Parameters.Add("animal", OracleDbType.Varchar2).Value = animalType;
                com.Parameters.Add("old_type", OracleDbType.Int32).Value = oldType;
                
                com.Prepare();
                com.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!");
            }
            
        }
    }
}
