﻿using System;
using System.Collections.Generic;
using Oracle.DataAccess.Client;
using AWMS.Common;

namespace AWMS.Procs
{
    public class sm_GetDepartments : StoredProcBase
    {
        private readonly int shelterId;
        public List<AnimalCareDepartment> AnimalCareDepartments;
        public List<CustomerServiceDepartment> CustomerServiceDepartments;
        public List<Department> AllDepartments; 

        public sm_GetDepartments(OracleConnection c, int? s)
        {
            Connection = c;
            shelterId = s == null ? 0 : (int)s;
        }

        public override void Execute()
        {
            try
            {
                AnimalCareDepartments = new List<AnimalCareDepartment>();
                CustomerServiceDepartments = new List<CustomerServiceDepartment>();
                AllDepartments = new List<Department>();

                GetAnimalCareDepartments();
                GetCustomerServiceDepartments();

                AllDepartments.AddRange(AnimalCareDepartments.ToArray());
                AllDepartments.AddRange(CustomerServiceDepartments.ToArray());
            }
            catch (Exception ex)
            {
                Util.ErrorBox(ex.Message + ex.StackTrace);
            }
            
        }

        private void GetCustomerServiceDepartments()
        {
            var command = new OracleCommand(CustomerServiceQuery(), Connection);
            OracleDataReader read = command.ExecuteReader();

            while (read.Read())
            {
                int deptId = read.GetInt32(0);
                string deptName = read.GetString(1);
                int ext = read.GetInt32(2);
                string email = read.GetString(3);
                int ph = read.GetInt32(4);

                var d = new CustomerServiceDepartment(deptId, deptName, ext, email, ph);

                CustomerServiceDepartments.Add(d);
            }
            read.Close();
        }

        private void GetAnimalCareDepartments()
        {
            var command = new OracleCommand(AnimalCareQuery(), Connection);
            OracleDataReader read = command.ExecuteReader();

            while (read.Read())
            {
                int deptId = read.GetInt32(0);
                string deptName = read.GetString(1);
                int ext = read.GetInt32(2);
                string type = read.GetString(3);
                int v = read.GetInt32(4);

                var d = new AnimalCareDepartment(deptId, deptName, ext, type, v);

                AnimalCareDepartments.Add(d);
            }
            read.Close();
        }

        public string AnimalCareQuery()
        {
            return "SELECT d.dept_num,d.dept_name,d.phone_ext,a.animal_type,a.vet_contact "+
                    "FROM department d, animalcaredept a WHERE a.dept_num = d.dept_num AND d.shelter_num =" + shelterId;
        }
        public string CustomerServiceQuery()
        {
            return "SELECT d.dept_num,d.dept_name,d.phone_ext,c.public_email,c.public_phone " +
                    "FROM department d, customerservicedept c WHERE c.dept_num = d.dept_num AND d.shelter_num =" + shelterId;
        }
    }
}
