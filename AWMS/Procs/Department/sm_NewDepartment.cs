﻿using System;
using System.Data;
using System.Windows.Forms;
using AWMS.Common;
using Oracle.DataAccess.Client;
namespace AWMS.Procs
{
    class sm_NewDepartment : StoredProcBase
    {
        private String Name;
        private int managerId;
        private int? shelterId;
        private int phone;
        private DepartmentType deptType;
        private int? publicPhone;
        private string publicEmail;
        private int? vetPhone;
        private string animalType;

        public int Id = 0;

        public sm_NewDepartment(OracleConnection c, String n, int m, int? s, int p, DepartmentType t, string at, int vp)
        {
            Setup(c,n,m,s,p,t);
            animalType = at;
            vetPhone = vp;
        }
        public sm_NewDepartment(OracleConnection c, String n, int m, int? s, int p, DepartmentType t, int pp, string pe)
        {
            Setup(c, n, m, s, p, t);
            publicEmail = pe;
            publicPhone = pp;
        }
        private void Setup(OracleConnection c, String n, int m, int? s, int p, DepartmentType t)
        {
            Connection = c;
            Name = n;
            managerId = m;
            shelterId = s;
            phone = p;
            deptType = t;
        }
        public override void Execute()
        {
            try
            {
                int t = deptType == DepartmentType.CUSTOMER_SERVICE ? 1 : 0;
                var com = new OracleCommand("sm_NewDepartment", Connection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.Add("dept_name", OracleDbType.Varchar2).Value = Name;
                com.Parameters.Add("ext", OracleDbType.Int32).Value = phone;
                com.Parameters.Add("shelter_id", OracleDbType.Int32).Value = shelterId;
                com.Parameters.Add("dept_type", OracleDbType.Int32).Value = t;
                com.Parameters.Add("dept_id", OracleDbType.Int32).Direction = ParameterDirection.Output;
                com.Parameters.Add("mgr_id", OracleDbType.Int32).Value = managerId;
                com.Parameters.Add("pub_phone", OracleDbType.Int32).Value = publicPhone;
                com.Parameters.Add("pub_email", OracleDbType.Varchar2).Value = publicEmail;
                com.Parameters.Add("vet", OracleDbType.Int32).Value = vetPhone;
                com.Parameters.Add("animal", OracleDbType.Varchar2).Value = animalType;
                com.Prepare();
                com.ExecuteNonQuery();
                Id = Convert.ToInt32(com.Parameters["dept_id"].Value.ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!");
                throw;
            }
           
        }
    }
}
