﻿using System;
using System.Data;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace AWMS.Procs
{
    class sm_EditEmployee : StoredProcBase
    {
        private String fullName;
        private int salary;
        private int phone;
        private String email;
        private int? dept;
        private int id;

        public sm_EditEmployee(OracleConnection c, string em, string fn, int s, int ph, int? d, int id)
        {
            Connection = c;
            fullName = fn;
            email = em;
            this.id = id;
            salary = s;
            phone = ph;
            dept = d;
        }

        public override void Execute()
        {
            try
            {
                var com = new OracleCommand("sm_EditEmployee", Connection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.Add("id", OracleDbType.Int32).Value = id;
                com.Parameters.Add("emp_email", OracleDbType.Varchar2).Value = email;
                com.Parameters.Add("emp_name", OracleDbType.Varchar2).Value = fullName;
                com.Parameters.Add("sal", OracleDbType.Int32).Value = salary;
                com.Parameters.Add("ph", OracleDbType.Int32).Value = phone;
                com.Parameters.Add("dept", OracleDbType.Int32).Value = dept;
                com.Prepare();
                com.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!");
            }
       
        }
    }
}
