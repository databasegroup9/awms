﻿using System.Data;
using Oracle.DataAccess.Client;

namespace AWMS.Procs
{
    class sm_DelEmployee : StoredProcBase
    {
        private int id;

        public sm_DelEmployee(OracleConnection c, int id)
        {
            Connection = c;
            this.id = id;
        }

        public override void Execute()
        {
            var command = new OracleCommand("sm_DelEmployee", Connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add("id", OracleDbType.Int32).Value = id;
            command.Prepare();
            command.ExecuteNonQuery();
        }
    }
}
