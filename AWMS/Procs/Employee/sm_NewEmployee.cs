﻿using System;
using System.Data;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace AWMS.Procs
{
    class sm_NewEmployee : StoredProcBase
    {
        private String fullname;
        private String email;
        private String pass;
        private int salary;
        private int phone;
        private int? dept;

        public int Id = 0;

        public sm_NewEmployee(OracleConnection c, String em, String p, String fn, int s, int ph, int? d)
        {
            Connection = c;
            fullname = fn;
            email = em;
            pass = p;
            salary = s;
            phone = ph;
            dept = d;
        }

        public override void Execute()
        {
            try
            {
                var com = new OracleCommand("sm_NewEmployee", Connection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.Add("id", OracleDbType.Int32).Direction = ParameterDirection.Output;
                com.Parameters.Add("emp_email", OracleDbType.Varchar2).Value = email;
                com.Parameters.Add("emp_name", OracleDbType.Varchar2).Value = fullname;
                com.Parameters.Add("sal", OracleDbType.Int32).Value = salary;
                com.Parameters.Add("phone", OracleDbType.Int32).Value = phone;
                com.Parameters.Add("dept", OracleDbType.Int32).Value = dept;
                com.Parameters.Add("pass", OracleDbType.Varchar2).Value = pass;
                com.Prepare();
                com.ExecuteNonQuery();
                Id = Convert.ToInt32(com.Parameters["id"].Value.ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!");
            }

        }
    }
}
