﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using AWMS.Common;
using Oracle.DataAccess.Client;

namespace AWMS.Procs
{
    public class sm_GetEmployees : StoredProcBase
    {
        private int shelterId;
        public List<DeptManager> DeptManagerList;
        public List<Employee> EmployeeList;
        public List<User> AllEmployeeList; 
        public ShelterManager ShelterManager;  

        public sm_GetEmployees(OracleConnection c, int? s)
        {
            Connection = c;
            shelterId = s == null ? 0 : (int)s;
        }

        public override void Execute()
        {
            try
            {
                GetDeptManagers();
                GetEmployees();
                GetShelterManager();

                AllEmployeeList = new List<User> {ShelterManager};
                AllEmployeeList.AddRange(EmployeeList.ToArray());
                AllEmployeeList.AddRange(DeptManagerList.ToArray());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace, "Error!");
            }
        }

        private void GetShelterManager()
        {
            var command = new OracleCommand(GetShelterManagerQuery(), Connection);
            OracleDataReader read = command.ExecuteReader();

            read.Read();

            int id = read.GetInt32(0);
            string fn = read.GetString(1);
            string em = read.GetString(2);
            int ph = read.GetInt32(3);
            int sal = read.GetInt32(4);

            ShelterManager = new ShelterManager(fn, em, id, shelterId,ph,sal);
            
            read.Close();
        }

        private void GetEmployees()
        {
            var command = new OracleCommand(GetEmployeesQuery(), Connection);
            OracleDataReader read = command.ExecuteReader();
            EmployeeList = new List<Employee>();

            while (read.Read())
            {
                int id = read.GetInt32(0);
                string fn = read.GetString(1);
                string em = read.GetString(2);
                int deptId = read.GetInt32(3);
                int ph = read.GetInt32(4);
                int sal = read.GetInt32(5);

                EmployeeList.Add(new Employee(fn, em, deptId, id,sal,ph));
            }
            read.Close();

            command = new OracleCommand(GetEmployeesUnQuery(), Connection);
            read = command.ExecuteReader();

            while (read.Read())
            {
                int id = read.GetInt32(0);
                string fn = read.GetString(1);
                string em = read.GetString(2);
                int ph = read.GetInt32(4);
                int sal = read.GetInt32(5);

                EmployeeList.Add(new Employee(fn, em, null, id,sal,ph));
            }
            read.Close();
        }

        private void GetDeptManagers()
        {
            var command = new OracleCommand(GetDeptManagersQuery(), Connection);
            OracleDataReader read = command.ExecuteReader();
            DeptManagerList = new List<DeptManager>();

            while (read.Read())
            {
                int id = read.GetInt32(0);
                string fn = read.GetString(1);
                string em = read.GetString(2);
                int ph = read.GetInt32(3);
                int sal = read.GetInt32(4);

                int deptId = read.GetInt32(5);
                string deptName = read.GetString(6);
                int phone = read.GetInt32(7);

                var d = new Department(deptId, deptName, phone, DepartmentType.ANIMAL_CARE);

                DeptManagerList.Add(new DeptManager(fn, em, id, d,sal,ph));
            }
            read.Close();
        }

        public string GetDeptManagersQuery()
        {
            return "SELECT dm.emp_id,dm.full_name,dm.email,dm.phone,dm.salary,d.dept_num,d.dept_name,d.phone_ext FROM employee dm, department d WHERE d.shelter_num = "+ 
                shelterId +" AND dm.dept_num = d.dept_num AND dm.isdeptmanager = 1" ;
        }
        private string GetEmployeesQuery()
        {
            return "SELECT e.emp_id,e.full_name,e.email,d.dept_num,e.phone,e.salary FROM employee e, department d WHERE d.shelter_num = " +
                shelterId + " AND e.dept_num = d.dept_num AND e.isdeptmanager = 0";
        }
        private string GetEmployeesUnQuery()
        {
            return "SELECT e.emp_id,e.full_name,e.email,e.dept_num,e.phone,e.salary FROM employee e WHERE e.dept_num is null " +
                    "AND e.emp_id not in (SELECT mgr_id FROM animalshelter)";
        }

        private string GetShelterManagerQuery()
        {
            return "SELECT e.emp_id,e.full_name,e.email,e.phone,e.salary FROM employee e, animalshelter a " +
                    "WHERE a.mgr_id = e.emp_id " +    
                    "AND a.shelter_num = " + shelterId;
        }
    }
}
