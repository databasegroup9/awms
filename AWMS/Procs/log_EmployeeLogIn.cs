﻿using System;
using AWMS.Common;
using Oracle.DataAccess.Client;

namespace AWMS.Procs
{
    class log_EmployeeLogIn : StoredProcBase
    {
        private String email;
        private String pass;
        private int isDeptManager;

        public ResultStruct Result;

        public struct ResultStruct
        {
            public int EM_ID;
            public string EM_EMAIL;
            public string EM_FULL_NAME;
            public UserType EM_TYPE;
            public int? EM_DEPT_ID;
            public int? EM_SHELTER_ID;
            public String EM_ORG_NAME;
        }

        public log_EmployeeLogIn(OracleConnection c, String em, String p)
        {
            Connection = c;
            email = em;
            pass = p;
        }

        public override void Execute()
        {
            Result = new ResultStruct();
            GetEmployeeInformation();

            //Throw Incorrect Login Details
            if (Result.EM_ID == -1)
            {
                //LoginAttemptFailed();
                return;
            }
            
            if (Result.EM_DEPT_ID.HasValue)
            {
                GetShelterInfo();
            }
            
            if (isShelterMgr())
            {
                Result.EM_TYPE = UserType.SHELTER_MANAGER;
            }
            else if (isDeptManager == 1)
            {
                Result.EM_TYPE = UserType.DEPT_MANAGER;
            }
            else
            {
                Result.EM_TYPE = UserType.EMPLOYEE;
            }

            if (Result.EM_TYPE == UserType.EMPLOYEE && !Result.EM_DEPT_ID.HasValue)
            {
                // Raise employee not assigned to a department error
                Result.EM_ID = -2;
                //LoginAttemptFailed();
                //return;
            }
            LoginAttemptPassed();

        }

        private void LoginAttemptFailed()
        {
            throw new NotImplementedException();
        }
        private void LoginAttemptPassed()
        {
            var command = new OracleCommand(GenerateLoginAttemptQuery(), Connection);
            command.ExecuteNonQuery();
        }

        private void GetEmployeeInformation()
        {
            var command = new OracleCommand(GenerateEmployeeInfoQuery(), Connection);
            OracleDataReader read = command.ExecuteReader();

            if (!read.Read())
            {
                //Incorect Login
                Result.EM_ID = -1;
            }
            else
            {
                Result.EM_ID = read.GetInt32(0);
                Result.EM_EMAIL = read.GetString(1);
                Result.EM_FULL_NAME = read.GetString(2);
                Result.EM_DEPT_ID = read.IsDBNull(3) ? (int?) null : read.GetInt32(3);
                isDeptManager = read.GetInt32(4);
            }
            read.Close();
        }

        private void GetShelterInfo()
        {
            var command = new OracleCommand(GenerateShelterIdQuery(), Connection);
            OracleDataReader read = command.ExecuteReader();
            if (!read.Read())
            {
                Result.EM_SHELTER_ID = null;
            }
            else
            {
                Result.EM_SHELTER_ID = read.IsDBNull(0) ? (int?)null : read.GetInt32(0);
            }
            read.Close();           
        }
        
        public string GenerateEmployeeInfoQuery()
        {
            return "SELECT emp_id , email , full_name, dept_num, isdeptmanager FROM employee " +
                        "WHERE email = '" + email + "' AND emp_password = '" + pass + "'";
        }
        public string GenerateLoginAttemptQuery()
        {
            return string.Format("INSERT INTO loginattempt VALUES ({0},"+
                "log_seq.NEXTVAL," +
                "TO_DATE('{1}','DD/MM/YYYY HH24:MI:SS'),{2})", 
                Result.EM_ID, DateTime.Now, 1);
        }
        public string GenerateShelterIdQuery()
        {
            return "SELECT shelter_num FROM department WHERE dept_num = " + Result.EM_DEPT_ID;
        }
        public string CheckShelterMgrQuery()
        {
            return "SELECT shelter_num, org_name FROM animalshelter WHERE mgr_id = " + Result.EM_ID;
        }

        private Boolean isShelterMgr()
        {
            var command = new OracleCommand(CheckShelterMgrQuery(), Connection);
            OracleDataReader read = command.ExecuteReader();
            bool result = read.Read();
            if (result)
            {
                Result.EM_SHELTER_ID = read.GetInt32(0);
                Result.EM_ORG_NAME = read.GetString(1);
            }
            read.Close();
            return result;
        }
    }

}
