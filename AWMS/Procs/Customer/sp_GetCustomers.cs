﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using AWMS.Common;
using Oracle.DataAccess.Client;

namespace AWMS.Procs
{
    public class sp_GetCustomers : StoredProcBase
    {
        public List<Customer> CustomersList = new List<Customer>();
        public List<int> DeptCustomerNumbers = new List<int>();
        private readonly int deptId;

        public sp_GetCustomers(OracleConnection c, int d)
        {
            Connection = c;
            deptId = d;
        }

        public override void Execute()
        {
            try
            {
                GetCustomers();
                GetServedCustomers();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace, "Error!");
            }
        }

        private void GetCustomers()
        {
            var command = new OracleCommand(GetCustomersQuery(), Connection);
            OracleDataReader read = command.ExecuteReader();

            while (read.Read())
            {
                int id = read.GetInt32(0);
                string e = read.GetString(1);
                string cn = read.GetString(2);
                int age = read.GetInt32(3);
                string ad = read.GetString(4);

                var c = new Customer(id,age,e,cn,ad);
                CustomersList.Add(c);
            }

            read.Close();
        }

        private void GetServedCustomers()
        {
            var command = new OracleCommand(GetServedCustomersQuery(), Connection);
            OracleDataReader read = command.ExecuteReader();

            while (read.Read())
            {
                DeptCustomerNumbers.Add(read.GetInt32(0));
            }

            read.Close();
        }

        public string GetCustomersQuery()
        {
            return "SELECT cust_id,cust_email,cust_name,cust_age,cust_address " +
                   "FROM Customer";
        }

        public string GetServedCustomersQuery()
        {
            return "SELECT cust_id FROM serves WHERE dept_num = " + deptId;
        }
    }
}
