﻿using System;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace AWMS.Procs
{
    public class sp_ServesCustomer : StoredProcBase
    {
        private readonly int customerId;
        private readonly int deptId;

        public sp_ServesCustomer(OracleConnection c, int cust, int d)
        {
            Connection = c;
            customerId = cust;
            deptId = d;
        }

        public override void Execute()
        {
            try
            {
                var com = new OracleCommand(GetAssignQuery(), Connection);
                com.Prepare();
                com.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace, "Error!");
            }
        }

        private string GetAssignQuery()
        {
            return string.Format("INSERT INTO serves VALUES ({0},{1})", customerId, deptId);
        }
    }
}
