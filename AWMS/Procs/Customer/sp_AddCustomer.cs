﻿using System;
using System.Data;
using System.Windows.Forms;
using AWMS.Common;
using Oracle.DataAccess.Client;

namespace AWMS.Procs
{
    public class sp_AddCustomer : StoredProcBase
    {
        private Customer customer;
        public int CustomerId;

        public sp_AddCustomer(OracleConnection c, Customer cust)
        {
            Connection = c;
            customer = cust;
        }

        public override void Execute()
        {
            try
            {
                var com = new OracleCommand("sp_AddCustomer", Connection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.Add("cust_id", OracleDbType.Int32).Direction = ParameterDirection.Output;
                com.Parameters.Add("cust_email", OracleDbType.Varchar2).Value = customer.Email;
                com.Parameters.Add("cust_name", OracleDbType.Varchar2).Value = customer.Name;
                com.Parameters.Add("cust_age", OracleDbType.Int32).Value = customer.Age;
                com.Parameters.Add("cust_address", OracleDbType.Varchar2).Value = customer.Address;
                com.Prepare();
                com.ExecuteNonQuery();
                CustomerId = Convert.ToInt32(com.Parameters["cust_id"].Value.ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace, "Error!");
            }
        }
    }
}
