﻿using System;
using System.Data;
using System.Windows.Forms;
using AWMS.Common;
using Oracle.DataAccess.Client;

namespace AWMS.Procs
{
    public class sp_EditCustomer : StoredProcBase
    {
        private readonly Customer customer;

        public sp_EditCustomer(OracleConnection c, Customer cust)
        {
            Connection = c;
            customer = cust;
        }

        public override void Execute()
        {
            try
            {
                var com = new OracleCommand("sp_EditCustomer", Connection) {CommandType = CommandType.StoredProcedure};
                com.Parameters.Add("c_id", OracleDbType.Int32).Value = customer.Id;
                com.Parameters.Add("c_email", OracleDbType.Varchar2).Value = customer.Email;
                com.Parameters.Add("c_name", OracleDbType.Varchar2).Value = customer.Name;
                com.Parameters.Add("c_age", OracleDbType.Int32).Value = customer.Age;
                com.Parameters.Add("c_address", OracleDbType.Varchar2).Value = customer.Address;
                com.Prepare();
                com.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace, "Error!");
            }
        }
    }
}
