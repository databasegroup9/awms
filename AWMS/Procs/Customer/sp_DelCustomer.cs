﻿using System;
using System.Data;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace AWMS.Procs
{
    public class sp_DelCustomer : StoredProcBase
    {
        private readonly int customerId;

        public sp_DelCustomer(OracleConnection c, int id)
        {
            Connection = c;
            customerId = id;
        }

        public override void Execute()
        {
            try
            {
                var com = new OracleCommand("sp_DeleteCustomer", Connection) { CommandType = CommandType.StoredProcedure };
                com.Parameters.Add("c_num", OracleDbType.Int32).Value = customerId;
                com.Prepare();
                com.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace, "Error!");
            }
        }
    }
}
