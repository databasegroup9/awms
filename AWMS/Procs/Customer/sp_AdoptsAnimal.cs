﻿using System;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace AWMS.Procs
{
    public class sp_AdoptsAnimal : StoredProcBase
    {
        private readonly int customerId;
        private readonly int animalNum;
        public int Result = 1;

        public sp_AdoptsAnimal(OracleConnection c, int cust, int animal)
        {
            Connection = c;
            customerId = cust;
            animalNum = animal;
        }

        public override void Execute()
        {
            try
            {
                var com = new OracleCommand(GetAssignQuery(), Connection);
                com.Prepare();
                com.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace, "Error!");
                Result = -1;
            }
        }

        private string GetAssignQuery()
        {
            return string.Format("UPDATE animal SET adopted_by = {0} WHERE animal_num = {1}", customerId, animalNum);
        }
    }
}
