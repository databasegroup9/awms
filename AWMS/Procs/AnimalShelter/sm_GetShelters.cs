﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using AWMS.Common;
using Oracle.DataAccess.Client;

namespace AWMS.Procs
{
    public class sm_GetShelters : StoredProcBase
    {
        private String org_name;
        public List<AnimalShelterManager> ShelterManagers;
        public List<AnimalShelter> Shelters;

        public sm_GetShelters(OracleConnection c, String o)
        {
            Connection = c;
            org_name = o;
        }

        public override void Execute()
        {
            try
            {
                GetShelters();
                GetShelterManagers();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace, "Error!");
            }
        }

        private void GetShelterManagers()
        {
            var command = new OracleCommand(GetShelterManagersQuery(), Connection);
            OracleDataReader read = command.ExecuteReader();
            ShelterManagers = new List<AnimalShelterManager>();

            while (read.Read())
            {
                int id = read.GetInt32(0);
                string fn = read.GetString(1);

                ShelterManagers.Add(new AnimalShelterManager
                    {
                        Id = id,
                        Name = fn
                    });
            }
            read.Close();
        }

        private void GetShelters()
        {
            var command = new OracleCommand(GetSheltersQuery(), Connection);
            OracleDataReader read = command.ExecuteReader();
            Shelters = new List<AnimalShelter>();

            while (read.Read())
            {
                int id = read.GetInt32(0);
                int mid = read.GetInt32(1);
                string org = read.GetString(2);
                string l = read.GetString(3);
                string n = read.GetString(4);

                Shelters.Add(new AnimalShelter(id, n, mid, l, org));
            }
            read.Close();
        }

        public string GetSheltersQuery()
        {
            return "SELECT shelter_num, mgr_id, org_name, shelter_location, shelter_name FROM AnimalShelter "
                + "WHERE org_name = '" + org_name +"'";
        }
        private string GetShelterManagersQuery()
        {
            return "SELECT e.emp_id,e.full_name FROM employee e, animalshelter a "+
                "WHERE a.mgr_id = e.emp_id AND a.org_name = '" + org_name + "'";
        }
    }
    public class AnimalShelterManager
    {
        public int Id;
        public string Name;

        public override string ToString()
        {
            return Id + " - " + Name;
        }
    }
}
