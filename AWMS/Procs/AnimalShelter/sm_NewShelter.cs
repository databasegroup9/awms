﻿using System;
using System.Data;
using System.Windows.Forms;
using AWMS.Common;
using Oracle.DataAccess.Client;

namespace AWMS.Procs
{
    class sm_NewShelter : StoredProcBase
    {
        private AnimalShelter shelter;
        public int Id = 0;

        public sm_NewShelter(OracleConnection c, AnimalShelter a)
        {
            Connection = c;
            shelter =  a;
        }

        public override void Execute()
        {
            try
            {
                var com = new OracleCommand("sm_NewShelter", Connection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.Add("id", OracleDbType.Int32).Direction = ParameterDirection.Output;
                com.Parameters.Add("mgr", OracleDbType.Int32).Value = shelter.ManagerId;
                com.Parameters.Add("org", OracleDbType.Varchar2).Value = shelter.Organisation;
                com.Parameters.Add("location", OracleDbType.Varchar2).Value = shelter.Location;
                com.Parameters.Add("name", OracleDbType.Varchar2).Value = shelter.Name;
                com.Prepare();
                com.ExecuteNonQuery();
                Id = Convert.ToInt32(com.Parameters["id"].Value.ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!");
            }

        }
    }
}
