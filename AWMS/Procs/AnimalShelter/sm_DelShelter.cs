﻿using System.Data;
using Oracle.DataAccess.Client;

namespace AWMS.Procs
{
    class sm_DelShelter : StoredProcBase
    {
        private int id;

        public sm_DelShelter(OracleConnection c, int id)
        {
            Connection = c;
            this.id = id;
        }

        public override void Execute()
        {
            var command = new OracleCommand("sm_DelShelter", Connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add("id", OracleDbType.Int32).Value = id;
            command.Prepare();
            command.ExecuteNonQuery();
        }
    }
}
