﻿using System;
using System.Data;
using System.Windows.Forms;
using AWMS.Common;
using Oracle.DataAccess.Client;

namespace AWMS.Procs
{
    class sm_EditShelter : StoredProcBase
    {
        private AnimalShelter shelter;

        public sm_EditShelter(OracleConnection c, AnimalShelter a)
        {
            Connection = c;
            shelter = a;
        }

        public override void Execute()
        {
            try
            {
                var com = new OracleCommand("sm_EditShelter", Connection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.Add("id", OracleDbType.Int32).Value = shelter.ShelterId;
                com.Parameters.Add("mgr", OracleDbType.Int32).Value = shelter.ManagerId;
                com.Parameters.Add("location", OracleDbType.Varchar2).Value = shelter.Location;
                com.Parameters.Add("name", OracleDbType.Varchar2).Value = shelter.Name;
                com.Prepare();
                com.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!");
            }
       
        }
    }
}
