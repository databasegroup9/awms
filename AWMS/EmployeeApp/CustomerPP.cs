﻿using System;
using System.Windows.Forms;
using AWMS.Procs;
using Oracle.DataAccess.Client;
using AWMS.Common;

namespace AMWS.EmployeeApp
{
    public partial class CustomerPP : Form
    {
        private OracleConnection connection;
        private Customer customer;
        private EmployeeMainApp parentForm;

        public CustomerPP(OracleConnection c, EmployeeMainApp pf)
        {
            //New Customer
            InitializeComponent();
            connection = c;
            parentForm = pf;
        }

        public CustomerPP(OracleConnection c, Customer cust, EmployeeMainApp pf)
        {
            InitializeComponent();
            connection = c;
            customer = cust;
            parentForm = pf;
            RefreshFields();
        }


        private void OkBtn_Click(object sender, EventArgs e)
        {
            if(Save()) Close();
        }

        private bool Save()
        {
            if(!ValidateFields()) return false;

            try
            {
                String n = CustNameInput.Text.Trim();
                String e = CustEmailInput.Text.Trim();
                String a = AddressInput.Text.Trim();
                int age = Convert.ToInt32(AgeInput.Text.Trim());
                var c = new Customer(null,age,e,n,a);
                
                if (customer == null)
                {
                    var proc = new sp_AddCustomer(connection, c);
                    proc.Execute();
                    c.Id = proc.CustomerId;
                    customer = c;
                }
                else
                {
                    c.Id = customer.Id;
                    var proc = new sp_EditCustomer(connection, c);
                    proc.Execute();
                    customer = c;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }

        private bool ValidateFields()
        {
            errorProvider.Clear();

            bool valid = true;
            //Empty Fields
            if (String.IsNullOrEmpty(CustNameInput.Text.Trim()))
            {
                errorProvider.SetError(CustNameInput,"Pet Name must not be empty.");
                valid = false;
            }

            if (String.IsNullOrEmpty(CustEmailInput.Text.Trim()))
            {
                errorProvider.SetError(CustEmailInput, "Common Type must not be empty.");
                valid = false;
            }

            if (String.IsNullOrEmpty(AddressInput.Text.Trim()))
            {
                errorProvider.SetError(AddressInput, "Breed must not be empty.");
                valid = false;
            }

            if (String.IsNullOrEmpty(AgeInput.Text.Trim()))
            {
                errorProvider.SetError(AgeInput, "Adoption Fee must not be empty.");
                valid = false;
            }
            else if (!Util.IsValidNumber(AgeInput.Text.Trim()))
            {
                errorProvider.SetError(AgeInput, "Adoption Fee be a valid number.");
                valid = false;
            }

            return valid;
        }

        

        private void ApplyBtn_Click(object sender, EventArgs e)
        {
            if (!Save()) return;
            RefreshFields();
            OnCloseParentForm();
        }

        private void OnCloseParentForm()
        {
                parentForm.OnPropertyPageClosed();
        }

        private void CancelBtn_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void RefreshFields()
        {
            TitleLbl.Text = "Editing Customer: " + customer.Id + " - " + customer.Name;
            CustNameInput.Text = customer.Name;
            AgeInput.Text = customer.Age.ToString();
            CustEmailInput.Text = customer.Email;
            AddressInput.Text = customer.Address;
        }

        private void CustomerPP_FormClosed(object sender, FormClosedEventArgs e)
        {
            OnCloseParentForm();
        }
    }
}
