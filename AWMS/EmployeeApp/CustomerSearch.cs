﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using AMWS.EmployeeApp;
using AWMS.Common;
using AWMS.Procs;

namespace AWMS.EmployeeApp
{
    public partial class CustomerSearch : Form
    {
        private EmployeeMainApp parentForm;
        private List<Customer> customerList;

        public CustomerSearch(EmployeeMainApp pf)
        {
            InitializeComponent();
            parentForm = pf;
            customerList = pf.CustomersProc.CustomersList;
            AddCutomerBtn.Enabled = false;
            RefreshListAll();
        }

        private void RefreshListAll()
        {
            parentForm.CustomersProc = new sp_GetCustomers(parentForm.Database.Connection, (int)parentForm.Database.DeptId);
            parentForm.CustomersProc.Execute();

            customerSearchListView.Items.Clear();

            var item = new ListViewItem();

            //Add All Customer Not Already Served
            customerList = parentForm.CustomersProc.CustomersList;
            //Remove Served Customers
            foreach (Customer c in parentForm.CustomersServed)
            {
                int index = FindCustomerIndex((int)c.Id);
                customerList.RemoveAt(index);
            }
            foreach (Customer c in customerList)
            {
                string[] itm = new string[5];
                itm[0] = c.Id.ToString();
                itm[1] = c.Name;
                itm[2] = c.Email;
                itm[3] = c.Address;
                itm[4] = c.Age.ToString();

                item = new ListViewItem(itm);
                customerSearchListView.Items.Add(item);
            }
        }

        private int FindCustomerIndex(int id)
        {
            foreach (Customer c in customerList)
            {
                if (c.Id == id)
                    return customerList.IndexOf(c);
            }

            return -1;
        }

        private void RefreshButton_Click(object sender, System.EventArgs e)
        {
            RefreshListAll();
        }

        private void SearchBox_TextChanged(object sender, System.EventArgs e)
        {
            RefreshListSearch();
        }

        private void RefreshListSearch()
        {
            customerSearchListView.Items.Clear();

            var item = new ListViewItem();

            foreach (Customer c in customerList)
            {
                if (!CheckSearch(c)) continue;

                string[] itm = new string[5];
                itm[0] = c.Id.ToString();
                itm[1] = c.Name;
                itm[2] = c.Email;
                itm[3] = c.Address;
                itm[4] = c.Age.ToString();

                item = new ListViewItem(itm);
                customerSearchListView.Items.Add(item);
            }
        }

        private bool CheckSearch(Customer customer)
        {
            if (SearchBox.Text.Trim().Equals(customer.Id.ToString()))
            {
                return true;
            }

            if (customer.Name.ToLower().Contains(SearchBox.Text.Trim().ToLower()))
            {
                return true;
            }

            if (customer.Email.ToLower().Contains(SearchBox.Text.Trim().ToLower()))
            {
                return true;
            }

            if (customer.Address.ToLower().Contains(SearchBox.Text.Trim().ToLower()))
            {
                return true;
            }

            return false;
        }

        private void CancelBtn_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        private void CustomerSearch_FormClosing(object sender, FormClosingEventArgs e)
        {
            parentForm.OnPropertyPageClosed();
        }

        private void customerSearchListView_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            AddCutomerBtn.Enabled = customerSearchListView.SelectedItems.Count == 1;
        }

        private void NewCustBtn_Click(object sender, System.EventArgs e)
        {
            var pp = new CustomerPP(parentForm.Database.Connection, parentForm);
            pp.ShowDialog();
            Close();
        }

        private void AddCutomerBtn_Click(object sender, System.EventArgs e)
        {
            int c = Convert.ToInt32(customerSearchListView.SelectedItems[0].SubItems[0].Text.Trim());
            var proc = new sp_ServesCustomer(parentForm.Database.Connection, c, parentForm.Department.DepartmentId);
            proc.Execute();
            Close();
        }
    }
}
