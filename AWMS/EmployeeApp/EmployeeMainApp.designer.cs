﻿namespace AMWS.EmployeeApp
{
    partial class EmployeeMainApp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);

            if (!logout)
            {
                logForm.Dispose();
            }          
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ListViewGroup listViewGroup1 = new System.Windows.Forms.ListViewGroup("Animal In My Care", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup2 = new System.Windows.Forms.ListViewGroup("Available Animals", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup3 = new System.Windows.Forms.ListViewGroup("Animals Not Cared For", System.Windows.Forms.HorizontalAlignment.Left);
            this.label1 = new System.Windows.Forms.Label();
            this.LogOutBtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.UserNameLbl = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.ShelterLbl = new System.Windows.Forms.Label();
            this.animalsTabPage = new System.Windows.Forms.TabPage();
            this.UnassignBtn = new System.Windows.Forms.Button();
            this.AssignBtn = new System.Windows.Forms.Button();
            this.AnimalsListView = new System.Windows.Forms.ListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader4 = new System.Windows.Forms.ColumnHeader();
            this.BreedColumn = new System.Windows.Forms.ColumnHeader();
            this.DelAnimalBtn = new System.Windows.Forms.Button();
            this.EditAnimalBtn = new System.Windows.Forms.Button();
            this.AddAnimalBtn = new System.Windows.Forms.Button();
            this.tabsEmployees = new System.Windows.Forms.TabControl();
            this.customerTabPage = new System.Windows.Forms.TabPage();
            this.CustomerListView = new System.Windows.Forms.ListView();
            this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader5 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader6 = new System.Windows.Forms.ColumnHeader();
            this.DelCustomerBtn = new System.Windows.Forms.Button();
            this.EditCustomerBtn = new System.Windows.Forms.Button();
            this.AddCustomerBtn = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.deptLabel = new System.Windows.Forms.Label();
            this.AdoptBtn = new System.Windows.Forms.Button();
            this.adoptedTabPage = new System.Windows.Forms.TabPage();
            this.columnHeader7 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader8 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader9 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader10 = new System.Windows.Forms.ColumnHeader();
            this.adoptedAnimalsListView = new System.Windows.Forms.ListView();
            this.columnHeader11 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader12 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader13 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader14 = new System.Windows.Forms.ColumnHeader();
            this.animalsTabPage.SuspendLayout();
            this.tabsEmployees.SuspendLayout();
            this.customerTabPage.SuspendLayout();
            this.adoptedTabPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(481, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "Loged In As:";
            // 
            // LogOutBtn
            // 
            this.LogOutBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LogOutBtn.Location = new System.Drawing.Point(626, 9);
            this.LogOutBtn.Name = "LogOutBtn";
            this.LogOutBtn.Size = new System.Drawing.Size(75, 23);
            this.LogOutBtn.TabIndex = 2;
            this.LogOutBtn.Text = "Log Out";
            this.LogOutBtn.UseVisualStyleBackColor = true;
            this.LogOutBtn.Click += new System.EventHandler(this.LogOutBtn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(13, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(135, 18);
            this.label2.TabIndex = 3;
            this.label2.Text = "AWMS: Employee";
            // 
            // UserNameLbl
            // 
            this.UserNameLbl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.UserNameLbl.AutoSize = true;
            this.UserNameLbl.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UserNameLbl.Location = new System.Drawing.Point(480, 42);
            this.UserNameLbl.Name = "UserNameLbl";
            this.UserNameLbl.Size = new System.Drawing.Size(0, 20);
            this.UserNameLbl.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 37);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 20);
            this.label3.TabIndex = 6;
            this.label3.Text = "Shelter:";
            // 
            // ShelterLbl
            // 
            this.ShelterLbl.AutoSize = true;
            this.ShelterLbl.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ShelterLbl.Location = new System.Drawing.Point(99, 37);
            this.ShelterLbl.Name = "ShelterLbl";
            this.ShelterLbl.Size = new System.Drawing.Size(34, 20);
            this.ShelterLbl.TabIndex = 7;
            this.ShelterLbl.Text = "Test";
            // 
            // animalsTabPage
            // 
            this.animalsTabPage.BackColor = System.Drawing.Color.Transparent;
            this.animalsTabPage.Controls.Add(this.UnassignBtn);
            this.animalsTabPage.Controls.Add(this.AssignBtn);
            this.animalsTabPage.Controls.Add(this.AnimalsListView);
            this.animalsTabPage.Controls.Add(this.DelAnimalBtn);
            this.animalsTabPage.Controls.Add(this.EditAnimalBtn);
            this.animalsTabPage.Controls.Add(this.AddAnimalBtn);
            this.animalsTabPage.Location = new System.Drawing.Point(4, 25);
            this.animalsTabPage.Name = "animalsTabPage";
            this.animalsTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.animalsTabPage.Size = new System.Drawing.Size(676, 408);
            this.animalsTabPage.TabIndex = 1;
            this.animalsTabPage.Text = "Animals";
            this.animalsTabPage.UseVisualStyleBackColor = true;
            // 
            // UnassignBtn
            // 
            this.UnassignBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.UnassignBtn.Location = new System.Drawing.Point(527, 64);
            this.UnassignBtn.Name = "UnassignBtn";
            this.UnassignBtn.Size = new System.Drawing.Size(143, 25);
            this.UnassignBtn.TabIndex = 10;
            this.UnassignBtn.Text = "Unassign";
            this.UnassignBtn.UseVisualStyleBackColor = true;
            this.UnassignBtn.Click += new System.EventHandler(this.UnassignBtn_Click);
            // 
            // AssignBtn
            // 
            this.AssignBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.AssignBtn.Location = new System.Drawing.Point(527, 64);
            this.AssignBtn.Name = "AssignBtn";
            this.AssignBtn.Size = new System.Drawing.Size(143, 25);
            this.AssignBtn.TabIndex = 9;
            this.AssignBtn.Text = "Assign To Me";
            this.AssignBtn.UseVisualStyleBackColor = true;
            this.AssignBtn.Click += new System.EventHandler(this.AssignBtn_Click);
            // 
            // AnimalsListView
            // 
            this.AnimalsListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.AnimalsListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader4,
            this.BreedColumn});
            this.AnimalsListView.FullRowSelect = true;
            listViewGroup1.Header = "Animal In My Care";
            listViewGroup1.Name = "MyAnimals";
            listViewGroup2.Header = "Available Animals";
            listViewGroup2.Name = "AvAnimals";
            listViewGroup3.Header = "Animals Not Cared For";
            listViewGroup3.Name = "ntAnimals";
            this.AnimalsListView.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup1,
            listViewGroup2,
            listViewGroup3});
            this.AnimalsListView.Location = new System.Drawing.Point(5, 6);
            this.AnimalsListView.MultiSelect = false;
            this.AnimalsListView.Name = "AnimalsListView";
            this.AnimalsListView.Size = new System.Drawing.Size(507, 396);
            this.AnimalsListView.TabIndex = 8;
            this.AnimalsListView.UseCompatibleStateImageBehavior = false;
            this.AnimalsListView.View = System.Windows.Forms.View.Details;
            this.AnimalsListView.SelectedIndexChanged += new System.EventHandler(this.AnimalsListView_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Animal Num";
            this.columnHeader1.Width = 72;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Pet Name";
            this.columnHeader2.Width = 142;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Type";
            this.columnHeader4.Width = 120;
            // 
            // BreedColumn
            // 
            this.BreedColumn.Text = "Breed";
            // 
            // DelAnimalBtn
            // 
            this.DelAnimalBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DelAnimalBtn.Location = new System.Drawing.Point(527, 95);
            this.DelAnimalBtn.Name = "DelAnimalBtn";
            this.DelAnimalBtn.Size = new System.Drawing.Size(142, 23);
            this.DelAnimalBtn.TabIndex = 7;
            this.DelAnimalBtn.Text = "Delete";
            this.DelAnimalBtn.UseVisualStyleBackColor = true;
            this.DelAnimalBtn.Click += new System.EventHandler(this.DelAnimalBtn_Click);
            // 
            // EditAnimalBtn
            // 
            this.EditAnimalBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.EditAnimalBtn.Location = new System.Drawing.Point(527, 35);
            this.EditAnimalBtn.Name = "EditAnimalBtn";
            this.EditAnimalBtn.Size = new System.Drawing.Size(142, 23);
            this.EditAnimalBtn.TabIndex = 6;
            this.EditAnimalBtn.Text = "Edit";
            this.EditAnimalBtn.UseVisualStyleBackColor = true;
            this.EditAnimalBtn.Click += new System.EventHandler(this.EditAnimalBtn_Click);
            // 
            // AddAnimalBtn
            // 
            this.AddAnimalBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.AddAnimalBtn.Location = new System.Drawing.Point(527, 6);
            this.AddAnimalBtn.Name = "AddAnimalBtn";
            this.AddAnimalBtn.Size = new System.Drawing.Size(143, 23);
            this.AddAnimalBtn.TabIndex = 5;
            this.AddAnimalBtn.Text = "Add";
            this.AddAnimalBtn.UseVisualStyleBackColor = true;
            this.AddAnimalBtn.Click += new System.EventHandler(this.AddAnimalBtn_Click);
            // 
            // tabsEmployees
            // 
            this.tabsEmployees.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabsEmployees.Controls.Add(this.animalsTabPage);
            this.tabsEmployees.Controls.Add(this.customerTabPage);
            this.tabsEmployees.Controls.Add(this.adoptedTabPage);
            this.tabsEmployees.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabsEmployees.Location = new System.Drawing.Point(12, 97);
            this.tabsEmployees.Name = "tabsEmployees";
            this.tabsEmployees.SelectedIndex = 0;
            this.tabsEmployees.Size = new System.Drawing.Size(684, 437);
            this.tabsEmployees.TabIndex = 0;
            // 
            // customerTabPage
            // 
            this.customerTabPage.Controls.Add(this.AdoptBtn);
            this.customerTabPage.Controls.Add(this.CustomerListView);
            this.customerTabPage.Controls.Add(this.DelCustomerBtn);
            this.customerTabPage.Controls.Add(this.EditCustomerBtn);
            this.customerTabPage.Controls.Add(this.AddCustomerBtn);
            this.customerTabPage.Location = new System.Drawing.Point(4, 25);
            this.customerTabPage.Name = "customerTabPage";
            this.customerTabPage.Size = new System.Drawing.Size(676, 408);
            this.customerTabPage.TabIndex = 2;
            this.customerTabPage.Text = "Customers";
            this.customerTabPage.UseVisualStyleBackColor = true;
            // 
            // CustomerListView
            // 
            this.CustomerListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.CustomerListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader3,
            this.columnHeader5,
            this.columnHeader6});
            this.CustomerListView.FullRowSelect = true;
            this.CustomerListView.Location = new System.Drawing.Point(5, 6);
            this.CustomerListView.MultiSelect = false;
            this.CustomerListView.Name = "CustomerListView";
            this.CustomerListView.Size = new System.Drawing.Size(507, 396);
            this.CustomerListView.TabIndex = 12;
            this.CustomerListView.UseCompatibleStateImageBehavior = false;
            this.CustomerListView.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Customer ID";
            this.columnHeader3.Width = 81;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Name";
            this.columnHeader5.Width = 142;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Email";
            this.columnHeader6.Width = 120;
            // 
            // DelCustomerBtn
            // 
            this.DelCustomerBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DelCustomerBtn.Location = new System.Drawing.Point(527, 93);
            this.DelCustomerBtn.Name = "DelCustomerBtn";
            this.DelCustomerBtn.Size = new System.Drawing.Size(142, 23);
            this.DelCustomerBtn.TabIndex = 11;
            this.DelCustomerBtn.Text = "Delete";
            this.DelCustomerBtn.UseVisualStyleBackColor = true;
            this.DelCustomerBtn.Click += new System.EventHandler(this.DelCustomerBtn_Click);
            // 
            // EditCustomerBtn
            // 
            this.EditCustomerBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.EditCustomerBtn.Location = new System.Drawing.Point(527, 35);
            this.EditCustomerBtn.Name = "EditCustomerBtn";
            this.EditCustomerBtn.Size = new System.Drawing.Size(142, 23);
            this.EditCustomerBtn.TabIndex = 10;
            this.EditCustomerBtn.Text = "Edit";
            this.EditCustomerBtn.UseVisualStyleBackColor = true;
            this.EditCustomerBtn.Click += new System.EventHandler(this.EditCustomerBtn_Click);
            // 
            // AddCustomerBtn
            // 
            this.AddCustomerBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.AddCustomerBtn.Location = new System.Drawing.Point(527, 6);
            this.AddCustomerBtn.Name = "AddCustomerBtn";
            this.AddCustomerBtn.Size = new System.Drawing.Size(143, 23);
            this.AddCustomerBtn.TabIndex = 9;
            this.AddCustomerBtn.Text = "Add";
            this.AddCustomerBtn.UseVisualStyleBackColor = true;
            this.AddCustomerBtn.Click += new System.EventHandler(this.AddCustomerBtn_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 63);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 20);
            this.label4.TabIndex = 8;
            this.label4.Text = "Department:";
            // 
            // deptLabel
            // 
            this.deptLabel.AutoSize = true;
            this.deptLabel.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deptLabel.Location = new System.Drawing.Point(99, 63);
            this.deptLabel.Name = "deptLabel";
            this.deptLabel.Size = new System.Drawing.Size(34, 20);
            this.deptLabel.TabIndex = 9;
            this.deptLabel.Text = "Test";
            // 
            // AdoptBtn
            // 
            this.AdoptBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.AdoptBtn.Location = new System.Drawing.Point(527, 64);
            this.AdoptBtn.Name = "AdoptBtn";
            this.AdoptBtn.Size = new System.Drawing.Size(143, 23);
            this.AdoptBtn.TabIndex = 13;
            this.AdoptBtn.Text = "Adopt Animal";
            this.AdoptBtn.UseVisualStyleBackColor = true;
            this.AdoptBtn.Click += new System.EventHandler(this.AdoptBtn_Click);
            // 
            // adoptedTabPage
            // 
            this.adoptedTabPage.Controls.Add(this.adoptedAnimalsListView);
            this.adoptedTabPage.Location = new System.Drawing.Point(4, 25);
            this.adoptedTabPage.Name = "adoptedTabPage";
            this.adoptedTabPage.Size = new System.Drawing.Size(676, 408);
            this.adoptedTabPage.TabIndex = 3;
            this.adoptedTabPage.Text = "Adopted Animals";
            this.adoptedTabPage.UseVisualStyleBackColor = true;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Animal Num";
            this.columnHeader7.Width = 72;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Pet Name";
            this.columnHeader8.Width = 142;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "Type";
            this.columnHeader9.Width = 120;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Breed";
            // 
            // adoptedAnimalsListView
            // 
            this.adoptedAnimalsListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.adoptedAnimalsListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader11,
            this.columnHeader12,
            this.columnHeader13,
            this.columnHeader14});
            this.adoptedAnimalsListView.FullRowSelect = true;
            this.adoptedAnimalsListView.Location = new System.Drawing.Point(5, 6);
            this.adoptedAnimalsListView.MultiSelect = false;
            this.adoptedAnimalsListView.Name = "adoptedAnimalsListView";
            this.adoptedAnimalsListView.Size = new System.Drawing.Size(668, 396);
            this.adoptedAnimalsListView.TabIndex = 9;
            this.adoptedAnimalsListView.UseCompatibleStateImageBehavior = false;
            this.adoptedAnimalsListView.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "Animal Num";
            this.columnHeader11.Width = 83;
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "Pet Name";
            this.columnHeader12.Width = 132;
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "Type";
            this.columnHeader13.Width = 120;
            // 
            // columnHeader14
            // 
            this.columnHeader14.Text = "Adopted By";
            this.columnHeader14.Width = 259;
            // 
            // EmployeeMainApp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(711, 546);
            this.Controls.Add(this.deptLabel);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.ShelterLbl);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.UserNameLbl);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.LogOutBtn);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tabsEmployees);
            this.Name = "EmployeeMainApp";
            this.Text = "AWMS: Employee App";
            this.animalsTabPage.ResumeLayout(false);
            this.tabsEmployees.ResumeLayout(false);
            this.customerTabPage.ResumeLayout(false);
            this.adoptedTabPage.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button LogOutBtn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label UserNameLbl;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label ShelterLbl;
        private System.Windows.Forms.TabPage animalsTabPage;
        private System.Windows.Forms.ListView AnimalsListView;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.Button DelAnimalBtn;
        private System.Windows.Forms.Button EditAnimalBtn;
        private System.Windows.Forms.Button AddAnimalBtn;
        private System.Windows.Forms.TabControl tabsEmployees;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label deptLabel;
        private System.Windows.Forms.TabPage customerTabPage;
        private System.Windows.Forms.ListView CustomerListView;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.Button DelCustomerBtn;
        private System.Windows.Forms.Button EditCustomerBtn;
        private System.Windows.Forms.Button AddCustomerBtn;
        private System.Windows.Forms.ColumnHeader BreedColumn;
        private System.Windows.Forms.Button AssignBtn;
        private System.Windows.Forms.Button UnassignBtn;
        private System.Windows.Forms.Button AdoptBtn;
        private System.Windows.Forms.TabPage adoptedTabPage;
        private System.Windows.Forms.ListView adoptedAnimalsListView;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
    }
}