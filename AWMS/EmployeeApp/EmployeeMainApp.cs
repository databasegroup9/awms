﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using AWMS;
using AWMS.EmployeeApp;
using AWMS.Procs;
using AWMS.Common;

namespace AMWS.EmployeeApp
{
    public partial class EmployeeMainApp : Form
    {
        private MainLogin logForm;
        public DataBaseManager Database;
        public Department Department;

        private bool logout;

        public sp_GetAnimals AnimalsProc;
        public sm_GetDepartments DeptsProc;
        public sp_GetCustomers CustomersProc;
        public List<Customer> CustomersServed; 

        public EmployeeMainApp(MainLogin lg, DataBaseManager dbm)
        {
            InitializeComponent();
            logForm = lg;
            Database = dbm;
            Database.Close();
            Database.Connect();

            DeptsProc = new sm_GetDepartments(Database.Connection, Database.ShelterId);
            DeptsProc.Execute();
            
            RefreshAllAnimalList();
            RefreshCustomerList();
            RefreshAdoptedAnimalList();

            SetUserAndShelter();

            tabsEmployees.TabPages.Remove(Department.DepartmentType == DepartmentType.ANIMAL_CARE
                                              ? customerTabPage
                                              : animalsTabPage);
            ValidateAnimalButtons();
        }

        private void SetUserAndShelter()
        {
            UserNameLbl.Text = Database.ConnectedUser.FullName;

            var proc = new sm_GetShelterDetails(Database.Connection, Database.ShelterId);
            proc.Execute();

            ShelterLbl.Text = proc.Result.Name;
            Department = FindDepartment(Database.DeptId);
            deptLabel.Text = Department.ToString();
        }

        private Department FindDepartment(int? deptId)
        {
            foreach (Department department in DeptsProc.AllDepartments)
            {
                if (department.DepartmentId == deptId)
                {
                    return department;
                }
            }
            return null;
        }

        private void AddAnimalBtn_Click(object sender, EventArgs e)
        {
            try
            {
                var animPP = new AnimalPP(Database.Connection, this);
                animPP.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void EditAnimalBtn_Click(object sender, EventArgs e)
        {
            if (AnimalsListView.SelectedIndices.Count == 1)
            {
                var animal = FindAnimal(AnimalsListView.SelectedItems[0]);
                var animPP = new AnimalPP(Database.Connection,animal, this);
                animPP.Show();
            }
        }

        private Animal FindAnimal(ListViewItem selectedItem)
        {
            int id = Convert.ToInt32(selectedItem.SubItems[0].Text);
            foreach (Animal animal in AnimalsProc.AnimalsList)
            {
                if (animal.AnimalNum == id)
                {
                    return animal;
                }
            }
            return new Animal("ERROR","ERROR","ERROR","ERROR",0,0,0);
        }

        private void DelAnimalBtn_Click(object sender, EventArgs e)
        {
            if (AnimalsListView.SelectedIndices.Count == 1)
            {
                var animal = FindAnimal(AnimalsListView.SelectedItems[0]);
                var proc = new sp_DelAnimal(Database.Connection, (int) animal.AnimalNum);
                proc.Execute();
            }
            RefreshAllAnimalList();
            RefreshAdoptedAnimalList();
        }

        private void RefreshAdoptedAnimalList()
        {
            AnimalsProc = new sp_GetAnimals(Database.Connection, (int)Database.DeptId, Database.ConnectedUser.UserId);
            AnimalsProc.Execute();
            
            adoptedAnimalsListView.Items.Clear();
            
            
            string[] itm = new string[3];
            var item = new ListViewItem();

            //Add All Animals
            List<sp_GetAnimals.AdoptedAnimal> list = AnimalsProc.AdoptedAnimals;
            foreach (sp_GetAnimals.AdoptedAnimal a in list)
            {
                itm = new string[4];
                itm[0] = a.AnimalNum.ToString();
                itm[1] = a.PetName;
                itm[2] = a.AnimalType;
                itm[3] = a.Customer;

                item = new ListViewItem(itm);
                adoptedAnimalsListView.Items.Add(item);
            }       
        }

        private void RefreshAllAnimalList()
        {
            AnimalsProc = new sp_GetAnimals(Database.Connection, (int)Database.DeptId, Database.ConnectedUser.UserId);
            AnimalsProc.Execute();

            AnimalsListView.Items.Clear();


            string[] itm = new string[3];
            var item = new ListViewItem();

            //Add All Animals
            List<Animal> list = AnimalsProc.AnimalsList;
            foreach (Animal a in list)
            {
                if (a.KeptIn == Database.DeptId)
                {
                    itm = new string[4];
                    itm[0] = a.AnimalNum.ToString();
                    itm[1] = a.PetName;
                    itm[2] = a.CommonAnimalName;
                    itm[3] = a.Breed;

                    item = new ListViewItem(itm);
                    if (AnimalsProc.MyAnimalNumbers.Contains((int)a.AnimalNum))
                    {
                        item.ForeColor = System.Drawing.Color.ForestGreen;
                        item.Group = AnimalsListView.Groups[0];
                    }
                    else if (AnimalsProc.AnimalsNotCared.Contains((int) a.AnimalNum))
                    {
                        item.ForeColor = System.Drawing.Color.Red;
                        item.Group = AnimalsListView.Groups[2];
                    }
                    else
                    {
                        item.Group = AnimalsListView.Groups[1];
                    }
                    AnimalsListView.Items.Add(item);
                }
            }
        }
        private void RefreshCustomerList()
        {
            CustomersProc = new sp_GetCustomers(Database.Connection, (int)Database.DeptId);
            CustomersProc.Execute();

            CustomerListView.Items.Clear();
            CustomersServed = new List<Customer>();


            var item = new ListViewItem();

            //Add Served Customer
            List<Customer> list = CustomersProc.CustomersList;
            foreach (Customer c in list)
            {
                if(!CustomersProc.DeptCustomerNumbers.Contains((int)c.Id)) continue;

                string[] itm = new string[3];
                itm[0] = c.Id.ToString();
                itm[1] = c.Name;
                itm[2] = c.Email;

                item = new ListViewItem(itm);
                CustomerListView.Items.Add(item);
                CustomersServed.Add(c);
            }
        }

        public void OnPropertyPageClosed()
        {
            RefreshAllAnimalList();
            RefreshCustomerList();
            RefreshAdoptedAnimalList();
        }

        private void LogOutBtn_Click(object sender, EventArgs e)
        {
            logForm.ResetLoginForm();
            logForm.Show();
            Database.ConnectedUser = null;
            Database.Close();
            logout = true;
            Dispose();       
        }

        private void AssignBtn_Click(object sender, EventArgs e)
        {
            var proc = new sp_CareForAnimal(Database.Connection, (int) FindAnimal(AnimalsListView.SelectedItems[0]).AnimalNum,
                                            Database.ConnectedUser.UserId,true);
            proc.Execute();
            RefreshAllAnimalList();
        }

        private void UnassignBtn_Click(object sender, EventArgs e)
        {
            var proc = new sp_CareForAnimal(Database.Connection, (int)FindAnimal(AnimalsListView.SelectedItems[0]).AnimalNum,
                                            Database.ConnectedUser.UserId, false);
            proc.Execute();
            RefreshAllAnimalList();
        }

        private void AnimalsListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            ValidateAnimalButtons();
        }

        private void ValidateAnimalButtons()
        {
            ResetAnimalButtons();
            if (AnimalsListView.SelectedItems.Count != 1)
            {
                EditAnimalBtn.Enabled = false;
                DelAnimalBtn.Enabled = false;
                AssignBtn.Enabled = false;
                AssignBtn.Visible = true;
                UnassignBtn.Visible = false;
                return;
            }

            var a_num = (int) FindAnimal(AnimalsListView.SelectedItems[0]).AnimalNum;
            if (!AnimalsProc.MyAnimalNumbers.Contains(a_num)) return;
            
            AssignBtn.Visible = false;
            UnassignBtn.Visible = true;
        }

        private void ResetAnimalButtons()
        {
            EditAnimalBtn.Enabled = true;
            DelAnimalBtn.Enabled = true;
            AssignBtn.Enabled = true;
            AssignBtn.Visible = true;
            UnassignBtn.Visible = false;
        }

        private void AddCustomerBtn_Click(object sender, EventArgs e)
        {
            var s = new CustomerSearch(this);
            s.ShowDialog();
        }

        private void EditCustomerBtn_Click(object sender, EventArgs e)
        {
            if (CustomerListView.SelectedItems.Count == 1)
            {
                var customer = FindCustomer(CustomerListView.SelectedItems[0]);
                var pp = new CustomerPP(Database.Connection, customer, this);
                pp.Show();
            }
        }

        private void DelCustomerBtn_Click(object sender, EventArgs e)
        {
            if (CustomerListView.SelectedItems.Count == 1)
            {
                var customer = FindCustomer(CustomerListView.SelectedItems[0]);
                var proc = new sp_DelCustomer(Database.Connection, (int) customer.Id);
                proc.Execute();
            }
            RefreshCustomerList();
        }

        private Customer FindCustomer(ListViewItem selectedItem)
        {
            int id = Convert.ToInt32(selectedItem.SubItems[0].Text);
            foreach (Customer customer in CustomersProc.CustomersList)
            {
                if (customer.Id == id)
                {
                    return customer;
                }
            }
            return new Customer(0,0,"ERROR","ERROR","ERROR");
        }

        private void AdoptBtn_Click(object sender, EventArgs e)
        {
            if (CustomerListView.SelectedItems.Count == 1)
            {
                var customer = FindCustomer(CustomerListView.SelectedItems[0]);
                var s = new AnimalSearch(this, customer);
                s.ShowDialog();
            }
        }
    } 
}
