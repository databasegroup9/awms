﻿using System;
using System.Windows.Forms;
using AWMS.Procs;
using Oracle.DataAccess.Client;
using AWMS.Common;

namespace AMWS.EmployeeApp
{
    public partial class AnimalPP : Form
    {
        private OracleConnection connection;
        private Animal animal;
        private EmployeeMainApp parentForm;

        public AnimalPP(OracleConnection c, EmployeeMainApp pf)
        {
            //New Employee
            InitializeComponent();
            connection = c;
            parentForm = pf;
            AdoptedByText.Enabled = false;
            KeptText.Enabled = false;
            KeptText.Text = pf.Department.ToString();
            AdoptedByText.Text = "Not Adopted";
        }

        public AnimalPP(OracleConnection c, Animal a, EmployeeMainApp pf)
        {
            InitializeComponent();
            connection = c;
            animal = a;
            parentForm = pf;
            RefreshFields();
            AdoptedByText.Enabled = false;
            KeptText.Enabled = false;
        }


        private void OkBtn_Click(object sender, EventArgs e)
        {
            if(Save()) Close();
        }

        private bool Save()
        {
            if(!ValidateFields()) return false;

            try
            {
                String pn = PetNameInput.Text.Trim();
                String cn = CommonNameInput.Text.Trim();
                String desc = DescInput.Text.Trim();
                String breed = BreedInput.Text.Trim();
                int fee = Convert.ToInt32(FeeInput.Text.Trim());
                var keptIn = parentForm.Department.DepartmentId;
                var a = new Animal(pn, desc, cn, breed, fee, null, keptIn);
                

                if (animal == null)
                {
                    var proc = new sp_AddAnimal(connection, a);
                    proc.Execute();
                    a.AnimalNum = proc.AnimalNum;
                    animal = a;
                }
                else
                {
                    a.AnimalNum = animal.AnimalNum;
                    var proc = new sp_EditAnimal(connection, a);
                    proc.Execute();
                    animal = a;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }

        private bool ValidateFields()
        {
            errorProvider.Clear();

            bool valid = true;
            //Empty Fields
            if (String.IsNullOrEmpty(PetNameInput.Text.Trim()))
            {
                errorProvider.SetError(PetNameInput,"Pet Name must not be empty.");
                valid = false;
            }

            if (String.IsNullOrEmpty(CommonNameInput.Text.Trim()))
            {
                errorProvider.SetError(CommonNameInput, "Common Type must not be empty.");
                valid = false;
            }

            if (String.IsNullOrEmpty(BreedInput.Text.Trim()))
            {
                errorProvider.SetError(BreedInput, "Breed must not be empty.");
                valid = false;
            }

            if (String.IsNullOrEmpty(FeeInput.Text.Trim()))
            {
                errorProvider.SetError(FeeInput, "Adoption Fee must not be empty.");
                valid = false;
            }
            else if (!Util.IsValidNumber(FeeInput.Text.Trim()))
            {
                errorProvider.SetError(FeeInput, "Adoption Fee be a valid number.");
                valid = false;
            }

            if (String.IsNullOrEmpty(DescInput.Text.Trim()))
            {
                errorProvider.SetError(DescInput, "Description must not be empty.");
                valid = false;
            }
            return valid;
        }

        

        private void ApplyBtn_Click(object sender, EventArgs e)
        {
            if (!Save()) return;
            RefreshFields();
            OnCloseParentForm();
        }

        private void OnCloseParentForm()
        {
                parentForm.OnPropertyPageClosed();
        }

        private void CancelBtn_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void RefreshFields()
        {
            TitleLbl.Text = "Editing Animal: " + animal.AnimalNum + " - " + animal.PetName;
            PetNameInput.Text = animal.PetName;
            FeeInput.Text = animal.AdoptionFee.ToString();
            DescInput.Text = animal.Description;
            CommonNameInput.Text = animal.CommonAnimalName;
            BreedInput.Text = animal.Breed;
            KeptText.Text = parentForm.Department.ToString();

            if (animal.AdoptedBy == null)
            {
                AdoptedByText.Text = "Not Adopted";
            }
        }

        private void AnimalPP_FormClosed(object sender, FormClosedEventArgs e)
        {
            OnCloseParentForm();
        }
    }
}
