﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using AMWS.EmployeeApp;
using AWMS.Common;
using AWMS.Procs;

namespace AWMS.EmployeeApp
{
    public partial class AnimalSearch : Form
    {
        private EmployeeMainApp parentForm;
        private List<Animal> animalList;
        private Customer customer;

        public AnimalSearch(EmployeeMainApp pf, Customer cust)
        {
            InitializeComponent();
            customer = cust;
            parentForm = pf;
            AdoptAnimalBtn.Enabled = false;
            RefreshListAll();
        }

        private void RefreshListAll()
        {
            var proc = new sp_GetAnimals(parentForm.Database.Connection);
            proc.ExecuteAvailableAnimals();
            animalList = proc.AnimalsList;

            animalSearchListView.Items.Clear();

            var item = new ListViewItem();

            foreach (Animal a in animalList)
            {
                string[] itm = new string[5];
                itm[0] = a.AnimalNum.ToString();
                itm[1] = a.PetName;
                itm[2] = a.CommonAnimalName;
                itm[3] = a.Breed;
                itm[4] = a.AdoptionFee.ToString();

                item = new ListViewItem(itm);
                animalSearchListView.Items.Add(item);
            }
        }

        private void RefreshButton_Click(object sender, System.EventArgs e)
        {
            RefreshListAll();
        }

        private void SearchBox_TextChanged(object sender, System.EventArgs e)
        {
            RefreshListSearch();
        }

        private void RefreshListSearch()
        {
            animalSearchListView.Items.Clear();

            var item = new ListViewItem();

            foreach (Animal a in animalList)
            {
                if (!CheckSearch(a)) continue;

                string[] itm = new string[5];
                itm[0] = a.AnimalNum.ToString();
                itm[1] = a.PetName;
                itm[2] = a.CommonAnimalName;
                itm[3] = a.Breed;
                itm[4] = a.AdoptionFee.ToString();

                item = new ListViewItem(itm);
                animalSearchListView.Items.Add(item);
            }
        }

        private bool CheckSearch(Animal animal)
        {
            if (SearchBox.Text.Trim().Equals(animal.AnimalNum.ToString()))
            {
                return true;
            }

            if (animal.PetName.ToLower().Contains(SearchBox.Text.Trim().ToLower()))
            {
                return true;
            }

            if (animal.CommonAnimalName.ToLower().Contains(SearchBox.Text.Trim().ToLower()))
            {
                return true;
            }

            if (animal.Breed.ToLower().Contains(SearchBox.Text.Trim().ToLower()))
            {
                return true;
            }

            return false;
        }

        private void CancelBtn_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        private void CustomerSearch_FormClosing(object sender, FormClosingEventArgs e)
        {
            parentForm.OnPropertyPageClosed();
        }

        private void customerSearchListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            AdoptAnimalBtn.Enabled = animalSearchListView.SelectedItems.Count == 1;
        }

        private void AdoptAnimalBtn_Click(object sender, EventArgs e)
        {
            int a = Convert.ToInt32(animalSearchListView.SelectedItems[0].SubItems[0].Text.Trim());
            string pn = animalSearchListView.SelectedItems[0].SubItems[1].Text.Trim();
            var proc = new sp_AdoptsAnimal(parentForm.Database.Connection, (int) customer.Id, a);
            proc.Execute();
            Close();
            if (proc.Result == 1)
            {
                MessageBox.Show(customer.Name + " has adopted " + pn + ".", "Info" , MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
        }
    }
}
