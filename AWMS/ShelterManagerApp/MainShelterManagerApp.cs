﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using AWMS;
using AWMS.Procs;
using AWMS.Common;

namespace AMWS.ShelterManagerApp
{
    public partial class MainShelterManagerApp : Form
    {
        private MainLogin logForm;
        public DataBaseManager Database;

        private bool logout;

        public sm_GetEmployees EmployeesProc;
        public sm_GetDepartments DeptsProc;
        public sm_GetShelters SheltersProc;

        private List<AnimalShelterManager> possibleManagers = new List<AnimalShelterManager>();

        public MainShelterManagerApp(MainLogin lg, DataBaseManager dbm)
        {
            InitializeComponent();
            logForm = lg;
            Database = dbm;
            Database.Close();
            Database.Connect();

            SetUserAndStore();

            RefreshEmployeesList();
            RefreshDeptsList();
            RefreshSheltersList();

        }

        private void SetUserAndStore()
        {
            UserNameLbl.Text = Database.ConnectedUser.FullName;

            var proc = new sm_GetShelterDetails(Database.Connection, Database.ShelterId);
            proc.Execute();

            StoreLbl.Text = proc.Result.Name;
        }
        private void AddDeptBtn_Click(object sender, EventArgs e)
        {
            var DeptPP = new DepartmentPP(Database.Connection,GetValidDeptManagers(),this);
            DeptPP.Show();
        }

        private List<User> GetValidDeptManagers()
        {
            EmployeesProc = new sm_GetEmployees(Database.Connection, Database.ShelterId);
            EmployeesProc.Execute();
            var list = new List<User>();
            list.AddRange(EmployeesProc.DeptManagerList.ToArray());
            list.AddRange(EmployeesProc.EmployeeList.ToArray());
            return list;
        }

        private void EditDeptBtn_Click(object sender, EventArgs e)
        {
            if (DeptsListView.SelectedIndices.Count == 1)
            {
                DepartmentPP DeptPP = null;
                var selDept = DeptsProc.AllDepartments[DeptsListView.SelectedIndices[0]];
                switch (selDept.DepartmentType)
                {
                    case DepartmentType.ANIMAL_CARE:
                        {
                            AnimalCareDepartment dept = GetAnimalCareDepartment(Convert.ToInt32(DeptsListView.SelectedItems[0].SubItems[0].Text));
                            DeptPP = new DepartmentPP(Database.Connection, GetValidDeptManagers(), dept, FindDepartmentManager(selDept.DepartmentId), this);
                        }
                        break;
                    case DepartmentType.CUSTOMER_SERVICE:
                        {
                            CustomerServiceDepartment dept = GetCustomerServiceDepartment(Convert.ToInt32(DeptsListView.SelectedItems[0].SubItems[0].Text));
                            DeptPP = new DepartmentPP(Database.Connection, GetValidDeptManagers(), dept, FindDepartmentManager(selDept.DepartmentId), this);
                        }
                        break;
                }
                if (DeptPP == null) return;
                DeptPP.Show();
            }        
        }

        private CustomerServiceDepartment GetCustomerServiceDepartment(int id)
        {
            foreach (CustomerServiceDepartment d in DeptsProc.CustomerServiceDepartments)
            {
                if (d.DepartmentId == id)
                {
                    return d;
                }
            }
            return new CustomerServiceDepartment(0, "NONE", 0, "NONE", 0);
        }

        private AnimalCareDepartment GetAnimalCareDepartment(int id)
        {
            foreach (AnimalCareDepartment d in DeptsProc.AnimalCareDepartments)
            {
                if (d.DepartmentId == id)
                {
                    return d;
                }
            }
            return new AnimalCareDepartment(0,"NONE",0,"NONE",0);
        }

        private void AddDptMngBtn_Click(object sender, EventArgs e)
        {
            try
            {
                var DepMgnPP = new EmployeePP(Database.Connection, this);
                DepMgnPP.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void EditDeptManagerBtn_Click(object sender, EventArgs e)
        {
            if (EmployeesListView.SelectedIndices.Count == 1)
            {
                var employee = FindEmployee(EmployeesListView.SelectedItems[0]);
                var DepMgnPP = new EmployeePP(Database.Connection, employee, this);
                DepMgnPP.Show();
            }
        }

        private User FindEmployee(ListViewItem selectedItem)
        {
            int id = Convert.ToInt32(selectedItem.SubItems[0].Text);
            foreach (User user in EmployeesProc.AllEmployeeList)
            {
                if (user.UserId == id)
                {
                    return user;
                }
            }
            return new User("ERROR","ERROR",UserType.INVALID,-1,0);
        }

        private void DelDeptManagerBtn_Click(object sender, EventArgs e)
        {
            if (EmployeesListView.SelectedIndices.Count == 1)
            {
                var employee = FindEmployee(EmployeesListView.SelectedItems[0]);
                var proc = new sm_DelEmployee(Database.Connection, employee.UserId);
                proc.Execute();
            }
            RefreshEmployeesList();
            RefreshDeptsList();
        }

        private void RefreshDeptsList()
        {
            DeptsProc = new sm_GetDepartments(Database.Connection, Database.ShelterId);
            DeptsProc.Execute();
            List<Department> list = DeptsProc.AllDepartments;

            DeptsListView.Items.Clear();

            foreach (Department d in list)
            {
                string[] itm = new string[4];
                itm[0] = d.DepartmentId.ToString();
                itm[1] = d.Name;

                DeptManager dm = FindDepartmentManager(d.DepartmentId);

                if (dm == null)
                {
                    itm[2] = "None";
                }
                else
                {
                    itm[2] = dm.ToNameString();
                }

                itm[3] = d.PhoneExt.ToString();

                var item = new ListViewItem(itm)
                    {
                        Group =
                            d.DepartmentType == DepartmentType.ANIMAL_CARE
                                ? DeptsListView.Groups[0]
                                : DeptsListView.Groups[1]
                    };
                DeptsListView.Items.Add(item);
                
            }
        }

        private void RefreshSheltersList()
        {
            SheltersProc = new sm_GetShelters(Database.Connection, Database.OrgName);
            SheltersProc.Execute();
            List<AnimalShelter> list = SheltersProc.Shelters;

            SheltersListView.Items.Clear();

            foreach (AnimalShelter a in list)
            {
                string[] itm = new string[3];
                itm[0] = a.ShelterId.ToString();
                itm[1] = a.Name;
                itm[2] = a.Location;

                var item = new ListViewItem(itm);
                SheltersListView.Items.Add(item);

            }          
        }

        private DeptManager FindDepartmentManager(int deptId)
        {
            EmployeesProc = new sm_GetEmployees(Database.Connection, Database.ShelterId);
            EmployeesProc.Execute();
            List<DeptManager> list = EmployeesProc.DeptManagerList;

            foreach (DeptManager dm in list)
            {
                if(dm.Dept != null)
                {
                    if (dm.Dept.DepartmentId == deptId) return dm;
                }
            }
            
            return null;
        }

        private void RefreshEmployeesList()
        {
            EmployeesProc = new sm_GetEmployees(Database.Connection, Database.ShelterId);
            EmployeesProc.Execute();
            
            EmployeesListView.Items.Clear();
            possibleManagers = new List<AnimalShelterManager>();
            
            //Add Shelter Manager
            string[] itm = new string[3];
            itm[0] = EmployeesProc.ShelterManager.UserId.ToString();
            itm[1] = EmployeesProc.ShelterManager.FullName;
            itm[2] = "None - Shelter Manager";
            var item = new ListViewItem(itm) { Group = EmployeesListView.Groups[0] };
            EmployeesListView.Items.Add(item);


            //Add Dept Managers
            List<DeptManager> list = EmployeesProc.DeptManagerList;
            foreach (DeptManager dm in list)
            {
                itm = new string[3];
                itm[0] = dm.UserId.ToString();
                itm[1] = dm.FullName;
                itm[2] = dm.Dept.Name;

                item = new ListViewItem(itm) {Group = EmployeesListView.Groups[1]};
                EmployeesListView.Items.Add(item);
            }

            //Add Employees
            foreach (Employee e in EmployeesProc.EmployeeList)
            {
                int groupId;
                itm = new string[3];
                itm[0] = e.UserId.ToString();
                itm[1] = e.FullName;
                if (e.DeptId == null)
                {
                    itm[2] = "None";
                    groupId = 3;
                    possibleManagers.Add(new AnimalShelterManager
                        {
                            Id = e.UserId,
                            Name = e.FullName
                        });
                }
                else
                {
                    itm[2] = GetDeptName(e.DeptId);
                    groupId = 2;
                }
                
                item = new ListViewItem(itm) { Group = EmployeesListView.Groups[groupId] };
                EmployeesListView.Items.Add(item);
            }
        }

        private string GetDeptName(int? deptId)
        {
            DeptsProc = new sm_GetDepartments(Database.Connection,Database.ShelterId);
            DeptsProc.Execute();
            foreach (Department d in DeptsProc.AllDepartments)
            {
                if (d.DepartmentId == deptId)
                {
                    return d.Name;
                }
            }

            return "None";
        }

        public void OnPropertyPageClosed()
        {
            RefreshEmployeesList();
            RefreshDeptsList();
            RefreshSheltersList();
        }

        private void LogOutBtn_Click(object sender, EventArgs e)
        {
            logForm.ResetLoginForm();
            logForm.Show();
            Database.ConnectedUser = null;
            logout = true;
            Dispose();       
        }

        private void DelDepartBtn_Click(object sender, EventArgs e)
        {
            if (DeptsListView.SelectedIndices.Count == 1)
            {
                var selDept = DeptsProc.AllDepartments[DeptsListView.SelectedIndices[0]];
                var m = FindDepartmentManager(selDept.DepartmentId);
                if (m == null)
                {
                    var proc = new sm_DelDepartment(Database.Connection, selDept.DepartmentId, 0);
                    proc.Execute();
                }
                else
                {
                    var proc = new sm_DelDepartment(Database.Connection, selDept.DepartmentId,m.UserId);
                    proc.Execute();
                }            
            }

            RefreshDeptsList();
            RefreshEmployeesList();
        }

        private void addShelterBtn_Click(object sender, EventArgs e)
        {
            try
            {
                var dialog = new ShelterPP(Database.Connection, this, possibleManagers);
                dialog.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void editShelterBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (SheltersListView.SelectedItems.Count == 1)
                {
                    AnimalShelter shelter = GetSelectedShelter();
                    AnimalShelterManager manager = GetSelectedShelterManager();
                    var dialog = new ShelterPP(Database.Connection, shelter, this, manager);
                    dialog.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private AnimalShelterManager GetSelectedShelterManager()
        {
            int id = Convert.ToInt32(SheltersListView.SelectedItems[0].SubItems[0].Text);
            int manId = 0;
            foreach (AnimalShelter a in SheltersProc.Shelters)
            {
                if (a.ShelterId == id)
                {
                    manId = a.ManagerId;
                }
            }

            foreach (AnimalShelterManager manager in SheltersProc.ShelterManagers)
            {
                if (manager.Id == manId)
                {
                    return manager;
                }
            }

            return new AnimalShelterManager();
        }

        private AnimalShelter GetSelectedShelter()
        {
            int id = Convert.ToInt32(SheltersListView.SelectedItems[0].SubItems[0].Text);
            foreach (AnimalShelter a in SheltersProc.Shelters)
            {
                if (a.ShelterId == id)
                {
                    return a;
                }
            }
            return new AnimalShelter(0,"ERROR",0,"ERROR","ERROR");
        }
    } 
}
