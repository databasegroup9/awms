﻿using System;
using System.Windows.Forms;

namespace AWMS.ShelterManagerApp
{
    public partial class AnimalCareControl : UserControl
    {
        public AnimalCareControl()
        {
            InitializeComponent();
        }

        public string GetAnimalType()
        {
            return AnimalTypeInputBox.Text.Trim();
        }
        public void SetAnimalType(string type)
        {
            AnimalTypeInputBox.Text = type;
        }
        public int GetVetContact()
        {
            if (string.IsNullOrEmpty(VetContactTextBox.Text.Trim())) return 0;
            return Convert.ToInt32(VetContactTextBox.Text.Trim());
        }
        public void SetVetContact(int v)
        {
            VetContactTextBox.Text = v.ToString();
        }
    }
}
