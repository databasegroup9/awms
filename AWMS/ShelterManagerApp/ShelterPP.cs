﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using AWMS.Procs;
using Oracle.DataAccess.Client;
using AWMS.Common;

namespace AMWS.ShelterManagerApp
{
    public partial class ShelterPP : Form
    {
        private OracleConnection connection;
        private AnimalShelter shelter;
        private MainShelterManagerApp parentForm;
        private List<AnimalShelterManager> possibleManagers;
        private AnimalShelterManager currentManager;

        public ShelterPP(OracleConnection c, MainShelterManagerApp pf, List<AnimalShelterManager> managers)
        {
            //New Shelter
            InitializeComponent();
            connection = c;
            parentForm = pf;
            possibleManagers = managers;
            RefreshManagers();
        }

        public ShelterPP(OracleConnection c, AnimalShelter a, MainShelterManagerApp pf, AnimalShelterManager cManager)
        {
            InitializeComponent();
            connection = c;
            shelter = a;
            parentForm = pf;
            currentManager = cManager;
            RefreshFields();
        }

        private void OkBtn_Click(object sender, EventArgs e)
        {
            if(Save()) Close();
        }

        private bool Save()
        {
            if(!ValidateFields()) return false;

            try
            {
                String n = NameInputBox.Text.Trim();
                String l = LocationInputBox.Text.Trim();
                var man = (AnimalShelterManager)managerComboBox.SelectedItem;

                

                if (shelter == null)
                {
                    currentManager = man;
                    shelter = new AnimalShelter(0, n, man.Id, l, parentForm.Database.OrgName);
                    var proc = new sm_NewShelter(connection, shelter);
                    proc.Execute();
                    shelter.ShelterId = proc.Id;
                }
                else
                {
                    shelter.Name = n;
                    shelter.Location = l;
                    var proc = new sm_EditShelter(connection, shelter);
                    proc.Execute();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }

        private bool ValidateFields()
        {
            errorProvider.Clear();

            bool valid = true;
            //Empty Fields
            if (String.IsNullOrEmpty(NameInputBox.Text.Trim()))
            {
                errorProvider.SetError(NameInputBox,"Name must not be empty.");
                valid = false;
            }

            if (String.IsNullOrEmpty(LocationInputBox.Text.Trim()))
            {
                errorProvider.SetError(LocationInputBox, "Location must not be empty.");
                valid = false;
            }

            if (managerComboBox.SelectedIndex == -1 || managerComboBox.Items.Count == 0)
            {
                errorProvider.SetError(managerComboBox, "Please select a Shelter Manager or create a new Employee");
                valid = false;
            }

            return valid;
        }

        private void ApplyBtn_Click(object sender, EventArgs e)
        {
            if (!Save()) return;
            RefreshFields();
            OnCloseParentForm();
        }

        private void OnCloseParentForm()
        {
            if (parentForm != null)
            {
                parentForm.OnPropertyPageClosed();
            }
        }

        private void CancelBtn_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void RefreshFields()
        {
            RefreshManagers(currentManager);
            TitleLbl.Text = "Editing Animal Shelter: " + shelter.Name;
            NameInputBox.Text = shelter.Name;
            LocationInputBox.Text = shelter.Location;
        }

        private void RefreshManagers(AnimalShelterManager c)
        {
            managerComboBox.Items.Clear();
            managerComboBox.Items.Add(c);
            managerComboBox.Enabled = false;
            managerComboBox.SelectedIndex = 0;
        }

        private void RefreshManagers()
        {
            managerComboBox.Items.AddRange(possibleManagers.ToArray());

            if (managerComboBox.Items.Count > 0)
            {
                managerComboBox.SelectedIndex = 0;
            }
        }

        private void ShelterPP_FormClosed(object sender, FormClosedEventArgs e)
        {
            OnCloseParentForm();
        }
    }
}
