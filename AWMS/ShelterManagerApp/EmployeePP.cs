﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using AMWS.DeptManagerApp;
using AWMS.Procs;
using Oracle.DataAccess.Client;
using AWMS.Common;

namespace AMWS.ShelterManagerApp
{
    public partial class EmployeePP : Form
    {
        private OracleConnection connection;
        private User employee = null;
        private MainShelterManagerApp parentForm;
        private MainDeptManagerApp managerForm;

        public EmployeePP(OracleConnection c, MainShelterManagerApp pf)
        {
            //New Employee
            InitializeComponent();
            connection = c;
            parentForm = pf;
            RefreshDepartments(null);
        }
        public EmployeePP(OracleConnection c, MainDeptManagerApp pf)
        {
            //New Employee
            InitializeComponent();
            connection = c;
            managerForm = pf;
            RefreshDepartments(null);
        }
        public EmployeePP(OracleConnection c, User e, MainShelterManagerApp pf)
        {
            InitializeComponent();
            connection = c;
            employee = e;
            parentForm = pf;
            RefreshFields();
        }
        public EmployeePP(OracleConnection c, User e, MainDeptManagerApp pf)
        {
            InitializeComponent();
            connection = c;
            employee = e;
            managerForm = pf;
            RefreshFields();
        }

        private void OkBtn_Click(object sender, EventArgs e)
        {
            if(Save()) Close();
        }

        private bool Save()
        {
            if(!ValidateFields(employee == null)) return false;

            try
            {
                String fn = FirstInputBox.Text.Trim();
                String em = EmailInputBox.Text.Trim();
                String p = PassInputBox.Text;
                var dept = (Department)deptComboBox.SelectedItem;
                int? d = dept.DepartmentId;
                if (d == 0)
                {
                    d = null;
                }
                int s = Convert.ToInt32(salaryBox.Text.Trim());
                int ph = Convert.ToInt32(phoneTextBox.Text.Trim());

                if (employee == null)
                {
                    var proc = new sm_NewEmployee(connection, em, p, fn, s, ph, d);
                    proc.Execute();
                    employee = new User(fn, em, UserType.EMPLOYEE, proc.Id, s, ph, d);
                }
                else
                {
                    var proc = new sm_EditEmployee(connection, em, fn, s, ph, d, employee.UserId);
                    proc.Execute();
                    employee = new User(fn, em, employee.UserType, employee.UserId, s, ph, d);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }

        private bool ValidateFields(bool isnew)
        {
            errorProvider.Clear();

            bool valid = true;
            //Empty Fields
            if (String.IsNullOrEmpty(FirstInputBox.Text.Trim()))
            {
                errorProvider.SetError(FirstInputBox,"Name must not be empty.");
                valid = false;
            }


            if (String.IsNullOrEmpty(EmailInputBox.Text.Trim()))
            {
                errorProvider.SetError(EmailInputBox, "Email must not be empty.");
                valid = false;
            }

            if (String.IsNullOrEmpty(phoneTextBox.Text.Trim()))
            {
                errorProvider.SetError(phoneTextBox, "Phone Number must not be empty.");
                valid = false;
            }
            else if (!Util.IsValidNumber(phoneTextBox.Text.Trim()))
            {
                errorProvider.SetError(phoneTextBox, "Phone Number must be a valid number.");
                valid = false;
            }

            if (String.IsNullOrEmpty(salaryBox.Text.Trim()))
            {
                errorProvider.SetError(salaryBox, "Salary must not be empty.");
                valid = false;
            }
            else if (!Util.IsValidNumber(salaryBox.Text.Trim()))
            {
                errorProvider.SetError(salaryBox, "Salary must be a valid number.");
                valid = false;
            }

            if (String.IsNullOrEmpty(PassInputBox.Text.Trim()) && isnew)
            {
                errorProvider.SetError(PassInputBox, "Password must not be empty.");
                valid = false;
            }
            return valid;
        }

        

        private void ApplyBtn_Click(object sender, EventArgs e)
        {
            if (!Save()) return;
            RefreshFields();
            OnCloseParentForm();
        }

        private void OnCloseParentForm()
        {
            if (parentForm != null)
            {
                parentForm.OnPropertyPageClosed();
            }
            else
            {
                managerForm.OnPropertyPageClosed();
            }
        }

        private void CancelBtn_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void RefreshFields()
        {
            TitleLbl.Text = "Editing Employee: " + employee.FullName;
            FirstInputBox.Text = employee.FullName;
            EmailInputBox.Text = employee.Email;
            phoneTextBox.Text = employee.PhoneNum.ToString();
            salaryBox.Text = employee.Salary.ToString();
            RefreshDepartments(employee.DeptId);

            
            
            PassInputBox.Visible = false;
            PassLbl.Visible = false;
        }

        private void RefreshDepartments(int? dept_id)
        {
            if (managerForm != null)
            {
                deptComboBox.Enabled = false;
                deptComboBox.Items.Add(FindDepartment((int)managerForm.Database.DeptId, false));
                deptComboBox.SelectedIndex = 0;
                return;
            }
            deptComboBox.Items.Add(new Department(0, "None", 0, DepartmentType.NONE));
            deptComboBox.Items.AddRange(parentForm.DeptsProc.AllDepartments.ToArray());

            if (dept_id == null)
            {
                deptComboBox.SelectedIndex = 0;
            }
            else
            {
                deptComboBox.SelectedItem = FindDepartment((int) dept_id, true);
            }

            if (employee == null) return;

            if (employee.UserType == UserType.DEPT_MANAGER || employee.UserType == UserType.SHELTER_MANAGER)
            {
                deptComboBox.Enabled = false;
            }
        }

        private Department FindDepartment(int id, bool isShelterMgr)
        {
            var list = isShelterMgr ? parentForm.DeptsProc.AllDepartments : managerForm.DeptsProc.AllDepartments;
            foreach (Department d in list)
            {
                if (d.DepartmentId == id)
                {
                    return d;
                }
            }
            return null;
        }
        private void DepartmentManagerPP_FormClosed(object sender, FormClosedEventArgs e)
        {
            OnCloseParentForm();
        }
    }
}
