﻿namespace AMWS.ShelterManagerApp
{
    partial class MainShelterManagerApp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);

            if (!logout)
            {
                logForm.Dispose();
            }          
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ListViewGroup listViewGroup1 = new System.Windows.Forms.ListViewGroup("Animal Care", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup2 = new System.Windows.Forms.ListViewGroup("Customer Service", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup3 = new System.Windows.Forms.ListViewGroup("Shelter Manager", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup4 = new System.Windows.Forms.ListViewGroup("Department Managers", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup5 = new System.Windows.Forms.ListViewGroup("Employees", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup6 = new System.Windows.Forms.ListViewGroup("Unassigned Employees", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup7 = new System.Windows.Forms.ListViewGroup("Shelter Manager", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup8 = new System.Windows.Forms.ListViewGroup("Department Managers", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup9 = new System.Windows.Forms.ListViewGroup("Employees", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup10 = new System.Windows.Forms.ListViewGroup("Unassigned Employees", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup11 = new System.Windows.Forms.ListViewGroup("Animal Care", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup12 = new System.Windows.Forms.ListViewGroup("Customer Service", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup13 = new System.Windows.Forms.ListViewGroup("Shelter Manager", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup14 = new System.Windows.Forms.ListViewGroup("Department Managers", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup15 = new System.Windows.Forms.ListViewGroup("Employees", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup16 = new System.Windows.Forms.ListViewGroup("Unassigned Employees", System.Windows.Forms.HorizontalAlignment.Left);
            this.tabDptManagers = new System.Windows.Forms.TabControl();
            this.tabDepartments = new System.Windows.Forms.TabPage();
            this.DeptsListView = new System.Windows.Forms.ListView();
            this.IDCol = new System.Windows.Forms.ColumnHeader();
            this.NameCol = new System.Windows.Forms.ColumnHeader();
            this.ManagerCol = new System.Windows.Forms.ColumnHeader();
            this.PhoneCol = new System.Windows.Forms.ColumnHeader();
            this.DelDepartBtn = new System.Windows.Forms.Button();
            this.EditDeptBtn = new System.Windows.Forms.Button();
            this.AddDeptBtn = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.EmployeesListView = new System.Windows.Forms.ListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader4 = new System.Windows.Forms.ColumnHeader();
            this.DelDeptManagerBtn = new System.Windows.Forms.Button();
            this.EditDeptManagerBtn = new System.Windows.Forms.Button();
            this.AddDptMngBtn = new System.Windows.Forms.Button();
            this.sheltersTab = new System.Windows.Forms.TabPage();
            this.SheltersListView = new System.Windows.Forms.ListView();
            this.columnHeader14 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader15 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader16 = new System.Windows.Forms.ColumnHeader();
            this.editShelterBtn = new System.Windows.Forms.Button();
            this.addShelterBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.LogOutBtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.UserNameLbl = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.StoreLbl = new System.Windows.Forms.Label();
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader5 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader6 = new System.Windows.Forms.ColumnHeader();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.listView2 = new System.Windows.Forms.ListView();
            this.columnHeader7 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader8 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader9 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader10 = new System.Windows.Forms.ColumnHeader();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.listView3 = new System.Windows.Forms.ListView();
            this.columnHeader11 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader12 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader13 = new System.Windows.Forms.ColumnHeader();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.tabDptManagers.SuspendLayout();
            this.tabDepartments.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.sheltersTab.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabDptManagers
            // 
            this.tabDptManagers.Controls.Add(this.tabDepartments);
            this.tabDptManagers.Controls.Add(this.tabPage2);
            this.tabDptManagers.Controls.Add(this.sheltersTab);
            this.tabDptManagers.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabDptManagers.Location = new System.Drawing.Point(12, 79);
            this.tabDptManagers.Name = "tabDptManagers";
            this.tabDptManagers.SelectedIndex = 0;
            this.tabDptManagers.Size = new System.Drawing.Size(662, 465);
            this.tabDptManagers.TabIndex = 0;
            // 
            // tabDepartments
            // 
            this.tabDepartments.BackColor = System.Drawing.Color.Transparent;
            this.tabDepartments.Controls.Add(this.DeptsListView);
            this.tabDepartments.Controls.Add(this.DelDepartBtn);
            this.tabDepartments.Controls.Add(this.EditDeptBtn);
            this.tabDepartments.Controls.Add(this.AddDeptBtn);
            this.tabDepartments.Location = new System.Drawing.Point(4, 25);
            this.tabDepartments.Name = "tabDepartments";
            this.tabDepartments.Padding = new System.Windows.Forms.Padding(3);
            this.tabDepartments.Size = new System.Drawing.Size(654, 436);
            this.tabDepartments.TabIndex = 0;
            this.tabDepartments.Text = "Departments";
            this.tabDepartments.UseVisualStyleBackColor = true;
            // 
            // DeptsListView
            // 
            this.DeptsListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.IDCol,
            this.NameCol,
            this.ManagerCol,
            this.PhoneCol});
            this.DeptsListView.FullRowSelect = true;
            listViewGroup1.Header = "Animal Care";
            listViewGroup1.Name = "AnimalCareGroup";
            listViewGroup2.Header = "Customer Service";
            listViewGroup2.Name = "CustomerServiceGroup";
            this.DeptsListView.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup1,
            listViewGroup2});
            this.DeptsListView.Location = new System.Drawing.Point(6, 6);
            this.DeptsListView.MultiSelect = false;
            this.DeptsListView.Name = "DeptsListView";
            this.DeptsListView.Size = new System.Drawing.Size(496, 396);
            this.DeptsListView.TabIndex = 4;
            this.DeptsListView.UseCompatibleStateImageBehavior = false;
            this.DeptsListView.View = System.Windows.Forms.View.Details;
            // 
            // IDCol
            // 
            this.IDCol.Text = "ID";
            this.IDCol.Width = 56;
            // 
            // NameCol
            // 
            this.NameCol.Text = "Name";
            this.NameCol.Width = 142;
            // 
            // ManagerCol
            // 
            this.ManagerCol.Text = "Manager";
            this.ManagerCol.Width = 145;
            // 
            // PhoneCol
            // 
            this.PhoneCol.Text = "Phone Extension";
            this.PhoneCol.Width = 92;
            // 
            // DelDepartBtn
            // 
            this.DelDepartBtn.Location = new System.Drawing.Point(506, 63);
            this.DelDepartBtn.Name = "DelDepartBtn";
            this.DelDepartBtn.Size = new System.Drawing.Size(142, 23);
            this.DelDepartBtn.TabIndex = 3;
            this.DelDepartBtn.Text = "Delete";
            this.DelDepartBtn.UseVisualStyleBackColor = true;
            this.DelDepartBtn.Click += new System.EventHandler(this.DelDepartBtn_Click);
            // 
            // EditDeptBtn
            // 
            this.EditDeptBtn.Location = new System.Drawing.Point(506, 35);
            this.EditDeptBtn.Name = "EditDeptBtn";
            this.EditDeptBtn.Size = new System.Drawing.Size(142, 23);
            this.EditDeptBtn.TabIndex = 2;
            this.EditDeptBtn.Text = "Edit";
            this.EditDeptBtn.UseVisualStyleBackColor = true;
            this.EditDeptBtn.Click += new System.EventHandler(this.EditDeptBtn_Click);
            // 
            // AddDeptBtn
            // 
            this.AddDeptBtn.Location = new System.Drawing.Point(506, 6);
            this.AddDeptBtn.Name = "AddDeptBtn";
            this.AddDeptBtn.Size = new System.Drawing.Size(143, 23);
            this.AddDeptBtn.TabIndex = 1;
            this.AddDeptBtn.Text = "Add";
            this.AddDeptBtn.UseVisualStyleBackColor = true;
            this.AddDeptBtn.Click += new System.EventHandler(this.AddDeptBtn_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.Transparent;
            this.tabPage2.Controls.Add(this.EmployeesListView);
            this.tabPage2.Controls.Add(this.DelDeptManagerBtn);
            this.tabPage2.Controls.Add(this.EditDeptManagerBtn);
            this.tabPage2.Controls.Add(this.AddDptMngBtn);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(654, 436);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Employees";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // EmployeesListView
            // 
            this.EmployeesListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader4});
            this.EmployeesListView.FullRowSelect = true;
            listViewGroup3.Header = "Shelter Manager";
            listViewGroup3.Name = "ShelterManagerGroup";
            listViewGroup4.Header = "Department Managers";
            listViewGroup4.Name = "DeptManagersGroup";
            listViewGroup5.Header = "Employees";
            listViewGroup5.Name = "EmployeesGroup";
            listViewGroup6.Header = "Unassigned Employees";
            listViewGroup6.Name = "UnEmployeeGroup";
            this.EmployeesListView.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup3,
            listViewGroup4,
            listViewGroup5,
            listViewGroup6});
            this.EmployeesListView.Location = new System.Drawing.Point(5, 6);
            this.EmployeesListView.MultiSelect = false;
            this.EmployeesListView.Name = "EmployeesListView";
            this.EmployeesListView.Size = new System.Drawing.Size(496, 396);
            this.EmployeesListView.TabIndex = 8;
            this.EmployeesListView.UseCompatibleStateImageBehavior = false;
            this.EmployeesListView.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Employee ID";
            this.columnHeader1.Width = 81;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Name";
            this.columnHeader2.Width = 142;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Department";
            this.columnHeader4.Width = 120;
            // 
            // DelDeptManagerBtn
            // 
            this.DelDeptManagerBtn.Location = new System.Drawing.Point(506, 63);
            this.DelDeptManagerBtn.Name = "DelDeptManagerBtn";
            this.DelDeptManagerBtn.Size = new System.Drawing.Size(142, 23);
            this.DelDeptManagerBtn.TabIndex = 7;
            this.DelDeptManagerBtn.Text = "Delete";
            this.DelDeptManagerBtn.UseVisualStyleBackColor = true;
            this.DelDeptManagerBtn.Click += new System.EventHandler(this.DelDeptManagerBtn_Click);
            // 
            // EditDeptManagerBtn
            // 
            this.EditDeptManagerBtn.Location = new System.Drawing.Point(506, 35);
            this.EditDeptManagerBtn.Name = "EditDeptManagerBtn";
            this.EditDeptManagerBtn.Size = new System.Drawing.Size(142, 23);
            this.EditDeptManagerBtn.TabIndex = 6;
            this.EditDeptManagerBtn.Text = "Edit";
            this.EditDeptManagerBtn.UseVisualStyleBackColor = true;
            this.EditDeptManagerBtn.Click += new System.EventHandler(this.EditDeptManagerBtn_Click);
            // 
            // AddDptMngBtn
            // 
            this.AddDptMngBtn.Location = new System.Drawing.Point(506, 6);
            this.AddDptMngBtn.Name = "AddDptMngBtn";
            this.AddDptMngBtn.Size = new System.Drawing.Size(143, 23);
            this.AddDptMngBtn.TabIndex = 5;
            this.AddDptMngBtn.Text = "Add";
            this.AddDptMngBtn.UseVisualStyleBackColor = true;
            this.AddDptMngBtn.Click += new System.EventHandler(this.AddDptMngBtn_Click);
            // 
            // sheltersTab
            // 
            this.sheltersTab.BackColor = System.Drawing.Color.Transparent;
            this.sheltersTab.Controls.Add(this.SheltersListView);
            this.sheltersTab.Controls.Add(this.editShelterBtn);
            this.sheltersTab.Controls.Add(this.addShelterBtn);
            this.sheltersTab.Location = new System.Drawing.Point(4, 25);
            this.sheltersTab.Name = "sheltersTab";
            this.sheltersTab.Padding = new System.Windows.Forms.Padding(3);
            this.sheltersTab.Size = new System.Drawing.Size(654, 436);
            this.sheltersTab.TabIndex = 2;
            this.sheltersTab.Text = "Animal Shelters";
            this.sheltersTab.UseVisualStyleBackColor = true;
            // 
            // SheltersListView
            // 
            this.SheltersListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader14,
            this.columnHeader15,
            this.columnHeader16});
            this.SheltersListView.FullRowSelect = true;
            this.SheltersListView.Location = new System.Drawing.Point(5, 6);
            this.SheltersListView.MultiSelect = false;
            this.SheltersListView.Name = "SheltersListView";
            this.SheltersListView.Size = new System.Drawing.Size(496, 396);
            this.SheltersListView.TabIndex = 8;
            this.SheltersListView.UseCompatibleStateImageBehavior = false;
            this.SheltersListView.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader14
            // 
            this.columnHeader14.Text = "Number";
            this.columnHeader14.Width = 81;
            // 
            // columnHeader15
            // 
            this.columnHeader15.Text = "Name";
            this.columnHeader15.Width = 142;
            // 
            // columnHeader16
            // 
            this.columnHeader16.Text = "Location";
            this.columnHeader16.Width = 120;
            // 
            // editShelterBtn
            // 
            this.editShelterBtn.Location = new System.Drawing.Point(506, 35);
            this.editShelterBtn.Name = "editShelterBtn";
            this.editShelterBtn.Size = new System.Drawing.Size(142, 23);
            this.editShelterBtn.TabIndex = 6;
            this.editShelterBtn.Text = "Edit";
            this.editShelterBtn.UseVisualStyleBackColor = true;
            this.editShelterBtn.Click += new System.EventHandler(this.editShelterBtn_Click);
            // 
            // addShelterBtn
            // 
            this.addShelterBtn.Location = new System.Drawing.Point(506, 6);
            this.addShelterBtn.Name = "addShelterBtn";
            this.addShelterBtn.Size = new System.Drawing.Size(143, 23);
            this.addShelterBtn.TabIndex = 5;
            this.addShelterBtn.Text = "Add";
            this.addShelterBtn.UseVisualStyleBackColor = true;
            this.addShelterBtn.Click += new System.EventHandler(this.addShelterBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(450, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "Loged In As:";
            // 
            // LogOutBtn
            // 
            this.LogOutBtn.Location = new System.Drawing.Point(595, 9);
            this.LogOutBtn.Name = "LogOutBtn";
            this.LogOutBtn.Size = new System.Drawing.Size(75, 23);
            this.LogOutBtn.TabIndex = 2;
            this.LogOutBtn.Text = "Log Out";
            this.LogOutBtn.UseVisualStyleBackColor = true;
            this.LogOutBtn.Click += new System.EventHandler(this.LogOutBtn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(13, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(180, 18);
            this.label2.TabIndex = 3;
            this.label2.Text = "AWMS: Shelter Manager";
            // 
            // UserNameLbl
            // 
            this.UserNameLbl.AutoSize = true;
            this.UserNameLbl.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UserNameLbl.Location = new System.Drawing.Point(453, 36);
            this.UserNameLbl.Name = "UserNameLbl";
            this.UserNameLbl.Size = new System.Drawing.Size(0, 20);
            this.UserNameLbl.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(16, 42);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 20);
            this.label3.TabIndex = 6;
            this.label3.Text = "Shelter:";
            // 
            // StoreLbl
            // 
            this.StoreLbl.AutoSize = true;
            this.StoreLbl.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StoreLbl.Location = new System.Drawing.Point(67, 42);
            this.StoreLbl.Name = "StoreLbl";
            this.StoreLbl.Size = new System.Drawing.Size(0, 20);
            this.StoreLbl.TabIndex = 7;
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader3,
            this.columnHeader5,
            this.columnHeader6});
            this.listView1.FullRowSelect = true;
            listViewGroup7.Header = "Shelter Manager";
            listViewGroup7.Name = "ShelterManagerGroup";
            listViewGroup8.Header = "Department Managers";
            listViewGroup8.Name = "DeptManagersGroup";
            listViewGroup9.Header = "Employees";
            listViewGroup9.Name = "EmployeesGroup";
            listViewGroup10.Header = "Unassigned Employees";
            listViewGroup10.Name = "UnEmployeeGroup";
            this.listView1.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup7,
            listViewGroup8,
            listViewGroup9,
            listViewGroup10});
            this.listView1.Location = new System.Drawing.Point(5, 6);
            this.listView1.MultiSelect = false;
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(496, 396);
            this.listView1.TabIndex = 8;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Employee ID";
            this.columnHeader3.Width = 81;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Name";
            this.columnHeader5.Width = 142;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Department";
            this.columnHeader6.Width = 120;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(506, 63);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(142, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "Delete";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(506, 35);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(142, 23);
            this.button2.TabIndex = 6;
            this.button2.Text = "Edit";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(506, 6);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(143, 23);
            this.button3.TabIndex = 5;
            this.button3.Text = "Add";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.Transparent;
            this.tabPage1.Controls.Add(this.listView2);
            this.tabPage1.Controls.Add(this.button4);
            this.tabPage1.Controls.Add(this.button5);
            this.tabPage1.Controls.Add(this.button6);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(654, 436);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Departments";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // listView2
            // 
            this.listView2.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader7,
            this.columnHeader8,
            this.columnHeader9,
            this.columnHeader10});
            this.listView2.FullRowSelect = true;
            listViewGroup11.Header = "Animal Care";
            listViewGroup11.Name = "AnimalCareGroup";
            listViewGroup12.Header = "Customer Service";
            listViewGroup12.Name = "CustomerServiceGroup";
            this.listView2.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup11,
            listViewGroup12});
            this.listView2.Location = new System.Drawing.Point(6, 6);
            this.listView2.MultiSelect = false;
            this.listView2.Name = "listView2";
            this.listView2.Size = new System.Drawing.Size(496, 396);
            this.listView2.TabIndex = 4;
            this.listView2.UseCompatibleStateImageBehavior = false;
            this.listView2.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "ID";
            this.columnHeader7.Width = 56;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Name";
            this.columnHeader8.Width = 142;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "Manager";
            this.columnHeader9.Width = 145;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Phone Extension";
            this.columnHeader10.Width = 92;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(506, 63);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(142, 23);
            this.button4.TabIndex = 3;
            this.button4.Text = "Delete";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(506, 35);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(142, 23);
            this.button5.TabIndex = 2;
            this.button5.Text = "Edit";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(506, 6);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(143, 23);
            this.button6.TabIndex = 1;
            this.button6.Text = "Add";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.Transparent;
            this.tabPage3.Controls.Add(this.listView3);
            this.tabPage3.Controls.Add(this.button7);
            this.tabPage3.Controls.Add(this.button8);
            this.tabPage3.Controls.Add(this.button9);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(654, 436);
            this.tabPage3.TabIndex = 1;
            this.tabPage3.Text = "Employees";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // listView3
            // 
            this.listView3.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader11,
            this.columnHeader12,
            this.columnHeader13});
            this.listView3.FullRowSelect = true;
            listViewGroup13.Header = "Shelter Manager";
            listViewGroup13.Name = "ShelterManagerGroup";
            listViewGroup14.Header = "Department Managers";
            listViewGroup14.Name = "DeptManagersGroup";
            listViewGroup15.Header = "Employees";
            listViewGroup15.Name = "EmployeesGroup";
            listViewGroup16.Header = "Unassigned Employees";
            listViewGroup16.Name = "UnEmployeeGroup";
            this.listView3.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup13,
            listViewGroup14,
            listViewGroup15,
            listViewGroup16});
            this.listView3.Location = new System.Drawing.Point(5, 6);
            this.listView3.MultiSelect = false;
            this.listView3.Name = "listView3";
            this.listView3.Size = new System.Drawing.Size(496, 396);
            this.listView3.TabIndex = 8;
            this.listView3.UseCompatibleStateImageBehavior = false;
            this.listView3.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "Employee ID";
            this.columnHeader11.Width = 81;
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "Name";
            this.columnHeader12.Width = 142;
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "Department";
            this.columnHeader13.Width = 120;
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(506, 63);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(142, 23);
            this.button7.TabIndex = 7;
            this.button7.Text = "Delete";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(506, 35);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(142, 23);
            this.button8.TabIndex = 6;
            this.button8.Text = "Edit";
            this.button8.UseVisualStyleBackColor = true;
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(506, 6);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(143, 23);
            this.button9.TabIndex = 5;
            this.button9.Text = "Add";
            this.button9.UseVisualStyleBackColor = true;
            // 
            // MainShelterManagerApp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(689, 556);
            this.Controls.Add(this.StoreLbl);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.UserNameLbl);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.LogOutBtn);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tabDptManagers);
            this.Name = "MainShelterManagerApp";
            this.Text = "AWMS: Shelter Manager App";
            this.tabDptManagers.ResumeLayout(false);
            this.tabDepartments.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.sheltersTab.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabDptManagers;
        private System.Windows.Forms.TabPage tabDepartments;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button LogOutBtn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListView DeptsListView;
        private System.Windows.Forms.ColumnHeader IDCol;
        private System.Windows.Forms.ColumnHeader NameCol;
        private System.Windows.Forms.ColumnHeader ManagerCol;
        private System.Windows.Forms.Button DelDepartBtn;
        private System.Windows.Forms.Button EditDeptBtn;
        private System.Windows.Forms.Button AddDeptBtn;
        private System.Windows.Forms.ListView EmployeesListView;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Button DelDeptManagerBtn;
        private System.Windows.Forms.Button EditDeptManagerBtn;
        private System.Windows.Forms.Button AddDptMngBtn;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.Label UserNameLbl;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label StoreLbl;
        private System.Windows.Forms.ColumnHeader PhoneCol;
        private System.Windows.Forms.TabPage sheltersTab;
        private System.Windows.Forms.ListView SheltersListView;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.ColumnHeader columnHeader15;
        private System.Windows.Forms.ColumnHeader columnHeader16;
        private System.Windows.Forms.Button editShelterBtn;
        private System.Windows.Forms.Button addShelterBtn;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.ListView listView2;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.ListView listView3;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
    }
}