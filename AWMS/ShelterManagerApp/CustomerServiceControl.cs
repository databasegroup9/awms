﻿using System;
using System.Windows.Forms;

namespace AWMS.ShelterManagerApp
{
    public partial class CustomerServiceControl : UserControl
    {
        public CustomerServiceControl()
        {
            InitializeComponent();
        }

        public int GetPublicPhone()
        {
            if (string.IsNullOrEmpty(PublicPhoneTextBox.Text.Trim())) return 0;
            return Convert.ToInt32(PublicPhoneTextBox.Text.Trim());
        }

        public void SetPublicPhone(int ph)
        {
            PublicPhoneTextBox.Text = ph.ToString();
        }

        public string GetPublicEmail()
        {
            return PublicEmailTextBox.Text.Trim();
        }

        public void SetPublicEmail(string em)
        {
            PublicEmailTextBox.Text = em;
        }
    }
}
