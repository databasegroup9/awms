﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using AWMS.ShelterManagerApp;
using Oracle.DataAccess.Client;
using AWMS.Common;
using AWMS.Procs;

namespace AMWS.ShelterManagerApp
{
    public partial class DepartmentPP : Form
    {
        private OracleConnection connection;
        private AnimalCareDepartment animalDept;
        private CustomerServiceDepartment customerDept;
        private Department dept;
        private MainShelterManagerApp parentForm;
        private List<User> deptManagers;
        private DeptManager currentManager;
        private AnimalCareControl animalCareControl;
        private CustomerServiceControl customerServiceControl;

        public DepartmentPP(OracleConnection c, List<User> l, MainShelterManagerApp pf)
        {
            //New Department
            InitializeComponent();
            connection = c;
            parentForm = pf;
            deptManagers = l;
            SetManagerItems();
            deptTypeComboBox.SelectedIndex = 0;
            animalCareControl = new AnimalCareControl();
            customerServiceControl = new CustomerServiceControl();
            deptTypePanel.Controls.Add(animalCareControl);
        }

        public DepartmentPP(OracleConnection c, List<User> l, AnimalCareDepartment d, DeptManager cm, MainShelterManagerApp pf)
        {
            InitializeComponent();
            connection = c;
            animalDept = d;
            customerDept = null;
            dept = d;
            parentForm = pf;
            deptManagers = l;
            currentManager = cm;
            RefreshFields();
            SetManagerItems();
            AddCurrentlySetManager();
        }
        public DepartmentPP(OracleConnection c, List<User> l, CustomerServiceDepartment d, DeptManager cm, MainShelterManagerApp pf)
        {
            InitializeComponent();
            connection = c;
            customerDept = d;
            animalDept = null;
            dept = d;
            parentForm = pf;
            deptManagers = l;
            currentManager = cm;
            RefreshFields();
            SetManagerItems();
            AddCurrentlySetManager();
        }

        private void RefreshFields()
        {
            animalCareControl = new AnimalCareControl();
            customerServiceControl = new CustomerServiceControl();

            if (animalDept != null)
            {
                TitleLbl.Text = "Editing Department: " + animalDept.Name;
                DeptNameInputBox.Text = animalDept.Name;
                phoneExtTextBox.Text = animalDept.PhoneExt.ToString();
                deptTypeComboBox.SelectedIndex = (int)animalDept.DepartmentType;
                animalCareControl.SetAnimalType(animalDept.AnimalType);
                animalCareControl.SetVetContact(animalDept.VetContactNumber);
                deptTypePanel.Controls.Clear();
                deptTypePanel.Controls.Add(animalCareControl);
            }
            else
            {
                TitleLbl.Text = "Editing Department: " + customerDept.Name;
                DeptNameInputBox.Text = customerDept.Name;
                phoneExtTextBox.Text = customerDept.PhoneExt.ToString();
                deptTypeComboBox.SelectedIndex = (int)customerDept.DepartmentType;
                customerServiceControl.SetPublicEmail(customerDept.PublicEmail);
                customerServiceControl.SetPublicPhone(customerDept.PublicPhoneNumber);
                deptTypePanel.Controls.Clear();
                deptTypePanel.Controls.Add(customerServiceControl);
            }
        }

        private void SetManagerItems()
        {
            ManagerComboBox.Items.Clear();

            if (deptManagers.Count == 0) return;

            foreach (var d in deptManagers)
            {
                ManagerComboBox.Items.Add(d.FullName);
            }
            
            ManagerComboBox.SelectedIndex = 0;
        }

        private void AddCurrentlySetManager()
        {
            if (currentManager != null)
            {
                ManagerComboBox.SelectedItem = currentManager.FullName;
            }      
        }

        private void OkBtn_Click(object sender, EventArgs e)
        {
            Save();
            Close();
        }

        private void ApplyBtn_Click(object sender, EventArgs e)
        {
            Save();
            RefreshFields();
            parentForm.OnPropertyPageClosed();
        }

        private void CancelBtn_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Save()
        {
            try
            {
                String n = DeptNameInputBox.Text.Trim();
                int m = deptManagers[ManagerComboBox.SelectedIndex].UserId;
                int? s = parentForm.Database.ShelterId;
                int p = Convert.ToInt32(phoneExtTextBox.Text.Trim());
                DepartmentType type = deptTypeComboBox.SelectedIndex == 0 ? DepartmentType.ANIMAL_CARE : DepartmentType.CUSTOMER_SERVICE;


                //Animal Care
                string at = null;
                int vp = 0;
                if (animalCareControl != null)
                {
                    at = animalCareControl.GetAnimalType();
                    vp = animalCareControl.GetVetContact();
                }
                
                //Customer Service
                int pp = 0;
                string pe = null;
                if (customerServiceControl != null)
                {
                    pp = customerServiceControl.GetPublicPhone();
                    pe = customerServiceControl.GetPublicEmail();
                }
                

                if (animalDept == null && customerDept == null)
                {
                    if (type == DepartmentType.ANIMAL_CARE)
                    {
                        var proc = new sm_NewDepartment(connection, n, m, s, p, DepartmentType.ANIMAL_CARE, at, vp);
                        proc.Execute();
                        animalDept = new AnimalCareDepartment(proc.Id, n, p, at, vp);
                    }
                    else
                    {
                        var proc = new sm_NewDepartment(connection, n, m, s, p, DepartmentType.CUSTOMER_SERVICE, pp, pe);
                        proc.Execute();
                        customerDept = new CustomerServiceDepartment(proc.Id, n, p, pe, pp);
                    }

                }
                else
                {
                    if (type == DepartmentType.ANIMAL_CARE)
                    {
                        var proc = new StoredProcBase();
                        if (currentManager == null)
                        {
                            proc = new sm_EditDepartment(connection, n, m, 0, dept.DepartmentId, p, type, vp, at, dept.DepartmentType);
                        }
                        else
                        {
                            proc = new sm_EditDepartment(connection, n, m, currentManager.UserId, dept.DepartmentId, p, type, vp, at, dept.DepartmentType);
                        }

                        proc.Execute();
                        customerDept = null;
                        animalDept = new AnimalCareDepartment(dept.DepartmentId, n, p, "test", 0);
                        dept = animalDept;
                    }
                    else
                    {
                        var proc = new StoredProcBase();
                        if (currentManager == null)
                        {
                            proc = new sm_EditDepartment(connection, n, m, 0, dept.DepartmentId, p, type, pe, pp, dept.DepartmentType);
                        }
                        else
                        {
                            proc = new sm_EditDepartment(connection, n, m, currentManager.UserId, dept.DepartmentId, p, type, pe, pp, dept.DepartmentType);
                        }

                        proc.Execute();
                        animalDept = null;
                        customerDept = new CustomerServiceDepartment(dept.DepartmentId, n, p, "test", 0);
                        dept = customerDept;
                    }

                }

            }
            catch (Exception ex)
            {
                Util.ErrorBox(ex.Message + ex.StackTrace);
            }
            }

        private void DepartmentPP_FormClosed(object sender, FormClosedEventArgs e)
        {
            parentForm.OnPropertyPageClosed();
        }

        private void deptTypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            deptTypePanel.Controls.Clear();
            if (deptTypeComboBox.SelectedIndex == 0)
            {
                deptTypePanel.Controls.Add(animalCareControl);
            }
            else
            {
                deptTypePanel.Controls.Add(customerServiceControl);
            }
        }
    }
}
