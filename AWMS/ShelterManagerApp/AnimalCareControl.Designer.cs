﻿namespace AWMS.ShelterManagerApp
{
    partial class AnimalCareControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.AnimalTypeInputBox = new System.Windows.Forms.TextBox();
            this.VetContactTextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(-4, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Animal Type:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(-4, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(132, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Vet Contact Number:";
            // 
            // AnimalTypeInputBox
            // 
            this.AnimalTypeInputBox.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AnimalTypeInputBox.Location = new System.Drawing.Point(134, 3);
            this.AnimalTypeInputBox.Name = "AnimalTypeInputBox";
            this.AnimalTypeInputBox.Size = new System.Drawing.Size(297, 26);
            this.AnimalTypeInputBox.TabIndex = 2;
            // 
            // VetContactTextBox
            // 
            this.VetContactTextBox.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.VetContactTextBox.Location = new System.Drawing.Point(134, 36);
            this.VetContactTextBox.Name = "VetContactTextBox";
            this.VetContactTextBox.Size = new System.Drawing.Size(297, 26);
            this.VetContactTextBox.TabIndex = 3;
            // 
            // AnimalCareControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.VetContactTextBox);
            this.Controls.Add(this.AnimalTypeInputBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "AnimalCareControl";
            this.Size = new System.Drawing.Size(449, 81);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox AnimalTypeInputBox;
        private System.Windows.Forms.TextBox VetContactTextBox;
    }
}
