﻿using System;

namespace AWMS.Common
{
    public class Department
    {
        public int DepartmentId;
        public String Name;
        public DepartmentType DepartmentType;
        public int PhoneExt;

        public Department(int id, String n, int p, DepartmentType t)
        {
            DepartmentId = id;
            Name = n;
            PhoneExt = p;
            DepartmentType = t;
        }

        public Department()
        {
        }

        public override string ToString()
        {
            if (DepartmentType == DepartmentType.ANIMAL_CARE)
            {
                return DepartmentId + " - " + Name + " - Animal Care";
            }
            if (DepartmentType == DepartmentType.CUSTOMER_SERVICE)
            {
                return DepartmentId + " - " + Name + " - Customer Service";
            }

            return "None";
        }
    }
    public class AnimalCareDepartment : Department
    {
        public string AnimalType;
        public int VetContactNumber;

        public AnimalCareDepartment(int id, String n, int p, string a, int v)
        {
            DepartmentId = id;
            Name = n;
            PhoneExt = p;
            DepartmentType = DepartmentType.ANIMAL_CARE;
            AnimalType = a;
            VetContactNumber = v;
        }
    }
    public class CustomerServiceDepartment : Department
    {
        public string PublicEmail;
        public int PublicPhoneNumber;

        public CustomerServiceDepartment(int id, String n, int p, string e, int ph)
        {
            DepartmentId = id;
            Name = n;
            PhoneExt = p;
            DepartmentType = DepartmentType.CUSTOMER_SERVICE;
            PublicEmail = e;
            PublicPhoneNumber = ph;
        }
    }

    public enum DepartmentType
    {
        ANIMAL_CARE = 0,
        CUSTOMER_SERVICE = 1,
        NONE = 3
    }
}
