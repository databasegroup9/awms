﻿using System;
using System.Windows.Forms;

namespace AWMS.Common
{
    public static class Util
    {
        public static bool IsValidNumber(string text)
        {
            try
            {
                int test = Convert.ToInt32(text);
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public static void ErrorBox(string message)
        {
            MessageBox.Show(message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }

}