﻿using System;

namespace AWMS.Common
{
    public class AnimalShelter
    {
        public int ShelterId;
        public String Name;
        public int ManagerId;
        public String Location;
        public String Organisation;

        public AnimalShelter(int id, String n, int sm, String a, String o)
        {
            ShelterId = id;
            Name = n;
            ManagerId = sm;
            Location = a;
            Organisation = o;
        }

        public AnimalShelter(int id)
        {
            ShelterId = id;
        }
    }
}
