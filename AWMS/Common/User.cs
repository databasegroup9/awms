﻿using System;

namespace AWMS.Common
{
    public class User
    {
        public String FullName;
        public String Email;
        public UserType UserType;
        public Int32 UserId;
        public int Salary;
        public int PhoneNum;
        public int? DeptId;

        public User(String fn, String em, UserType ut, int id, int sal)
        {
            FullName = fn;
            Email = em;
            UserType = ut;
            UserId = id;
            Salary = 0;
            PhoneNum = 0;
            DeptId = null;
        }

        public User(String fn, String em, UserType ut, int id, int sal, int ph, int? d)
        {
            FullName = fn;
            Email = em;
            UserType = ut;
            UserId = id;
            Salary = sal;
            PhoneNum = ph;
            DeptId = d;
        }

        public User()
        {
        }
    }

    public class ShelterManager : User
    {
        public int ShelterId;

        public ShelterManager(string fn, string em, int id, int s, int ph, int sal)
        {
            FullName = fn;            
            Email = em;
            UserId = id;
            UserType = UserType.SHELTER_MANAGER;
            ShelterId = s;
            Salary = sal;
            PhoneNum = ph;
        }
    }

    public class DeptManager : User
    {
        public Department Dept;

        public DeptManager(String fn, String em, int id, Department dept, int sal, int ph)
        {
            FullName = fn;
            Email = em;
            UserType = UserType.DEPT_MANAGER;
            UserId = id;
            Dept = dept;
            Salary = sal;
            PhoneNum = ph;
            DeptId = dept.DepartmentId;
        }

        public string ToNameString()
        {
            return FullName;
        }
    }

    public class Employee : User
    {
        public Employee(String fn, String em, int? d, int id, int sal, int ph)
        {
            FullName = fn;
            Email = em;
            UserType = UserType.EMPLOYEE;
            UserId = id;
            DeptId = d;
            Salary = sal;
            PhoneNum = ph;
        }

        public string ToNameString()
        {
            return FullName;
        }
    }

    public enum UserType
    {
        EMPLOYEE = 0,
        DEPT_MANAGER = 1,
        SHELTER_MANAGER = 2,
        INVALID = 3
    }
}
