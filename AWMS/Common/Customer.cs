﻿namespace AWMS.Common
{
    public class Customer
    {
        public int? Id;
        public string Email;
        public string Name;
        public int Age;
        public string Address;

        public Customer(int? id, int age, string e, string n, string a)
        {
            Id = id;
            Age = age;
            Email = e;
            Name = n;
            Address = a;
        }
    }
}
