﻿
namespace AWMS.Common
{
    public class Animal
    {
        public int? AnimalNum;
        public string PetName;
        public int AdoptionFee;
        public string Description;
        public string CommonAnimalName;
        public string Breed;
        public int? AdoptedBy = null;
        public int KeptIn;

        public Animal(string pn, string d, string cn, string b, int f, int? id, int k)
        {
            AnimalNum = id;
            PetName = pn;
            AdoptionFee = f;
            Description = d;
            CommonAnimalName = cn;
            Breed = b;
            KeptIn = k;
        }
    }
}
