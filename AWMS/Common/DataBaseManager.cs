﻿using System;
using System.Windows.Forms;
using Oracle.DataAccess.Client;
using AWMS.Procs;

namespace AWMS.Common
{
    public class DataBaseManager
    {
        private String LoginConnStr;

        public OracleConnection Connection = null;

        public User ConnectedUser;

        public int? ShelterId;

        public int? DeptId;

        public String OrgName;

        public void Connect(String Email, String Pass, String Server)
        {
            LoginConnStr = Server;
           
            try
            {
                Connection = new OracleConnection(LoginConnStr);
                Connection.Open();
                Login(Email, Pass);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex);
                if (Connection != null)
                {
                    Connection.Close();
                }   
            }
        }

        public void Connect()
        {
            try
            {
                Connection = new OracleConnection(LoginConnStr);
                Connection.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex);
                if (Connection != null)
                {
                    Connection.Close();
                }
            }
        }

        private void Login(String Email, String Pass)
        {
            var loginProc = new log_EmployeeLogIn(Connection, Email, Pass);
            loginProc.Execute();

            if (loginProc.Result.EM_ID > 0)
            {
                ConnectedUser = new User(loginProc.Result.EM_FULL_NAME,
                                         loginProc.Result.EM_EMAIL,
                                         loginProc.Result.EM_TYPE,
                                         loginProc.Result.EM_ID,0);
                ShelterId = loginProc.Result.EM_SHELTER_ID;
                DeptId = loginProc.Result.EM_DEPT_ID;
                OrgName = loginProc.Result.EM_ORG_NAME;
            }
            else if (loginProc.Result.EM_ID == -1)
            {
                ConnectedUser = IncorectLogin();
            }
            else if (loginProc.Result.EM_ID == -2)
            {
                ConnectedUser = NoDeptEmployee();
            }
        }

        private User NoDeptEmployee()
        {
            return new User("INCORECT", "ERROR", UserType.INVALID, -2,0);
        }

        private User IncorectLogin()
        {
            return new User("INCORECT","ERROR",UserType.INVALID,-1,0);
        }

        public void Close()
        {
            Connection.Close();
        }
    }
}
